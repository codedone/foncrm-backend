package fonr.crm

import com.vividsolutions.jts.geom.GeometryFactory
import fonr.crm.controllers.cmd.realty.IndexCommand
import fonr.crm.controllers.cmd.realty.SaveCommand
import fonr.crm.exceptions.service.image.DirectoryNotFoundException
import fonr.crm.exceptions.service.image.EmptyDataException
import fonr.crm.exceptions.service.image.IllegalContentTypeException
import fonr.crm.exceptions.service.image.UnknownException
import grails.transaction.Transactional
import groovy.sql.Sql
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.transaction.TransactionStatus
import org.springframework.web.multipart.support.AbstractMultipartHttpServletRequest

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NOT_FOUND

class RealtyController implements ControllerUtils {

    private static def log = LoggerFactory.getLogger(getClass())

    def dataSource
    def sessionFactory
    def springSecurityService

    def geomertryFactory = new GeometryFactory()

    static allowedMethods = [index: "GET", save: "PUT", show: "GET", update: "PUT", uploadImage: ['POST', 'PUT'],
        listRooms: "GET", addRoom: "PUT"
    ]

    RealtyController() {
    }

    def index(IndexCommand command) {

        if (command.hasErrors()) {
            renderFieldErrors(command.errors.fieldErrors)
            return
        }

        def joinb = new StringBuilder(" ")
        def countJoinb = new StringBuilder(" ")
        def whereb = new StringBuilder(" where 1 = 1 ")
        def orderb = new StringBuilder(" order by ")
        def opts = [:]

        if (command.created) {
            if ((command.created.min ?: -1) > 0) {
                whereb <<= "and r.created >= :created_min  "
                opts.put('created_min', new Date(command.created.min * 1000))
            }

            if ((command.created.max ?: -1) > 0) {
                whereb <<= "and r.created <= :created_max  "
                opts.put('created_max', new Date(command.created.max * 1000))
            }
        }

        if (command.updated) {
            if ((command.updated.min ?: -1) > 0) {
                whereb <<= "and r.updated >= :updated_min  "
                opts.put('updated_min', new Date(command.updated.min * 1000))
            }

            if ((command.updated.max ?: -1) > 0) {
                whereb <<= "and r.updated <= :updated_max  "
                opts.put('updated_max', new Date(command.updated.max * 1000))
            }
        }

        if (command.offerType) {
            joinb <<= " inner join r.offerTypes types "
            countJoinb <<= " inner join r.offerTypes types "
            whereb <<= " and types.id = :offer_type"
            opts.put('offer_type', command.offerType)
        }

        // todo: надо заобраться с этой штукой
        if (params."purposes[]") {
            def purposes = params.list("purposes[]").collect {
                Long.parseLong(it)
            }
            joinb <<= " inner join r.purposes purposes "
            countJoinb <<= " inner join r.purposes purposes "
            whereb <<= " and purposes.id in (:purposes) "
            opts.put("purposes", purposes)
        }

        if (command.phone) {
            if (String.valueOf(command.phone).matches("^8.*")) {
                String filterPhone = String.valueOf(command.phone).replaceAll("^8", "+7")
                whereb <<= " and r.id in (select distinct rphone.realty from RealtyPhone rphone " +
                        "where rphone.phone like concat('%', :phone, '%') or " +
                        "rphone.phone like concat('%', :filterPhone, '%')) "
                opts.put('phone', command.phone)
                opts.put('filterPhone', filterPhone)
            } else {
                whereb <<= " and r.id in (select distinct rphone.realty from RealtyPhone rphone where rphone.phone like concat('%', :phone, '%')) "
                opts.put('phone', command.phone)
            }
        }

        if (command.address) {
            joinb <<= " inner join fetch r.address add "
            countJoinb <<= " inner join r.address add "
            whereb <<= " and (" +
                    "add.country like concat('%', :address, '%') " +
                    "or add.region like concat('%', :address, '%') " +
                    "or add.city like concat('%', :address, '%') " +
                    "or add.street like concat('%', :address, '%') " +
                    "or add.building like concat('%', :address, '%') " +
                    "or add.room like concat('%', :address, '%') " +
                    ") "
            opts.put("address", command.address)
        }

        def countQuery = "select count(distinct r) from Realty r ${countJoinb.toString()} ${whereb.toString()}".toString()
        def count = Realty.executeQuery(countQuery, opts)

        def pagination = [:]
        def amount = command.amount ?: 200
        if (amount == 0)
            return [realties: [], size: count[0]]

        if (amount > 0)
            pagination['max'] = amount

        pagination['offset'] = command.offset ?: 0

        def orderType = params.order ? params.order.toLowerCase() : 'desc'
        if (orderType != 'desc' && orderType != 'asc') {
            orderType = 'desc'
        }

        def sort = command.orderBy ? command.orderBy.toLowerCase() : 'id'

        if (sort == 'address') {
            if (command.address == null) {
                joinb <<= " inner join fetch r.address add "
            }
            orderb.append("add.country ")
                    .append(orderType)
                    .append(",")
                    .append("add.region ")
                    .append(orderType)
                    .append(",")
                    .append("add.city ")
                    .append(orderType)
                    .append(",")
                    .append("add.street ")
                    .append(orderType)
                    .append(",")
                    .append("add.building ")
                    .append(orderType)
                    .append(",")
                    .append("add.room ")
                    .append(orderType)
        } else {
            orderb.append("r.")
                    .append(sort)
                    .append(" ")
                    .append(orderType)
        }

        def query = "select distinct r from Realty r ${joinb.toString()} ${whereb.toString()} ${orderb.toString()}".toString()
        def list = Realty.executeQuery(query, opts, pagination)

        return [realties: list, size: count[0]]
    }

    @Transactional
    def save(SaveCommand cmd) {
        if (!isAdmin()) {
            response.setStatus(403)
            return
        }

        if (cmd.hasErrors()) {
            renderFieldErrors(cmd.errors.fieldErrors)
            return
        }
        def phones = cmd.phones.collect { new RealtyPhone(phone: it) }

        def address = new Address(
                country: cmd.address.country ?: null,
                region: cmd.address.region ?: null,
                city: cmd.address.city ?: null,
                street: cmd.address.street ?: null,
                building: cmd.address.building ?: null,
                room: cmd.address.room ?: null,
        )
        if (cmd.address.location)
            address.setLocationByCoordinates(cmd.address.location.lat, cmd.address.location.lon)


        def purposes = []
        if (cmd.purposes != null && !cmd.purposes.empty)
            purposes.addAll(RealtyPurposeType.findAllByIdInList(cmd.purposes))

        def offerTypes = []
        if (cmd.offerTypes != null && !cmd.offerTypes.empty)
            offerTypes.addAll(OfferType.findAllByIdInList(cmd.offerTypes))


        def counterparty = Counterparty.findById(cmd.owner)

        def images = []
        if (cmd.images && !cmd.images.empty) {
            def mainImageId = null
            def arrayIds = cmd.images.collect {
                if (it.main ?: 0 == 1) {
                    mainImageId = it.id
                }
                it.id
            }
            images.addAll(RealtyImage.findAllByIdInList(arrayIds))
            def mainImage = images.find { it.id == mainImageId }
            mainImage.isMain = true
            mainImage.save()
        }

        def instance = new Realty(
                phones: phones ?: [],
                info: cmd.info ?: null,
                address: address ?: null,
                created: new Date(),
                updated: new Date(),
                owner: counterparty,
                purposes: purposes,
                offerTypes: offerTypes,
                images: images
        )

        instance.validate()
        if (instance.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(instance.errors.fieldErrors)
            return
        }

        instance.save flush: true
        Sql sql = Sql.newInstance(dataSource)
        createOrderProposals(instance, sql)
        return [realty: instance]
    }

    def show() {
        def r = Realty.findById(params.id)
        if (r == null) {
            renderError(NOT_FOUND, message('realty.not.found'))
            return
        }
        return [realty: r]
    }


    @Transactional
    def update(SaveCommand cmd) {
//        log.debug("Im in REALTY CONTROLLER\n")
        long id = -1
        try {
            id = Long.parseLong(params.id)
        } catch (NumberFormatException e) {
            log.warn('update realty', e)
        }

        Realty r = Realty.findById(id)
        if (r == null) {
            renderError(HttpStatus.NOT_FOUND)
            return
        }

        if (cmd.hasErrors()) {
            renderFieldErrors(cmd.errors.fieldErrors)
            return
        }

        Boolean shouldUpdateObjects = false
        if (isAdmin()) {
            Counterparty c = Counterparty.findById(cmd.owner)
            r.setOwner(c)
            r.resetPhones(cmd.phones ?: null)
            r.resetOfferTypes(cmd.offerTypes ?: null)
            def mainImage = null
            !r.resetImages(cmd.images ? cmd.images.collect {
                if (it.main ?: 0 == 1)
                    mainImage = it.id
                it.id
            } : null)

            if (mainImage != null)
                r.resetMainImage(mainImage)

            def address = new Address(
                    country: cmd.address.country,
                    region: cmd.address.region,
                    city: cmd.address.city,
                    street: cmd.address.street,
                    building: cmd.address.building,
                    room: cmd.address.room
            )
            if (cmd.address.location)
                address.setLocationByCoordinates(cmd.address.location.lat, cmd.address.location.lon)

            //todo: stupid address resetting
            def oldAddress = r.getAddress()
            if (!(oldAddress.location != null && address.location != null && oldAddress.location.x == address.location.x && oldAddress.location.y == address.location.y)) {
                shouldUpdateObjects = true
            }
            r.setAddress(address)
            oldAddress.delete()

            r.setInfo(cmd.info ?: null)
        }

        r.resetPurposes(cmd.purposes ?: null)
//        r.setUpdated(new Date())
//        log.debug("Im in REALTY CONTROLLER 2\n")
        r.validate()
//        log.debug("Im in REALTY CONTROLLER\n")
        if (r.hasErrors()) {
//            log.debug("Errors in REALTY - {}",r.errors)
            transactionStatus.setRollbackOnly()
            renderFieldErrors(r.errors.fieldErrors)
            return
        }
        r.save flush: true
        Sql sql = Sql.newInstance(dataSource)
        if (shouldUpdateObjects) {
            createOrderProposals(r, sql)
            updateSuitabilityForRealty(r)
        }
        return [realty: r]
    }

    ImageService imageService

    def uploadImage() {
        if (!(request instanceof AbstractMultipartHttpServletRequest)) {
            renderError(BAD_REQUEST)
            return
        }
        def multipartRequest = request as AbstractMultipartHttpServletRequest

        imageService.uploadImages(multipartRequest.getFiles('file'))
                .onError { Throwable e ->
            log.error('Upload failed', e)
            def messageId
            if (e instanceof EmptyDataException)
                messageId = 'imageService.uploadImage.emptyData'
            else if (e instanceof IllegalContentTypeException)
                messageId = 'imageService.uploadImage.notImage'
            else if (e instanceof DirectoryNotFoundException)
                messageId = 'imageService.initFilesDirectory.fail'
            else if (e instanceof UnknownException)
                messageId = 'imageService.uploadImage.unknown'

            if (messageId != null)
                renderError(BAD_REQUEST, message(messageId))
            else
                throw e
        }
        .then { List<ImageService.UploadedImage> images ->
            RealtyImage.withNewSession {
                session ->
                    RealtyImage.withTransaction {
                        TransactionStatus status ->
                            def realtyImages = images.collect {
                                RealtyImage rimage = new RealtyImage(
                                        contentType: it.contentType,
                                        filePath: it.originalFilePath,
                                        name: it.originalFileName
                                )
                                if (!rimage.validate()) {
                                    status.setRollbackOnly()
                                    renderFieldErrors(rimage.errors.fieldErrors)
                                    return
                                }
                                rimage.save flush: true
                                rimage
                            }
                            respond realtyImages, [view: 'uploadImage', model: [images: realtyImages]]
                    }
                    session.clear()
            }

        }
    }

    def listRooms(fonr.crm.controllers.cmd.room.IndexCommand cmd){
        def r = Realty.findById(params.id)
        if (r == null) {
            renderError(NOT_FOUND, message('realty.not.found'))
            return
        }
        cmd.setRealty(r.id)
        redirect actionName: 'index', controllerName: 'Room', params: params
    }

    def addRoom(fonr.crm.controllers.cmd.room.SaveCommand cmd){
        def r = Realty.findById(params.id)
        if (r == null) {
            renderError(NOT_FOUND, message('realty.not.found'))
            return
        }
        cmd.setRealty(r.id)
        redirect actionName: 'save', controllerName: 'Room', params: params
    }

    private boolean isAdmin() {
        return (springSecurityService.currentUser as User).isAdmin()
    }

    @Transactional
    public void createOrderProposals(Realty realty, Sql sql) {
        Date now = new Date()
        final session = sessionFactory.currentSession

        def ids = session.createSQLQuery("" +
                "SELECT o.id from orders o INNER JOIN realties r INNER JOIN addresses a ON r.address_id = a.id" +
                "  where " +
                "    o.id not in (SELECT oid from order_proposals where rid = :realty )" +
                "    AND ST_Contains(o.region, a.location) " +
                "    AND o.id in (" +
                "      SELECT order_id from realty_purposes r INNER JOIN order_purposes o" +
                "        ON r.realty_purpose_type_id = o.realty_purpose_type_id WHERE r.realty_id = :realty )" +
                "    AND o.id in (SELECT order_id from realty_offer_types r INNER JOIN order_offer_types o" +
                "        ON r.offer_type = o.offer_type WHERE r.realty_id = :realty )" +
                "    AND r.id = :realty ;"
        ).with {
            setLong('realty', realty.id)
            list()
        } as List
        List<Order> results
        if (ids.isEmpty())
            results = []
        else {
            results = Order.findAllByIdInList(ids as List)
            results = results.findAll {
                GeometryUtils.pointInsideMultipolygon(realty.address.location, it.region)
            }
        }

        sql.withBatch(
                "INSERT INTO order_proposals (oid,rid,created,updated,excellent,shareable,bysquare,state, version) " +
                        "VALUES (?,${realty.id}, ?, ?, ?, ?, ?, \"UNPROCESSED\",1);"
        ) { ps ->
            Boolean hasRooms = realty.rooms != null && !realty.rooms.isEmpty()
            BigDecimal totalSquare = hasRooms ? realty.rooms.groupBy { it.floorNumber }.entrySet().max {
                it.value.square.sum()
            }.value.square.sum() : BigDecimal.ZERO
            Boolean hasShareable = hasRooms && realty.rooms.find { it.shareable }
            BigDecimal minSquare = hasRooms ? realty.rooms.square.min() : BigDecimal.ZERO
            BigDecimal maxSquare = hasRooms ? realty.rooms.square.max() : BigDecimal.ZERO

            results.forEach {
                Boolean excellent = hasRooms && (it.minSquare == null || it.minSquare <= maxSquare) && (it.maxSquare == null || it.maxSquare >= minSquare) && realty.rooms.find { room ->
                    (it.minSquare == null || it.minSquare <= room.square) &&
                            (it.maxSquare == null || it.maxSquare >= room.square)
                } != null
                Boolean shareable = hasShareable && (it.maxSquare == null || realty.rooms.find { room -> room.shareable && room.minimalPart <= it.maxSquare })
                Boolean bySquare = hasRooms && (it.minSquare == null || totalSquare >= it.minSquare)
                ps.addBatch([it.id, now, now, excellent, shareable, bySquare])

            }
        }

//        sql.withBatch("INSERT INTO order_proposals (oid,rid,created,updated,state, version) " +
//                "SELECT ?, id, ?, ?, \"UNPROCESSED\",1 from realties " +
//                "WHERE id not in (SELECT rid from order_proposals WHERE oid = ?) " +
//                "AND id in (select r.id from addresses a join realties r on a.id = r.address_id " +
//                "where ST_Contains((SELECT region from orders where id = ?), a.location));"
//        ) { ps ->
//            ps.addBatch([order.id, now, now, order.id, order.id])
//        }
    }

    public static void updateSuitabilityForRealty(Realty realty) {
        def query = 'from OrderProposal op where op.realty.id = :realty_id and op.updated < :updated '
        Collection<OrderProposal> proposals = new ArrayList<>()
        def paramsToSelect = [realty_id: realty.id, updated: new Date()]
        proposals.addAll(OrderProposal.executeQuery(query, paramsToSelect))
        Boolean hasRooms = !realty.rooms.isEmpty()
        Boolean hasShareable = hasRooms && realty.rooms.find { it.shareable }

        BigDecimal totalSquare = hasRooms ? realty.rooms.groupBy { it.floorNumber }.entrySet().max {
            it.value.square.sum()
        }.value.square.sum() : BigDecimal.ZERO
        BigDecimal minSquare = hasRooms ? realty.rooms.square.min() : BigDecimal.ZERO
        BigDecimal maxSquare = hasRooms ? realty.rooms.square.max() : BigDecimal.ZERO
        proposals.forEach {
            it.setExcellent(hasRooms && (it.order.minSquare == null || it.order.minSquare <= maxSquare) && (it.order.maxSquare == null || it.order.maxSquare >= minSquare)
                    && realty.rooms.find { room ->
                (it.order.minSquare == null || it.order.minSquare <= room.square) &&
                        (it.order.maxSquare == null || it.order.maxSquare >= room.square)
            } != null)
            it.setShareable(hasShareable && (it.order.maxSquare == null || realty.rooms.find { room -> room.shareable && room.minimalPart <= it.order.maxSquare }))
            it.setByTotalSquare(hasRooms && (it.order.minSquare == null || totalSquare >= it.order.minSquare))
            it.save(failOnError: true, flush: true)
        }
    }
}
