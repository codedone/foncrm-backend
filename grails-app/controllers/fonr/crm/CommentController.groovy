package fonr.crm

import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional
import org.hibernate.SessionFactory

import static org.springframework.http.HttpStatus.*

class CommentController implements ControllerUtils {

    SpringSecurityService springSecurityService
    SessionFactory sessionFactory

    static allowedMethods = [
            list_counterparty  : "GET",
            add_counterparty   : "PUT",
            update_counterparty: "PUT",
            list_realties      : "GET",
            add_realties       : "PUT",
            update_realties    : "PUT",
            list_orders        : "GET",
            add_orders         : "PUT",
            update_orders      : "PUT",
            list_rooms        : "GET",
            add_rooms         : "PUT",
            update_rooms      : "PUT",
    ]

    def index() {
        renderError(METHOD_NOT_ALLOWED)
    }

    def list_counterparty(Long id) {
        if (!Counterparty.exists(id)) {
            renderError(NOT_FOUND, message('counterparty.notfound'))
            return
        }
        final dbSession = sessionFactory.currentSession

        final query = dbSession.createSQLQuery('SELECT c.* FROM comments c INNER JOIN comments_cp cp ON cp.comment_id = c.id WHERE cp.customer = :customer ORDER BY c.created DESC')

        final results = query.with {
            addEntity(Comment)
            setLong('customer', id)
            list()
        }

        return [commentsList: results]
    }

    @Transactional
    def add_counterparty() {
        User user = springSecurityService.getCurrentUser()
        if (user == null) {
            renderError(UNAUTHORIZED)
            return
        }
        boolean isAdmin = user.getAuthorities().contains(Role.getRoleWithName("admin"))
        if (!isAdmin) {
            renderError(FORBIDDEN)
            return
        }

        def json = readJSON()
        if (!json) {
            transactionStatus.setRollbackOnly()
            return
        }

        def c = new Comment(author: user, comment: json.comment)
        c.validate()
        if (c.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(c.errors.fieldErrors)
            return
        }
        c.save flush: true
        Counterparty cp = Counterparty.get(params.id)
        if (cp == null) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('counterparty.notfound'))
            return
        }
        cp.addToComments(c)
        cp.save()
        return [comment: c]
    }

    @Transactional
    def update_counterparty(Long id, Long cid) {
        User user = springSecurityService.getCurrentUser()
        if (user == null) {
            renderError(UNAUTHORIZED)
            return
        }
        boolean isAdmin = user.getAuthorities().contains(Role.getRoleWithName("admin"))
        if (!isAdmin) {
            renderError(FORBIDDEN)
            return
        }

        if (!Counterparty.exists(id)) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('counterparty.notfound'))
            return
        }

        def json = readJSON()
        if (!json) {
            transactionStatus.setRollbackOnly()
            return
        }

        Comment c = Comment.get(cid)
        c.setComment(json.comment)
        c.validate()
        if (c.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(c.errors.fieldErrors)
            return
        }
        c.save flush: true

        return [comment: c]
    }

    def list_realties(Long id) {
        if (!Realty.exists(id)) {
            renderError(NOT_FOUND, message('realty.not.found'))
            return
        }
        final dbSession = sessionFactory.currentSession

        final query = dbSession.createSQLQuery('SELECT c.* FROM comments c INNER JOIN comments_realties cr ON cr.comment_id = c.id WHERE cr.realty_id = :realty ORDER BY c.created DESC')

        final results = query.with {
            addEntity(Comment)
            setLong('realty', id)
            list()
        }

        return [commentsList: results]
    }

    @Transactional
    def add_realties() {
        def user = springSecurityService.getCurrentUser()
        if (user == null) {
            renderError(UNAUTHORIZED)
            return
        }

        def json = readJSON()
        if (!json) {
            transactionStatus.setRollbackOnly()
            return
        }

        def c = new Comment(author: user, comment: json.comment)
        c.validate()
        if (c.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(c.errors.fieldErrors)
            return
        }
        c.save flush: true
        Realty r = Realty.get(params.id)
        if (r == null) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('realty.not.found'))
            return
        }
        r.addToComments(c)
        r.save(flush: true)
        return [comment: c]
    }

    @Transactional
    def update_realties(Long id, Long cid) {
        def user = springSecurityService.getCurrentUser()
        if (user == null) {
            transactionStatus.setRollbackOnly()
            renderError(UNAUTHORIZED)
            return
        }

        boolean isAdmin = user.getAuthorities().contains(Role.getRoleWithName("admin"))
        if (!isAdmin) {
            renderError(FORBIDDEN)
            return
        }

        if (!Realty.exists(id)) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('realty.not.found'))
            return
        }

        def json = readJSON()
        if (!json) {
            transactionStatus.setRollbackOnly()
            return
        }

        Comment c = Comment.get(cid)
        c.setComment(json.comment)
        c.validate()
        if (c.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(c.errors.fieldErrors)
            return
        }
        c.save flush: true
        return [comment: c]
    }

    def list_orders(Long id) {
        def order = Order.findById(id)
        def user = springSecurityService.getCurrentUser()
        if (user == null) {
            transactionStatus.setRollbackOnly()
            renderError(UNAUTHORIZED)
            return
        }

        boolean isAdmin = user.getAuthorities().contains(Role.getRoleWithName("admin"))

        if (order == null || order.assignee != user && !isAdmin) {
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }
        final dbSession = sessionFactory.currentSession

        final query = dbSession.createSQLQuery('SELECT c.* FROM comments c INNER JOIN comments_orders co ON co.comment_id = c.id WHERE co.order_id = :order ORDER BY c.created DESC')

        final results = query.with {
            addEntity(Comment)
            setLong('order', id)
            list()
        }
        return [commentsList: results]
    }

    @Transactional
    def add_orders() {
        def user = springSecurityService.getCurrentUser()
        if (user == null) {
            renderError(UNAUTHORIZED)
            return
        }

        def json = readJSON()
        if (!json)
            return

        boolean isAdmin = user.getAuthorities().contains(Role.getRoleWithName("admin"))

        Order o = Order.findById(params.id)
        if (o == null || o.assignee != user && !isAdmin) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }

        def c = new Comment(author: user, comment: json.comment)
        c.validate()
        if (c.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(c.errors.fieldErrors)
            return
        }
        c.save flush: true
        o.addToComments(c)
        o.save(flush: true)

        return [comment: c]
    }

    @Transactional
    def update_orders(Long id, Long cid) {
        def user = springSecurityService.getCurrentUser()
        if (user == null) {
            renderError(UNAUTHORIZED)
            return
        }

        def json = readJSON()
        if (!json)
            return

        boolean isAdmin = user.getAuthorities().contains(Role.getRoleWithName("admin"))

        Order o = Order.get(id)
        if (o == null || o.assignee != user && !isAdmin) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }

        Comment c = Comment.get(cid)
        c.setComment(json.comment)
        c.validate()
        if (c.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(c.errors.fieldErrors)
            return
        }
        c.save flush: true

        return [comment: c]
    }

    @Transactional
    def comment_order_proposal(Long pid) {
        def user = springSecurityService.getCurrentUser()
        if (user == null) {
            renderError(UNAUTHORIZED)
            return
        }

        def json = readJSON()
        if (!json)
            return

        def orderProposal = OrderProposal.get(pid)
        if (orderProposal == null) {
            renderError(NOT_FOUND)
            return
        }
        def oldComment = orderProposal.comment

        if (json.comment) {
            if (oldComment == null) {
                def comment = new Comment(author: user, comment: json.comment)
                orderProposal.comment = comment
            } else {
                if (oldComment.author.id != user.id)
                    oldComment.author = user

                oldComment.comment = json.comment
                oldComment.created = new Date()
                orderProposal.updated = new Date()
            }
        } else {
            orderProposal.comment = null
            if (oldComment != null)
                oldComment.delete()
        }
        orderProposal.validate()
        if (orderProposal.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(orderProposal.errors.fieldErrors)
            return
        }
        orderProposal.save(failOnError: true, flush: true)
        return [comment: orderProposal.comment]
    }


    def list_rooms(Long id) {
        //todo: change
        if (!Room.exists(id)) {
            renderError(NOT_FOUND, message('room.not.found'))
            return
        }
        final dbSession = sessionFactory.currentSession

        final query = dbSession.createSQLQuery('SELECT c.* FROM comments c INNER JOIN comments_rooms cr ON cr.comment_id = c.id WHERE cr.room_id = :room ORDER BY c.created DESC')

        final results = query.with {
            addEntity(Comment)
            setLong('room', id)
            list()
        }

        return [commentsList: results]
    }

    @Transactional
    def add_rooms() {
        //todo: change
        def user = springSecurityService.getCurrentUser()
        if (user == null) {
            renderError(UNAUTHORIZED)
            return
        }

        def json = readJSON()
        if (!json) {
            transactionStatus.setRollbackOnly()
            return
        }

        def c = new Comment(author: user, comment: json.comment)
        c.validate()
        if (c.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(c.errors.fieldErrors)
            return
        }
        c.save flush: true
        Room r = Room.get(params.id)
        if (r == null) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('room.not.found'))
            return
        }
        r.addToComments(c)
        r.save(flush: true)
        return [comment: c]
    }

    @Transactional
    def update_rooms(Long id, Long cid) {
        //todo: change
        def user = springSecurityService.getCurrentUser()
        if (user == null) {
            transactionStatus.setRollbackOnly()
            renderError(UNAUTHORIZED)
            return
        }

        boolean isAdmin = user.getAuthorities().contains(Role.getRoleWithName("admin"))
        if (!isAdmin) {
            renderError(FORBIDDEN)
            return
        }

        if (!Room.exists(id)) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('room.not.found'))
            return
        }

        def json = readJSON()
        if (!json) {
            transactionStatus.setRollbackOnly()
            return
        }

        Comment c = Comment.get(cid)
        c.setComment(json.comment)
        c.validate()
        if (c.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(c.errors.fieldErrors)
            return
        }
        c.save flush: true
        return [comment: c]
    }

}
