package fonr.crm

import com.vividsolutions.jts.geom.MultiPolygon
import fonr.crm.controllers.cmd.order.RoomFilter
import fonr.crm.controllers.cmd.room.IndexCommand
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional
import groovy.sql.Sql
import org.hibernate.SQLQuery
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.validation.FieldError

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NOT_FOUND

class OrderController implements ControllerUtils {
    def dataSource
    def sessionFactory
    SpringSecurityService springSecurityService

    static final Logger log = LoggerFactory.getLogger(OrderController)

    static allowedMethods = [
            index        : 'GET',
            show         : 'GET',
            save         : 'PUT',
            update       : 'PUT',
            complete     : 'POST',
            discard      : 'POST',
            reopen       : 'POST',
            updateRealty : 'POST',
            suggest      : 'GET',
            updateSuggest: 'GET'
    ]
    private final allowedOrderFields = ['id', 'created', 'updated', 'assignee', 'counterparty', 'state']
    private final allowedOrderFieldsForOrderProposals = ['id', 'info', 'comment', 'state', 'address']


    OrderController() {
    }

    def index() {
        def whereB = new StringBuilder(" where 1 = 1 ")
        def joinB = new StringBuilder(" ")
        def countJoinB = new StringBuilder(" ")
        def opts = [:]

        def isAdmin = isAdmin()
        User currentUser = springSecurityService.currentUser
        if (!isAdmin) {
            whereB <<= " and o.assignee.id = :assignee_id "
            opts.put("assignee_id", currentUser.id)
        }

        if (params.state) {
            whereB <<= " and o.state = :state "
            opts.put("state", Order.State.of(params.state))
        }

        try {
            if (params.id) {
                whereB <<= " and o.id = :id "
                opts.put("id", Long.parseLong(params.id))
            }

            if (params.created) {
                def createdMin = params.created.min ? Long.parseLong(params.created.min) : -1
                def createdMax = params.created.max ? Long.parseLong(params.created.max) : -1
                if (createdMin > 0) {
                    whereB <<= " and o.created >= :created_min"
                    opts.put("created_min", new Date(createdMin * 1000))
                }
                if (createdMax > 0) {
                    whereB <<= " and o.created <= :created_max"
                    opts.put("created_max", new Date(createdMax * 1000))
                }
            }

            if (params.updated) {
                def updatedMin = params.updated.min ? Long.parseLong(params.updated.min) : -1
                def updatedMax = params.updated.max ? Long.parseLong(params.updated.max) : -1
                if (updatedMin > 0) {
                    whereB <<= " and o.updated >= :updated_min"
                    opts.put("updated_min", new Date(updatedMin * 1000))
                }
                if (updatedMax > 0) {
                    whereB <<= " and o.updated <= :updated_max"
                    opts.put("updated_max", new Date(updatedMax * 1000))
                }
            }

            if (isAdmin) {
                if (params.assigneeId) {
                    joinB <<= " inner join fetch o.assignee assignee "
                    countJoinB <<= " inner join o.assignee assignee "
                    whereB <<= " and assignee.id = :assignee_id "
                    opts.put("assignee_id", Long.parseLong(params.assigneeId))
                } else if (params.assignee) {
                    joinB <<= " inner join fetch o.assignee assignee "
                    countJoinB <<= " inner join o.assignee assignee "
                    whereB <<= " and assignee.name like concat('%', :assignee_name, '%') "
                    opts.put("assignee_name", params.assignee)
                }
            }

            if (params.offerType) {
                joinB <<= " inner join o.offerTypes type "
                countJoinB <<= " inner join o.offerTypes type "
                whereB <<= " and type.id = :offer_type "
                opts.put("offer_type", Long.parseLong(params.offerType))
            }

            if (params."purposes[]") {
                def purposes = params.list("purposes[]").collect {
                    Long.parseLong(it)
                }

                joinB <<= " inner join o.purposes purposes"
                countJoinB <<= "inner join o.purposes purposes "
                whereB <<= " and purposes.id in (:purposes) "
                opts.put("purposes", purposes)
            }

            if (params.counterparty) {
                joinB <<= " inner join fetch o.counterparty counterparty "
                countJoinB <<= " inner join o.counterparty counterparty "
                whereB <<= " and counterparty.name like concat('%', :counterparty_name ,'%') "
                opts.put("counterparty_name", params.counterparty)
            } else if (params.counterpartyId) {
                joinB <<= " inner join fetch o.counterparty counterparty "
                countJoinB <<= " inner join o.counterparty counterparty "
                whereB <<= " and counterparty.id = :counterparty_id "
                opts.put("counterparty_id", Long.parseLong(params.counterpartyId))
            }
        }
        catch (NumberFormatException e) {
            renderError(BAD_REQUEST, e.getMessage())
        }

        def countQuery = "select count(distinct o) from Order o ${countJoinB.toString()} ${whereB.toString()}".toString()
        def count = Order.executeQuery(countQuery, opts)

        def pagination = [:]
        def amount = params.amount != null ? Long.parseLong(params.amount) : 200
        if (amount == 0)
            return [orders: [], size: count[0]]

        if (amount > 0)
            pagination['max'] = amount

        pagination['offset'] = params.offset ?: 0

        def orderType = params.order ? params.order.toLowerCase() : 'desc'
        if (orderType != 'desc' && orderType != 'asc') {
            orderType = 'desc'
        }

        def sort = params.orderBy ? params.orderBy.toLowerCase() : 'id'
        if (!allowedOrderFields.contains(sort))
            sort = 'id'

        if (sort == "counterparty") {
            if (!(params.counterparty || params.counterpartyId))
                joinB <<= " inner join fetch o.counterparty counterparty "
            sort = "counterparty.name"
        } else if (sort == "assignee") {
            if (!(params.assignee || params.assigneeId))
                joinB <<= " inner join fetch o.assignee assignee "
            sort = "assignee.name"
        } else {
            sort = "o." + sort
        }

        def query = "select distinct o from Order o ${joinB.toString()} ${whereB.toString()} order by ${sort} ${orderType}".toString()
        def list = Order.executeQuery(query, opts, pagination)
        return [orders: list, size: count[0]]
    }

    def suggest() {
        def order = Order.findById(params.id)
        if (order == null) {
            renderError(HttpStatus.NOT_FOUND)
            return
        }

        def user = currentUser()
        if (order.assignee.id != user?.id && !isAdmin()) {
            renderError(HttpStatus.NOT_FOUND)
            return
        }

        def qb = new StringBuilder('select distinct op.* from order_proposals op ')
        def jb = new StringBuilder(' inner join realties r on op.rid = r.id ')
        def wb = new StringBuilder(' where op.oid=:order_id ')
        def qp = [order_id: params.id]

        if (params.phone) {
            jb <<= ' left join realty_phones rp on r.id = rp.realty '
            jb <<= ' left join counterparty_phones cp on r.owner = cp.owner '
            if (String.valueOf(params.phone).matches("^8.*")) {
                String filterPhone = String.valueOf(params.phone).replaceAll("^8", "+7")
                wb <<= ' and (rp.phone like concat(\'%\',:phone,\'%\') or cp.phone like concat(\'%\',:phone,\'%\') ' +
                        'or rp.phone like concat(\'%\',:filterPhone,\'%\') or cp.phone like concat(\'%\',:filterPhone,\'%\') ) '
                qp.put('phone', params.phone)
                qp.put('filterPhone', filterPhone)
            } else {
                wb <<= ' and (rp.phone like concat(\'%\',:phone,\'%\') or cp.phone like concat(\'%\',:phone,\'%\')) '
                qp.put('phone', params.phone)
            }
        }

        if (params.fullInfo) {
            jb <<= ' left join (select cr.realty_id, max(cr.comment_id) as last_comment from comments_realties cr group by cr.realty_id) lc on lc.realty_id = r.id left join comments c on c.id=lc.last_comment '
            wb <<= ' and (r.info like concat(\'%\', :fullInfo, \'%\') or c.comment like concat(\'%\',:fullInfo,\'%\')) '
            qp.put('fullInfo', params.fullInfo)
        }

        if (params.state) {
            wb <<= ' and op.state = :state'
            qp.put('state', params.state)
        }

        if (params.address) {
            jb <<= ' inner join addresses a on r.address_id = a.id '
            wb <<= ' and (' +
                    'a.country like concat(\'%\', :address, \'%\') ' +
                    'or a.region like concat(\'%\', :address, \'%\') ' +
                    'or a.city like concat(\'%\', :address, \'%\') ' +
                    'or a.street like concat(\'%\', :address, \'%\') ' +
                    'or a.building like concat(\'%\', :address, \'%\') ' +
                    'or a.room like concat(\'%\', :address, \'%\') ' +
                    ') '
            qp.put('address', params.address)
        }

        if (params.suitability) {
            OrderProposal.Suitability suitability = OrderProposal.Suitability.of(params.suitability)
            switch (suitability) {
                case OrderProposal.Suitability.EXCELLENT:
                    wb <<= ' and op.excellent '
                    break
                case OrderProposal.Suitability.SHAREABLE:
                    wb <<= ' and op.shareable '
                    break
                case OrderProposal.Suitability.BYTOTALSQUARE:
                    wb <<= ' and op.bysquare '
                    break
                case OrderProposal.Suitability.BYPLACE:
                    wb <<= ' and not op.excellent and not op.shareable and not op.bysquare '
                    break
                default:
                    break
            }
        }

        RoomFilter roomFilter = new RoomFilter()
        if (params.room) {
            jb <<= ' inner join rooms on r.id = rooms.realty '
            roomFilter.setOfferType(order.offerTypes.first())

            if (params.room.info != null && params.room.info != "") {
                roomFilter.setInfo(params.room.info)
                wb <<= " and rooms.info like concat('%', :roomInfo, '%')  "
                qp.put('roomInfo', roomFilter.info)
            }

            if (params.room.heatType != null) {
                roomFilter.setHeatType(Room.HeatType.of(params.room.heatType))
                wb <<= ' and rooms.heat_type = :heatType '
                qp.put("heatType", roomFilter.heatType.name.toUpperCase())
                roomFilter.empty = false
            }

            if (params.room.leased != null) {
                roomFilter.setLeased(Boolean.valueOf(params.room.leased))
                wb <<= ' and rooms.leased = :isLeased '
                qp.put("isLeased", roomFilter.leased)
                roomFilter.empty = false
            }

            if (params.room.shareable != null) {
                roomFilter.setShareable(Boolean.valueOf(params.room.shareable))
                wb <<= ' and rooms.shareable = :shareable '
                qp.put("shareable", roomFilter.shareable)
                roomFilter.empty = false
            }

            if (params.room.minimalPart) {
                roomFilter.setMinimalPart(new IndexCommand.DoubleRange())
                if (params.room.minimalPart.min && params.room.minimalPart.max) {
                    roomFilter.minimalPart.min = params.room.minimalPart.min.toDouble()
                    roomFilter.minimalPart.max = params.room.minimalPart.max.toDouble()
                    wb <<= ' and rooms.minimal_part <= :maxPartSize and rooms.square >= :minPartSize '
                    qp.put('minPartSize', roomFilter.minimalPart.min)
                    qp.put('maxPartSize', roomFilter.minimalPart.max)
                    roomFilter.empty = false
                } else if (params.room.minimalPart.min) {
                    roomFilter.minimalPart.min = params.room.minimalPart.min.toDouble()
                    wb <<= ' and rooms.square >= :partSize '
                    qp.put('partSize', roomFilter.minimalPart.min)
                    roomFilter.empty = false
                } else if (params.room.minimalPart.max) {
                    roomFilter.minimalPart.max = params.room.minimalPart.max.toDouble()
                    wb <<= ' and rooms.minimal_part <= :partSize '
                    qp.put('partSize', roomFilter.minimalPart.max)
                    roomFilter.empty = false
                }
            }

            if (params.room.rampantAvailable != null) {
                roomFilter.setRampantAvailable(Boolean.valueOf(params.room.rampantAvailable))
                wb <<= ' and rooms.rampant = :rampantAvailable '
                qp.put("rampantAvailable", roomFilter.rampantAvailable)
                roomFilter.empty = false
            }

            if (params.room.withFurniture != null) {
                roomFilter.setWithFurniture(Boolean.valueOf(params.room.withFurniture))
                wb <<= ' and rooms.furniture = :withFurniture '
                qp.put("withFurniture", roomFilter.withFurniture)
                roomFilter.empty = false
            }

            if (params.room.roomType) {
                roomFilter.setRoomType(Room.RoomType.of(params.room.roomType))
                wb <<= ' and rooms.room_type = :roomType '
                qp.put("roomType", roomFilter.roomType.name.toUpperCase())
                roomFilter.empty = false
            }

            if (params.room.roomPlanType) {
                roomFilter.setRoomPlanType(Room.RoomPlanType.of(params.room.roomPlanType))
                wb <<= ' and rooms.plan_type = :roomPlanType '
                qp.put("roomPlanType", roomFilter.roomPlanType.name.toUpperCase())
                roomFilter.empty = false
            }

            if (params.room.roomCondition) {
                roomFilter.setRoomCondition(Room.RoomCondition.of(params.room.roomCondition))
                wb <<= ' and rooms.remont = :roomCondition '
                qp.put("roomCondition", roomFilter.roomCondition.name.toUpperCase())
                roomFilter.empty = false
            }

            if (params.room.windowType) {
                roomFilter.setWindowType(Room.WindowType.of(params.room.windowType))
                wb <<= ' and rooms.window_type = :windowType '
                qp.put("windowType", roomFilter.windowType.name.toUpperCase())
                roomFilter.empty = false
            }

            if (params.room.square) {
                roomFilter.setSquare(new IndexCommand.DoubleRange())
                if (params.room.square.min) {
                    roomFilter.square.min = Double.valueOf(params.room.square.min)
                    wb <<= ' and rooms.square >= :squareMin '
                    qp.put('squareMin', roomFilter.square.min)
                    roomFilter.empty = false
                }
                if (params.room.square.max) {
                    roomFilter.square.max = Double.valueOf(params.room.square.max)
                    wb <<= ' and rooms.square <= :squareMax '
                    qp.put('squareMax', roomFilter.square.max)
                    roomFilter.empty = false
                }
            }

            if (params.room.floorNumber) {
                roomFilter.setFloorNumber(new IndexCommand.IntegerRange())
                if (params.room.floorNumber.min) {
                    roomFilter.floorNumber.min = Integer.valueOf(params.room.floorNumber.min)
                    wb <<= ' and rooms.floor >= :floorNumberMin '
                    qp.put('floorNumberMin', roomFilter.floorNumber.min)
                    roomFilter.empty = false
                }
                if (params.room.floorNumber.max) {
                    roomFilter.floorNumber.max = Integer.valueOf(params.room.floorNumber.max)
                    wb <<= ' and rooms.floor <= :floorNumberMax '
                    qp.put('floorNumberMax', roomFilter.floorNumber.max)
                    roomFilter.empty = false
                }
            }

            if (params.room.ceilingHeight) {
                roomFilter.setCeilingHeight(new IndexCommand.DoubleRange())
                if (params.room.ceilingHeight.min) {
                    roomFilter.ceilingHeight.min = Double.valueOf(params.room.ceilingHeight.min)
                    wb <<= ' and rooms.height >= :ceilingHeightMin '
                    qp.put('ceilingHeightMin', roomFilter.ceilingHeight.min)
                    roomFilter.empty = false
                }
                if (params.room.ceilingHeight.max) {
                    roomFilter.ceilingHeight.max = Double.valueOf(params.room.ceilingHeight.max)
                    wb <<= ' and rooms.height <= :ceilingHeightMax '
                    qp.put('ceilingHeightMax', roomFilter.ceilingHeight.max)
                    roomFilter.empty = false
                }
            }

            if (params.room.dedicatedElectricPower) {
                roomFilter.setDedicatedElectricPower(new IndexCommand.DoubleRange())
                if (params.room.dedicatedElectricPower.min) {
                    roomFilter.dedicatedElectricPower.min = Double.valueOf(params.room.dedicatedElectricPower.min)
                    wb <<= ' and rooms.electric_power >= :dedicatedElectricPowerMin '
                    qp.put('dedicatedElectricPowerMin', roomFilter.dedicatedElectricPower.min)
                    roomFilter.empty = false
                }
                if (params.room.dedicatedElectricPower.max) {
                    roomFilter.dedicatedElectricPower.max = Double.valueOf(params.room.dedicatedElectricPower.max)
                    wb <<= ' and rooms.electric_power <= :dedicatedElectricPowerMax '
                    qp.put('dedicatedElectricPowerMax', roomFilter.dedicatedElectricPower.max)
                    roomFilter.empty = false
                }
            }

            if (params.room.pricePerSquareMeter) {
                roomFilter.setPricePerSquareMeter(new IndexCommand.PriceFilter())
                if (roomFilter.offerType.id == 2L) {
                    if (params.room.pricePerSquareMeter.min) {
                        roomFilter.pricePerSquareMeter.min = Double.valueOf(params.room.pricePerSquareMeter.min)
                        wb <<= ' and rooms.price_per_meter >= :pricePerSquareMeterMin '
                        qp.put("pricePerSquareMeterMin", roomFilter.pricePerSquareMeter.min)
                        roomFilter.empty = false
                    }
                    if (params.room.pricePerSquareMeter.max) {
                        roomFilter.pricePerSquareMeter.max = Double.valueOf(params.room.pricePerSquareMeter.max)
                        wb <<= ' and rooms.price_per_meter <= :pricePerSquareMeterMax '
                        qp.put("pricePerSquareMeterMax", roomFilter.pricePerSquareMeter.max)
                        roomFilter.empty = false
                    }
                } else {
                    if (params.room.pricePerSquareMeter.min || params.room.pricePerSquareMeter.max) {
                        if (params.room.priceType != null && Room.PriceType.values().name.contains("per" + params.room.priceType)) {
                            qp.put('monthPriceType', "PERMONTH")
                            roomFilter.pricePerSquareMeter.priceType = params.room.priceType
                            if (params.room.pricePerSquareMeter.min) {
                                roomFilter.pricePerSquareMeter.min = Double.valueOf(params.room.pricePerSquareMeter.min)
                                wb <<= ' and rooms.price_per_meter * IF(rooms.price_type = :monthPriceType, 12,1) >= :pricePerSquareMeterMin '
                                qp.put("pricePerSquareMeterMin", roomFilter.pricePerSquareMeter.min *
                                        (roomFilter.pricePerSquareMeter.priceType == Room.PriceType.PERMONTH.nameForPrint() ? 12 : 1)
                                )
                                roomFilter.empty = false
                            }
                            if (params.room.pricePerSquareMeter.max) {
                                roomFilter.pricePerSquareMeter.max = Double.valueOf(params.room.pricePerSquareMeter.max)
                                wb <<= ' and rooms.price_per_meter * IF(rooms.price_type = :monthPriceType, 12,1) <= :pricePerSquareMeterMax '
                                qp.put("pricePerSquareMeterMax", roomFilter.pricePerSquareMeter.max *
                                        (roomFilter.pricePerSquareMeter.priceType == Room.PriceType.PERMONTH.nameForPrint() ? 12 : 1)
                                )
                                roomFilter.empty = false
                            }
                        } else {
                            if (params.room.pricePerSquareMeter.min) {
                                roomFilter.pricePerSquareMeter.min = Double.valueOf(params.room.pricePerSquareMeter.min)
                                wb <<= ' and rooms.price_per_meter >= :pricePerSquareMeterMin '
                                qp.put("pricePerSquareMeterMin", roomFilter.pricePerSquareMeter.min)
                                roomFilter.empty = false
                            }
                            if (params.room.pricePerSquareMeter.max) {
                                roomFilter.pricePerSquareMeter.max = Double.valueOf(params.room.pricePerSquareMeter.max)
                                wb <<= ' and rooms.price_per_meter <= :pricePerSquareMeterMax '
                                qp.put("pricePerSquareMeterMax", roomFilter.pricePerSquareMeter.max)
                                roomFilter.empty = false
                            }
                        }
                    }
                }
            }

            if (params.room.totalPrice) {
                roomFilter.setTotalPrice(new IndexCommand.PriceFilter())
                if (roomFilter.offerType.id == 2L) {
                    if (params.room.totalPrice.min) {
                        roomFilter.totalPrice.min = Double.valueOf(params.room.totalPrice.min)
                        wb <<= ' and rooms.total_price >= :totalPriceMin '
                        qp.put("totalPriceMin", roomFilter.totalPrice.min)
                        roomFilter.empty = false
                    }
                    if (params.room.totalPrice.max) {
                        roomFilter.totalPrice.max = Double.valueOf(params.room.totalPrice.max)
                        wb <<= ' and rooms.total_price <= :totalPriceMax '
                        qp.put("totalPriceMax", roomFilter.totalPrice.max)
                        roomFilter.empty = false
                    }
                } else {
                    if (params.room.totalPrice.min || params.room.totalPrice.max) {
                        if (params.room.priceType != null && Room.PriceType.values().name.contains("per" + params.room.priceType)) {
                            qp.put('monthPriceType', "PERMONTH")
                            roomFilter.totalPrice.priceType = params.room.priceType
                            if (params.room.totalPrice.min) {
                                roomFilter.totalPrice.min = Double.valueOf(params.room.totalPrice.min)
                                wb <<= ' and rooms.total_price * IF(rooms.price_type = :monthPriceType, 12,1) >= :totalPriceMin '
                                qp.put("totalPriceMin", roomFilter.totalPrice.min *
                                        (roomFilter.totalPrice.priceType == Room.PriceType.PERMONTH.nameForPrint() ? 12 : 1)
                                )
                                roomFilter.empty = false
                            }
                            if (params.room.totalPrice.max) {
                                roomFilter.totalPrice.max = Double.valueOf(params.room.totalPrice.max)
                                wb <<= ' and rooms.total_price * IF(rooms.price_type = :monthPriceType, 12,1) <= :totalPriceMax '
                                qp.put("totalPriceMax", roomFilter.totalPrice.max *
                                        (roomFilter.totalPrice.priceType == Room.PriceType.PERMONTH.nameForPrint() ? 12 : 1)
                                )
                                roomFilter.empty = false
                            }
                        } else {
                            if (params.room.totalPrice.min) {
                                roomFilter.totalPrice.min = Double.valueOf(params.room.totalPrice.min)
                                wb <<= ' and rooms.total_price >= :totalPriceMin '
                                qp.put("totalPriceMin", roomFilter.totalPrice.min)
                                roomFilter.empty = false
                            }
                            if (params.room.totalPrice.max) {
                                roomFilter.totalPrice.max = Double.valueOf(params.room.totalPrice.max)
                                wb <<= ' and rooms.total_price <= :totalPriceMax '
                                qp.put("totalPriceMax", roomFilter.totalPrice.max)
                                roomFilter.empty = false
                            }
                        }
                    }
                }
            }
        }

        def orderType = params.order ? params.order.toLowerCase() : 'desc'
        if (orderType != 'desc' || orderType != 'asc')
            if (orderType != 'desc' && orderType != 'asc')
                orderType = 'desc'
        def sort = params.orderBy ? params.orderBy.toLowerCase() : 'id'
        def orderStr

        switch (sort) {
            case 'address':
                if (!params.address)
                    jb <<= ' inner join addresses a on r.address_id = a.id '
                orderStr = " order by a.country ${orderType}, a.region ${orderType}, a.city ${orderType}, " +
                        "a.street ${orderType}, a.building ${orderType}, a.room ${orderType} ".toString()
                break;
            default:
                orderStr = " order by op.state ${orderType} ".toString()
                break;
        }
        def query = "${qb.toString()} ${jb.toString()} ${wb.toString()} GROUP BY op.id ${orderStr};".toString()
        def session = sessionFactory.currentSession
        final SQLQuery sqlQuery = session.createSQLQuery(query)
        def orderProposals = sqlQuery.with {
            addEntity(OrderProposal)
            qp.each { k, v -> setParameter(k, v) }
            list()
        }
//        log.debug("query params - {}", qp)
//        System.out.println("roomFilter - ${roomFilter}")

        return [proposals: fetchLastComments(orderProposals), roomFilter: roomFilter]
    }

    private List<OrderProposalWithLastComment> fetchLastComments(List<OrderProposal> proposals) {
        def realtyComments = []
        def realtyIds = proposals.collect { it.realty.id }
        if (!realtyIds.empty) {
            def query = "select realty_id, max(comment_id) as last_comment from comments_realties where realty_id in (:list) group by realty_id;"
            def session = sessionFactory.currentSession
            final SQLQuery sqlQuery = session.createSQLQuery(query)
            realtyComments.addAll(sqlQuery.with {
                setParameterList('list', realtyIds)
                list()
            })
        }

        if (realtyComments.empty)
            return proposals.collect { new OrderProposalWithLastComment(orderProposal: it) }

        def commentsIds = realtyComments.collect {
            it[1]
        }

        def comments = Comment.findAllByIdInList(commentsIds).collectEntries {
            [(it.id), it]
        }

        Map<Long, Comment> realtiesToComments = realtyComments.collectEntries {
            [((Long) it[0]): comments.get((Long) it[1])]
        }
        return proposals.collect {
            new OrderProposalWithLastComment(orderProposal: it, comment: realtiesToComments[(it.realty.id)])
        }
    }

    @Transactional
    def updateSuggest() {
        def order = Order.findById(params.id)
        def user = currentUser()
        log.debug("current user - {}", user.id)
        log.debug("order assignee - {}", order.assignee.id)
        if (order == null || order.assignee.id != user.id && !user.isAdmin()) {
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }
//        deleteUnprocessedProposals(order)
        updateSuitability(order)
        def oldProposals = OrderProposal.countByOrder(order)
        Sql sql = Sql.newInstance(dataSource)
        createSuggests(order, sql)
        def newProposals = OrderProposal.countByOrder(order)
        render([
                before: oldProposals,
                after : newProposals
        ] as JSON)
    }

    @Transactional
    def update() {
        def order = Order.findById(Long.valueOf(params.id) ?: -1)
        def user = currentUser()
        def isAdmin = user.isAdmin()
        if (order == null) {
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }

        if (!isAdmin && order.assignee.id != user.id) {
            renderError(NOT_FOUND)
            return
        }

        def data = readJSON()
        if (!data)
            return

        boolean shouldUpdateSuitability = false
        if (isAdmin) {
            def assignee = User.findById(Long.valueOf(data.assignee ?: "-1") ?: -1)
//        if (assignee == null) {
//            renderError(HttpStatus.NOT_FOUND, messageSource.getMessage('user.not.found', null, Locale.default))
//            return
//        }
            def counterparty = Counterparty.findById(Long.valueOf(data.counterparty) ?: -1)
            if (counterparty == null) {
                renderError(NOT_FOUND, message('counterparty.not.found'))
                return
            }

            if (assignee != order.assignee) {
                order.setAssignee(assignee)
            }
            if (counterparty != order.counterparty) {
                order.setCounterparty(counterparty)
            }

            if (data.maxPrice != null) {
                def maxPrice = Double.valueOf(data.maxPrice)
                if (maxPrice != null) {
                    if (maxPrice != order.maxPrice) {
                        order.setMaxPrice(maxPrice)
                    }
                }
            } else {
                order.setMaxPrice(null)
            }

            if (data.minPrice != null) {
                def minPrice = Double.valueOf(data.minPrice)
                if (minPrice != null) {
                    if (minPrice != order.minPrice) {
                        order.setMinPrice(minPrice)
                    }
                }
            } else {
                order.setMinPrice(null)
            }

            if (data.maxPricePerMeter != null) {
                def maxPricePerMeter = Double.valueOf(data.maxPricePerMeter)
                if (maxPricePerMeter != null) {
                    if (maxPricePerMeter != order.maxPricePerMeter) {
                        order.setMaxPricePerMeter(maxPricePerMeter)
                    }
                }
            } else {
                order.setMaxPricePerMeter(null)
            }

            if (data.minPricePerMeter != null) {
                def minPricePerMeter = Double.valueOf(data.minPricePerMeter)
                if (minPricePerMeter != null) {
                    if (minPricePerMeter != order.minPricePerMeter) {
                        order.setMinPricePerMeter(minPricePerMeter)
                    }
                }
            } else {
                order.setMinPricePerMeter(null)
            }

            if (data.maxSquare != null) {
                def maxSquare = Double.valueOf(data.maxSquare)
                if (maxSquare != null) {
                    if (maxSquare != order.maxSquare) {
                        order.setMaxSquare(maxSquare)
                        shouldUpdateSuitability = true
                    }
                }
            } else {
                if (order.maxSquare != null) {
                    order.setMaxSquare(null)
                    shouldUpdateSuitability = true
                }
            }

            if (data.priceType != null) {
                def priceTypeString = String.valueOf(data.priceType)
                if (Room.PriceType.values().name.contains("per" + priceTypeString)) {
                    order.setPriceType(Room.PriceType.of(priceTypeString))
                } else {
                    order.setPriceType(null)
                }
            } else {
                order.priceType = null
            }

            if (data.minSquare != null) {
                def minSquare = Double.valueOf(data.minSquare)
                if (minSquare != null) {
                    if (minSquare != order.minSquare) {
                        order.setMinSquare(minSquare)
                        shouldUpdateSuitability = true
                    }
                }
            } else {
                if (order.minSquare != null) {
                    order.setMinSquare(null)
                    shouldUpdateSuitability = true
                }
            }

            def info = data.info
            if (info != order.info) {
                order.setInfo(info)
            }

            def regionSuggestions = data.regionSuggestions
            if (regionSuggestions != order.regionSuggestions) {
                order.setRegionSuggestions(regionSuggestions)
            }

            def realtySuggestions = data.realtySuggestions
            if (realtySuggestions != order.realtySuggestions) {
                order.setRealtySuggestions(realtySuggestions)
            }

            def purposes = data.purposes ? Arrays.asList(data.purposes.toArray()) : null
            order.resetPurposes(purposes)

//            def offerTypes = data.offerTypes ? Arrays.asList(data.offerTypes.toArray()) : null
//            order.resetOfferTypes(offerTypes)
        }

        def now = new Date()
//        order.setUpdated(now)

        boolean needUpdateProposals = false
        if (data.regionChanged) {
            needUpdateProposals = true
            MultiPolygon polygon = null
            if (data.region) {
                try {
                    polygon = GeometryUtils.parseMultipolygon(data.region)
                } catch (Exception e) {
                    renderError(HttpStatus.BAD_REQUEST)
                    return
                }
            } else {
                polygon = GeometryUtils.DEFAULT_POLYGON
            }
            order.setRegion(polygon)
        }
        order.validate()
        if (order.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(order.errors.fieldErrors)
            return
        }
        order.save(failOnError: true, flush: true)
        if (needUpdateProposals) {
            deleteUnprocessedProposals(order)
            if (order.region != GeometryUtils.DEFAULT_POLYGON) {
                def sql = Sql.newInstance(dataSource)
                createSuggests(order, sql)
            }
        }
        if (shouldUpdateSuitability) {
            updateSuitability(order)
        }
        response.status = 200
        return [order: order]
    }

    @Transactional
    def save() {
        if (!isAdmin()) {
            response.setStatus(403)
            return
        }
        def data = readJSON()
        if (!data)
            return

        def assigneeId = Long.valueOf(data.assignee ?: "-1")
        def counterpartyId = Long.valueOf(data.counterparty)
        def maxPrice = data.maxPrice != null ? Double.valueOf(data.maxPrice) : null
        def minPrice = data.minPrice != null ? Double.valueOf(data.minPrice) : null
        def maxPricePerMeter = data.maxPricePerMeter != null ? Double.valueOf(data.maxPricePerMeter) : null
        def minPricePerMeter = data.minPricePerMeter != null ? Double.valueOf(data.minPricePerMeter) : null
        def priceType = data.priceType != null && Room.PriceType.values().name.contains("per" + data.priceType) ? Room.PriceType.of(data.priceType) : null

        def maxSquare = data.maxSquare != null ? Double.valueOf(data.maxSquare) : null
        def minSquare = data.minSquare != null ? Double.valueOf(data.minSquare) : null



        def info = data.info
        def now = new Date()
        def stateString = data.state
        def state = stateString ? Order.State.valueOf(stateString) : Order.State.OPEN
        def assignee = User.findById(assigneeId)
        def counterparty = Counterparty.findById(counterpartyId)

        MultiPolygon polygon = null
        if (data.region) {
            try {
                polygon = GeometryUtils.parseMultipolygon(data.region)
            } catch (Exception e) {
                response.status = 400
                return
            }
        } else {
            polygon = GeometryUtils.DEFAULT_POLYGON
        }

        def purposes = []
        def jsonTypes = data.purposes
        if (jsonTypes && jsonTypes.size() > 0) {
            purposes = jsonTypes.toArray().collect {
                return RealtyPurposeType.findById(it)
            }
        }

        def offerTypes = []
        if (data.offerTypes) {
            def offerOrderTypeIds = data.offerTypes.toArray().collect {
                return it
            }

            offerTypes.addAll(OfferType.findAllByIdInList(offerOrderTypeIds))
            if (offerTypes.size() != offerOrderTypeIds.size()) {
                renderFieldErrors([new FieldError(Order.class.name, "offerTypes", message("order.offerType.notFound"))])
                return
            }
        }

        def order = new Order(
                info: info ?: null,
                regionSuggestions: data.regionSuggestions ?: null,
                realtySuggestions: data.realtySuggestions ?: null,
                assignee: assignee ?: null,
                counterparty: counterparty ?: null,
                created: now,
                updated: now,
                state: state,
                maxPrice: maxPrice,
                minPrice: minPrice,
                maxPricePerMeter: maxPricePerMeter,
                minPricePerMeter: minPricePerMeter,
                priceType: priceType,
                maxSquare: maxSquare,
                minSquare: minSquare,
                region: polygon,
                offerTypes: offerTypes,
                purposes: purposes
        )

        order.validate()
        if (order.hasErrors()) {
            transactionStatus.setRollbackOnly()
            def errors = new ArrayList<FieldError>(order.errors.fieldErrors)
            renderFieldErrors(errors)
            return
        }
        order.save(failOnError: true, flush: true)

        if (order.region != GeometryUtils.DEFAULT_POLYGON) {
            def sql = Sql.newInstance(dataSource)
            createSuggests(order, sql)
        }

        response.status = 201
        return [order: order]
    }

    def show() {
        def order = Order.findById(params.id)

        // вернуть 404, если нет разрешен доступ до него или заказ не существует
        def user = currentUser()
        if (order == null || order.assignee != user && !user.isAdmin()) {
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }
        def comments = order.comments
                .sort { firstElem, secondElem -> secondElem.created <=> firstElem.created }
                .subList(0, Math.min(1, order.comments.size()))
        return [o: new OrderWithLastComments(order: order, comments: comments)]
    }

    @Transactional
    def complete() {
        Long id = Long.valueOf(params.id ?: -1)
        def order = Order.findById(id)

        // вернуть 404, если нет разрешен доступ до него или заказ не существует
        def user = currentUser()
        if (order == null || order.assignee != user && !user.isAdmin()) {
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }

        order.setState(Order.State.CLOSED)
        order.validate()
        if (order.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(order.errors.fieldErrors)
            return
        }
        order.save(failOnError: true, flush: true)
        response.status = 200
        return [order: order]
    }

    @Transactional
    def discard() {
        Long id = Long.valueOf(params.id ?: -1)
        def order = Order.findById(id)

        // вернуть 404, если нет разрешен доступ до него или заказ не существует
        def user = currentUser()
        if (order == null || order.assignee != user && !user.isAdmin()) {
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }

        order.setState(Order.State.REJECTED)
        order.validate()
        if (order.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(order.errors.fieldErrors)
            return
        }
        order.save(failOnError: true, flush: true)
        response.status = 200
        return [order: order]
    }

    @Transactional
    def reopen() {
        Long id = Long.valueOf(params.id ?: -1)
        def order = Order.findById(id)

        // вернуть 404, если нет разрешен доступ до него или заказ не существует
        def user = currentUser()
        if (order == null || order.assignee != user && !user.isAdmin()) {
            renderError(NOT_FOUND, message('order.not.found'))
            return
        }

        order.setState(Order.State.OPEN)
        order.validate()
        if (order.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(order.errors.fieldErrors)
            return
        }
        order.save(failOnError: true, flush: true)
        response.status = 200
        return [order: order]
    }

    @Transactional
    def updateRealty() {
        def data = readJSON()
        if (!data)
            return

        def orderProposal = OrderProposal.findById(Long.valueOf(params.pid))

        // вернуть 404, если нет разрешен доступ до него или заказ не существует
        def user = currentUser()
        if (orderProposal == null || orderProposal.order.assignee != user && !user.isAdmin()) {
            renderError(NOT_FOUND, message('orderproposal.not.found'))
            return
        }

        def state = OrderProposal.State.valueOf(String.valueOf(data.state).toUpperCase())
        orderProposal.state = state
        orderProposal.validate()
        if (orderProposal.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(orderProposal.errors.fieldErrors)
            return
        }
        orderProposal.save(failOnError: true, flush: true)
        response.status = 200
        def comments = orderProposal.realty.comments.sort {
            a, b -> b.created <=> a.created
        }
        def comment = comments.isEmpty() ? null : comments.first()
        return [op: new OrderProposalWithLastComment(orderProposal: orderProposal, comment: comment)]
    }

    @Transactional
    private void createSuggests(Order order, Sql sql) {
        Date now = new Date()
        final session = sessionFactory.currentSession
        def ids = session.createSQLQuery("select distinct r.id from addresses " +
                "a join realties r on a.id = r.address_id " +
                "where r.id not in (SELECT rid from order_proposals where oid = :order_id1) " +
                "and r.id in (SELECT realty_id from realty_purposes r INNER JOIN order_purposes o " +
                "ON r.realty_purpose_type_id = o.realty_purpose_type_id WHERE o.order_id = :order_id2) " +
                "and r.id in (SELECT realty_id from realty_offer_types r INNER JOIN order_offer_types o " +
                "ON r.offer_type = o.offer_type WHERE o.order_id = :order_id3) " +
                "and ST_Contains((SELECT region from orders where id = :order_id4), a.location);"
        ).with {
            setLong('order_id1', order.id)
            setLong('order_id2', order.id)
            setLong('order_id3', order.id)
            setLong('order_id4', order.id)
            list()
        } as List
        List<Realty> results
        if (ids.isEmpty())
            results = []
        else {
            results = Realty.findAllByIdInList(ids as List)
            results = results.findAll {
                GeometryUtils.pointInsideMultipolygon(it.address.location, order.region)
            }
        }

        sql.withBatch(
                "INSERT INTO order_proposals (oid,rid,created,updated,state, version, excellent, shareable, bysquare) " +
                        "VALUES (${order.id}, ?, ?, ?, \"UNPROCESSED\",1, ?, ?, ?);"
        ) { ps ->
            results.forEach {
                boolean excellent = false
                boolean shareable = false
                boolean byTotalSquare = false

                if (!it.rooms.isEmpty()) {
                    if (it.rooms.find { Room room ->
                        (order.minSquare == null || order.minSquare <= room.square) &&
                                (order.maxSquare == null || order.maxSquare >= room.square)
                    } != null) {
                        excellent = true
                    }
                    if (it.rooms.find { Room room ->
                        room.shareable && (order.maxSquare == null || room.minimalPart <= order.maxSquare)
                    } != null) {
                        shareable = true
                    }
                    if (order.minSquare == null || it.rooms.groupBy {
                        it.floorNumber
                    }.entrySet().collect {
                        it.value.square.sum() ?: BigDecimal.ZERO
                    }.max() >= order.minSquare) {
                        byTotalSquare = true
                    }
                }
                ps.addBatch([it.id, now, now, excellent, shareable, byTotalSquare])
            }
        }

//        sql.withBatch("INSERT INTO order_proposals (oid,rid,created,updated,state, version) " +
//                "SELECT ?, id, ?, ?, \"UNPROCESSED\",1 from realties " +
//                "WHERE id not in (SELECT rid from order_proposals WHERE oid = ?) " +
//                "AND id in (select r.id from addresses a join realties r on a.id = r.address_id " +
//                "where ST_Contains((SELECT region from orders where id = ?), a.location));"
//        ) { ps ->
//            ps.addBatch([order.id, now, now, order.id, order.id])
//        }
    }

    private static void updateSuitability(Order order) {
        def query = 'from OrderProposal op where op.order.id = :order_id and (op.realty.updated > op.updated or op.order.updated > op.updated)'
        Collection<OrderProposal> proposals = new ArrayList<>()
        def paramsToSelect = [order_id: order.id]
        proposals.addAll(OrderProposal.executeQuery(query, paramsToSelect))

        proposals.forEach {
            boolean excellent = false
            boolean shareable = false
            boolean byTotalSquare = false
            if (!it.realty.rooms.isEmpty()) {
                if (it.realty.rooms.find { Room room ->
                    (order.minSquare == null || order.minSquare <= room.square) &&
                            (order.maxSquare == null || order.maxSquare >= room.square)
                } != null) {
                    excellent = true
                }
                if (it.realty.rooms.find { Room room ->
                    room.shareable && (order.maxSquare == null || room.minimalPart <= order.maxSquare)
                } != null) {
                    shareable = true
                }
                if (order.minSquare == null || it.realty.rooms.groupBy {
                    it.floorNumber
                }.entrySet().collect {
                    it.value.square.sum() ?: BigDecimal.ZERO
                }.max() >= order.minSquare) {
                    byTotalSquare = true
                }
            }
            it.setExcellent(excellent)
            it.setShareable(shareable)
            it.setByTotalSquare(byTotalSquare)
            it.save(failOnError: true, flush: true)
        }
    }

    private static void deleteUnprocessedProposals(Order order) {
        def deleted = OrderProposal.executeUpdate("delete OrderProposal p where p.order.id = :orderId and p.state = :state",
                [orderId: order.id, state: OrderProposal.State.UNPROCESSED]);
        log.debug("Deleted {} proposals for and roder #{}", deleted, order.id)
    }

    private static void deleteAllProposals(Order order) {
        def deleted = OrderProposal.executeUpdate("delete OrderProposal p where p.order.id = :orderId",
                [orderId: order.id]);
        log.debug("Deleted {} proposals for and roder #{}", deleted, order.id)
    }

    private User currentUser() {
        return springSecurityService.currentUser as User;
    }

    private boolean isAdmin() {
        return currentUser().isAdmin()
    }
}

public class OrderProposalWithLastComment {
    public OrderProposal orderProposal
    public Comment comment
}

public class OrderWithLastComments {
    public Order order
    public ArrayList<Comment> comments
}

public class OrderIdToCommentId {
    Long orderId
    Long commentId
}
