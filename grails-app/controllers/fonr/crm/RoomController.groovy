package fonr.crm

import fonr.crm.controllers.cmd.room.IndexCommand
import fonr.crm.controllers.cmd.room.SaveCommand
import fonr.crm.exceptions.service.image.DirectoryNotFoundException
import fonr.crm.exceptions.service.image.EmptyDataException
import fonr.crm.exceptions.service.image.IllegalContentTypeException
import fonr.crm.exceptions.service.image.UnknownException
import grails.transaction.Transactional
import org.hibernate.SQLQuery
import org.hibernate.type.IntegerType
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.transaction.TransactionStatus
import org.springframework.web.multipart.support.AbstractMultipartHttpServletRequest

import static org.springframework.http.HttpStatus.*

class RoomController implements ControllerUtils {

    private static def log = LoggerFactory.getLogger(getClass())

    RoomController() {
    }

    static allowedMethods = [index: "GET", save: "PUT", show: "GET", update: "PUT", delete: "DELETE", uploadImage: ['POST', 'PUT']]

    def dataSource
    def sessionFactory
    def springSecurityService
    def index(IndexCommand command) {
//        log.error("command: totalPrice: {}, {}, {}",command.totalPrice.min, command.totalPrice.max, command.totalPrice.priceType)
        if(params.id != null){
            Realty realty = Realty.findById(params.id)
            if(realty == null){
                renderError(HttpStatus.NOT_FOUND)
                return
            }
            command.setRealty(realty.id)
            command.validate()
        }
        if (command.hasErrors()) {
            renderFieldErrors(command.errors.fieldErrors)
            return
        }

        def joinb = new StringBuilder(" ")
        def countJoinb = new StringBuilder(" ")
        def whereb = new StringBuilder(" where 1 = 1 ")
        def orderb = new StringBuilder(" order by ")
        def opts = [:]

        if (command.created) {
            if ((command.created.min ?: -1) > 0) {
                whereb <<= " and r.created >= :created_min  "
                opts.put('created_min', new Date(command.created.min * 1000))
            }

            if ((command.created.max ?: -1) > 0) {
                whereb <<= " and r.created <= :created_max  "
                opts.put('created_max', new Date(command.created.max * 1000))
            }
        }

        if (command.updated) {
            if ((command.updated.min ?: -1) > 0) {
                whereb <<= " and r.updated >= :updated_min  "
                opts.put('updated_min', new Date(command.updated.min * 1000))
            }

            if ((command.updated.max ?: -1) > 0) {
                whereb <<= " and r.updated <= :updated_max  "
                opts.put('updated_max', new Date(command.updated.max * 1000))
            }
        }


        if (command.realty){
            whereb <<= " and r.realty = :realty  "
            opts.put('realty', command.realty)
        }

        if (command.info){
            whereb <<= " and r.info like concat('%', :info, '%')  "
            opts.put('info', command.info)
        }

        if (command.leased != null) {
            whereb <<= " and r.leased = :isLeased  "
            opts.put('isLeased', command.leased)
        }

        if (command.floorNumber){
            whereb <<= " and r.floor like concat('%', :floorNumber, '%') "
            opts.put('floorNumber', command.floorNumber)
        }

        if (command.shareable != null) {
            whereb <<= " and r.shareable = :shareable "
            opts.put('shareable', command.shareable)
        }

        if (command.minimalPart != null) {
            if (command.minimalPart.min != null) {
                whereb <<= " and r.minimal_part >= :minimalPart  "
                opts.put('minimalPart', command.minimalPart.min)
            }
            if (command.minimalPart.max != null) {
                whereb <<= " and r.minimal_part <= :minimalPart  "
                opts.put('minimalPart', command.minimalPart.max)
            }
        }


        if (command.heatType != null) {
            whereb <<= " and r.heat_type = :heatType "
            opts.put('heatType', Room.HeatType.of(command.heatType).name.toUpperCase())
        }

        if (command.rampantAvailable != null){
            whereb <<= " and r.rampant = :rampantAvailable  "
            opts.put('rampantAvailable', command.rampantAvailable)
        }

        if (command.withFurniture != null){
            whereb <<= " and r.furniture = :withFurniture  "
            opts.put('withFurniture', command.withFurniture)
        }

        if(command.square != null){
            if(command.square.min != null){
                whereb <<= " and r.square >= :squareMin  "
                opts.put('squareMin', command.square.min)
            }
            if(command.square.max != null){
                whereb <<= " and r.square <= :squareMax  "
                opts.put('squareMax', command.square.max)
            }
        }

        if(command.ceilingHeight != null){
            if(command.ceilingHeight.min != null){
                whereb <<= " and r.height >= :ceilingHeightMin  "
                opts.put('ceilingHeightMin', command.ceilingHeight.min)
            }
            if(command.ceilingHeight.max != null){
                whereb <<= " and r.height <= :ceilingHeightMax  "
                opts.put('ceilingHeightMax', command.ceilingHeight.max)
            }
        }


        if (command.pricePerSquareMeter != null) {
            if (command.pricePerSquareMeter.min != null) {
                whereb <<= " and r.price_per_meter  >= :pricePerSquareMeterMin "
                opts.put('pricePerSquareMeterMin', command.pricePerSquareMeter.min)
            }
            if (command.pricePerSquareMeter.max != null) {
                whereb <<= " and r.price_per_meter  <= :pricePerSquareMeterMax "
                opts.put('pricePerSquareMeterMax', command.pricePerSquareMeter.max)
            }
        }

        if (command.totalPrice != null) {
            if (command.totalPrice.min != null) {
                whereb <<= " and r.total_price  >= :totalPriceMin "
                opts.put('totalPriceMin', command.totalPrice.min)
            }
            if (command.totalPrice.max != null) {
                whereb <<= " and r.total_price  <= :totalPriceMax "
                opts.put('totalPriceMax', command.totalPrice.max)
            }
        }

        if (command.priceType != null) {
            whereb <<= " and r.price_type  = :priceType "
            opts.put('priceType', Room.PriceType.of(command.priceType).name.toUpperCase())
        }

//        if (command.pricePerSquareMeter != null) {
//            if (command.pricePerSquareMeter.min != null) {
//                opts.put('monthPriceType', "PERMONTH")
//                whereb <<= " and IF(r.price_type = :monthPriceType, 12,1)*r.price_per_meter  >= :pricePerSquareMeterMin  "
//                if (command.pricePerSquareMeter.priceType == Room.PriceType.PERYEAR.name) {
//                    opts.put('pricePerSquareMeterMin', command.pricePerSquareMeter.min)
//                } else /*if (command.pricePerSquareMeter.priceType == Room.PriceType.PERMONTH.name)*/ {
//                    opts.put('pricePerSquareMeterMin', command.pricePerSquareMeter.min * 12)
//                }
//            }
//            if (command.pricePerSquareMeter.max != null) {
//                opts.put('monthPriceType', "PERMONTH")
//                whereb <<= " and IF(r.price_type = :monthPriceType, 12,1)*r.price_per_meter  <= :pricePerSquareMeterMax  "
//                if (command.pricePerSquareMeter.priceType == Room.PriceType.PERYEAR.name) {
//                    opts.put('pricePerSquareMeterMax', command.pricePerSquareMeter.max)
//                } else /*if (command.pricePerSquareMeter.priceType == Room.PriceType.PERMONTH.name) */ {
//                    opts.put('pricePerSquareMeterMax', command.pricePerSquareMeter.max * 12)
//                }
//            }
//        }
//
//        if (command.totalPrice != null) {
//            if (command.totalPrice.min != null) {
//                opts.put('monthPriceType', "PERMONTH")
//                whereb <<= " and IF(r.price_type = :monthPriceType, 12,1)*r.total_price >= :totalPriceMin  "
//                Room.PriceType priceType = Room.PriceType.of(command.totalPrice.priceType)
//                if (priceType == Room.PriceType.PERYEAR) {
//                    opts.put('totalPriceMin', command.totalPrice.min)
//                } else {
//                    opts.put('totalPriceMin', command.totalPrice.min * 12)
//                }
//            }
//            if (command.totalPrice.max != null) {
//                opts.put('monthPriceType', "PERMONTH")
//                whereb <<= " and IF(r.price_type = :monthPriceType, 12,1)*r.total_price <= :totalPriceMax  "
//                Room.PriceType priceType = Room.PriceType.of(command.totalPrice.priceType)
//                if (priceType == Room.PriceType.PERYEAR) {
//                    opts.put('totalPriceMax', command.totalPrice.max)
//                } else {
//                    opts.put('totalPriceMax', command.totalPrice.max * 12)
//                }
//            }
//        }

        if(command.dedicatedElectricPower != null){
            if(command.dedicatedElectricPower.min != null){
                whereb <<= " and r.electric_power >= :dedicatedElectricPowerMin  "
                opts.put('dedicatedElectricPowerMin', command.dedicatedElectricPower.min)
            }
            if(command.dedicatedElectricPower.max != null){
                whereb <<= " and r.electric_power <= :dedicatedElectricPowerMax  "
                opts.put('dedicatedElectricPowerMax', command.dedicatedElectricPower.max)
            }
        }

        if(command.roomType){
            whereb <<= " and r.room_type = :roomType  "
            opts.put('roomType', Room.RoomType.of(command.roomType).name.toUpperCase())
        }

        if(command.roomPlanType){
            whereb <<= " and r.plan_type = :roomPlanType  "
            opts.put('roomPlanType', Room.RoomPlanType.of(command.roomPlanType).name.toUpperCase())
        }

        if(command.roomCondition){
            whereb <<= " and r.remont = :roomCondition  "
            opts.put('roomCondition', Room.RoomCondition.of(command.roomCondition).name.toUpperCase())
        }

        if(command.windowType){
            whereb <<= " and r.window_type = :windowType  "
            opts.put('windowType', Room.WindowType.of(command.windowType).name.toUpperCase())
        }


        def countQuery = "select count(distinct r.id) as number from rooms r ${countJoinb.toString()} ${whereb.toString()}".toString()
//        log.error("CountOpts = {}", opts)
//        log.error("sqlCountQuery - {}", countQuery)
        def session = sessionFactory.currentSession
        //toso: change to sql
        final SQLQuery countSqlQuery = session.createSQLQuery(countQuery)
        def count = countSqlQuery.with {
            opts.forEach { k, v -> setParameter(k, v) }
            addScalar('number', IntegerType.INSTANCE)
            uniqueResult()
        } as Integer

        def amount = command.amount ?: 200
        if (amount == 0)
            return [rooms: [], size: count]

        def offset = command.offset ?: 0
        opts.put('amount', amount)
        opts.put('offset', offset)

        def orderType = params.order ? params.order.toLowerCase() : 'desc'
        if (orderType != 'desc' && orderType != 'asc') {
            orderType = 'desc'
        }

        def sort = command.orderBy ? command.orderBy.toLowerCase() : 'id'

        if (sort == 'totalprice'){
            orderb.append(" r.total_price ").append(orderType)
//            opts.put('monthPriceType', "PERMONTH")
//            orderb.append(" IF(r.price_type=:monthPriceType,12,1)*r.total_price ")
//                    .append(orderType)
        }else if(sort == 'pricepersquaremeter') {
            orderb.append(" r.price_per_meter ").append(orderType)
//            opts.put('monthPriceType', "PERMONTH")
//            orderb.append(" IF(r.price_type=:monthPriceType,12,1)*r.price_per_meter ")
//                    .append(orderType)
        } else {
            //todo: если добавится сортировка по еще каким-то полям, то поменять
            orderb.append("r.")
                    .append(sort)
                    .append(" ")
                    .append(orderType)
        }

        def query = "select r.* from rooms r ${joinb.toString()} ${whereb.toString()} ${orderb.toString()} limit :offset, :amount".toString()
//        log.error("Opts - {}", opts)
//        log.error("sqlQuery - {}", query)
        final SQLQuery sqlQuery = session.createSQLQuery(query)
        def rooms = sqlQuery.with {
            addEntity(Room)
            opts.each { k, v -> setParameter(k, v) }
            list()
        }

        return [rooms: rooms, size: count]
    }

    @Transactional
    def save(SaveCommand cmd) {
//        if (!isAdmin()) {
//            response.setStatus(403)
//            return
////        }
//        SaveCommand cmd = new SaveCommand()
//        def json = readJSON()
//        if (!json) {
//            return
//        }
//        bindData(cmd, json)
        Realty realty = null
        if(params.id != null){
            realty = Realty.findById(params.id)
            if(realty == null){
                renderError(HttpStatus.NOT_FOUND)
                return
            }
            cmd.setRealty(realty.id)
            cmd.validate()
        }
//        println(cmd as JSON)

        if (cmd.hasErrors()) {
            renderFieldErrors(cmd.errors.fieldErrors)
            return
        }
        realty = Realty.findById(cmd.realty)
        def images = []
        if (cmd.images && !cmd.images.empty) {
            def mainImageId = null
            def arrayIds = cmd.images.collect {
                if (it.main ?: 0 == 1) {
                    mainImageId = it.id
                }
                it.id
            }
            images.addAll(RoomImage.findAllByIdInList(arrayIds))
            def mainImage = images.find { it.id == mainImageId }
            mainImage.isMain = true
            mainImage.save()
        }
        Date now = new Date()

        Room room = new Room(
                realty: realty,
                info: cmd.info,
                isLeasedOut: cmd.leased,
                shareable: cmd.shareable,
                minimalPart: cmd.minimalPart,
                square: cmd.square,
                ceilingHeight: cmd.ceilingHeight,
                floorNumber: Integer.valueOf(cmd.floorNumber),
                pricePerSquareMeter: cmd.pricePerSquareMeter,
                totalPrice: cmd.pricePerSquareMeter * cmd.square,
                priceType: cmd.priceType != null ? Room.PriceType.of(cmd.priceType) : null,
                roomType: Room.RoomType.of(cmd.roomType),
                heatType: cmd.heatType != null ? Room.HeatType.of(cmd.heatType) : null,
                rampantAvailable: cmd.rampantAvailable,
                dedicatedElectricPower: cmd.dedicatedElectricPower,
                roomPlanType: cmd.roomPlanType != null ? Room.RoomPlanType.of(cmd.roomPlanType) : null,
                roomCondition: cmd.roomCondition != null ? Room.RoomCondition.of(cmd.roomCondition) : null,
                windowType: cmd.windowType != null ? Room.WindowType.of(cmd.windowType) : null,
                withFurniture: cmd.withFurniture,
                images: images,
                created: now,
                updated: now
        )
        room.validate()
        if(room.hasErrors()){
            transactionStatus.setRollbackOnly()
            renderFieldErrors(room.errors.fieldErrors)
            return
        }
        room.save flush: true
//        realty.setUpdated(new Date())
//        realty.save flush:true

        RealtyController.updateSuitabilityForRealty(realty)
        return [room: room]
    }

    @Transactional
    def update(SaveCommand cmd) {
//        if (!isAdmin()) {
//            response.setStatus(403)
//            return
//        }

        long id = -1
        try {
            id = Long.parseLong(params.id)
        } catch (NumberFormatException e) {
            log.warn('update realty', e)
        }

        Room room = Room.findById(id)
        if (room == null) {
            renderError(HttpStatus.NOT_FOUND)
            return
        }

        if (cmd.hasErrors()) {
            renderFieldErrors(cmd.errors.fieldErrors)
            return
        }

        def mainImage = null
        !room.resetImages(cmd.images ? cmd.images.collect {
            if (it.main ?: 0 == 1)
                mainImage = it.id
            it.id
        } : null)

        if (mainImage != null)
            room.resetMainImage(mainImage)

        def realty = Realty.findById(cmd.realty)
        Realty oldRealty = null
        Boolean shouldUpdateObjects = false
        if (realty != room.realty) {
            oldRealty = room.realty
            room.setRealty(realty)
            shouldUpdateObjects = true
        }
        room.setInfo(cmd.info ?: null)
        if (cmd.square != room.square) {
            room.setSquare(cmd.square)
            shouldUpdateObjects = true
        }
        room.setIsLeasedOut(cmd.leased)
        room.setSquare(cmd.square)
        room.setCeilingHeight(cmd.ceilingHeight)
        if (cmd.floorNumber != room.floorNumber) {
            room.setFloorNumber(cmd.floorNumber)
            shouldUpdateObjects = true
        }
        if (cmd.shareable != room.shareable) {
            shouldUpdateObjects = true
        }
        room.setShareable(cmd.shareable)
        if (cmd.minimalPart != room.minimalPart) {
            shouldUpdateObjects = true
        }
        room.setMinimalPart(cmd.minimalPart)
        room.setPricePerSquareMeter(cmd.pricePerSquareMeter)
        room.setTotalPrice(cmd.square * cmd.pricePerSquareMeter)
        room.setPriceType(cmd.priceType != null ? Room.PriceType.of(cmd.priceType) : null)
        room.setRoomType(Room.RoomType.of(cmd.roomType))
        room.setHeatType(cmd.heatType != null ? Room.HeatType.of(cmd.heatType) : null)
        room.setRampantAvailable(cmd.rampantAvailable)
        room.setDedicatedElectricPower(cmd.dedicatedElectricPower)
        room.setRoomPlanType(cmd.roomPlanType != null ? Room.RoomPlanType.of(cmd.roomPlanType) : null)
        room.setRoomCondition(cmd.roomCondition != null ? Room.RoomCondition.of(cmd.roomCondition) : null)
        room.setWindowType(cmd.windowType != null ? Room.WindowType.of(cmd.windowType) : null)
        room.setWithFurniture(cmd.withFurniture)
        room.validate()
        if (room.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(room.errors.fieldErrors)
            return
        }
        room.save failOnError: true, flush: true
        if (shouldUpdateObjects) {
//            realty.setUpdated(new Date())
//            realty.save(flush: true)
            RealtyController.updateSuitabilityForRealty(realty)
            if (oldRealty != null) {
//                oldRealty.setUpdated(new Date())
//                oldRealty.save(flush: true)
                RealtyController.updateSuitabilityForRealty(oldRealty)
            }
        }
        return [room: room]
    }

    def show() {
        def r = Room.findById(params.id)
        if (r == null) {
            renderError(NOT_FOUND, message('room.not.found'))
            return
        }
        return [room: r]
    }

    @Transactional
    def delete() {
        if (!isAdmin()) {
            response.setStatus(403)
            return
        }
        def r = Room.findById(params.id)
        if (r == null) {
            renderError(NOT_FOUND, message('room.not.found'))
            return
        }
        Realty realty = r.realty
        def session = sessionFactory.currentSession
        def toDelete = []
        def selectQuery = 'from RoomImage ri where ri.room.id = :room_id'
        def deleteQuery = 'delete from RoomImage ri where ri.room.id = :room_id'
        def paramsToDelete = [room_id: r.id]
        toDelete.addAll(RoomImage.executeQuery(selectQuery, paramsToDelete))
        RoomImage.executeUpdate(deleteQuery, paramsToDelete)
        if (!toDelete.empty)
            imageService.deleteList(toDelete)

        def listOfComments = r.comments.id
        def queryRoomComments = "DELETE from comments_rooms where room_id = ${r.id};"
        final SQLQuery sqlQueryRoomComments = session.createSQLQuery(queryRoomComments)
        sqlQueryRoomComments.executeUpdate()
        if (!listOfComments.isEmpty()) {
            def sqlComments = "DELETE from comments where id in (${String.join(",", listOfComments.collect { it.toString() })});"
            final SQLQuery sqlQueryComments = session.createSQLQuery(sqlComments)
            sqlQueryComments.executeUpdate()
        }
        final SQLQuery sqlRoom = session.createSQLQuery("delete from rooms where id = ${r.id}");
        sqlRoom.executeUpdate()
        response.setStatus(NO_CONTENT.value())
        realty.setUpdated(new Date())
        realty.save(flush: true)
        RealtyController.updateSuitabilityForRealty(realty)
        return

    }

    ImageService imageService

    def uploadImage() {
        if (!(request instanceof AbstractMultipartHttpServletRequest)) {
            renderError(BAD_REQUEST)
            return
        }
        def multipartRequest = request as AbstractMultipartHttpServletRequest

        imageService.uploadImages(multipartRequest.getFiles('file'))
                .onError { Throwable e ->
            log.error('Upload failed', e)
            def messageId
            if (e instanceof EmptyDataException)
                messageId = 'imageService.uploadImage.emptyData'
            else if (e instanceof IllegalContentTypeException)
                messageId = 'imageService.uploadImage.notImage'
            else if (e instanceof DirectoryNotFoundException)
                messageId = 'imageService.initFilesDirectory.fail'
            else if (e instanceof UnknownException)
                messageId = 'imageService.uploadImage.unknown'

            if (messageId != null)
                renderError(BAD_REQUEST, message(messageId))
            else
                throw e
        }
        .then { List<ImageService.UploadedImage> images ->
            RoomImage.withNewSession {
                session ->
                    RoomImage.withTransaction {
                        TransactionStatus status ->
                            def roomImages = images.collect {
                                RoomImage rimage = new RoomImage(
                                        contentType: it.contentType,
                                        filePath: it.originalFilePath,
                                        name: it.originalFileName
                                )
                                if (!rimage.validate()) {
                                    status.setRollbackOnly()
                                    renderFieldErrors(rimage.errors.fieldErrors)
                                    return
                                }
                                rimage.save flush: true
                                rimage
                            }
                            respond roomImages, [view: 'uploadImage', model: [images: roomImages]]
                    }
                    session.clear()
            }

        }
    }

    private boolean isAdmin() {
        return (springSecurityService.currentUser as User).isAdmin()
    }
}
