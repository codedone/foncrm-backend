package fonr.crm

class UrlMappings {

    static mappings = {

        "/login" {
            controller = 'login'
            action = [POST: 'login', GET: 'check']
        }

        "/logout" {
            controller = 'login'
            action = [POST: 'logout', GET: 'logout']
        }

        "/import" {
            controller = "import"
            action = [GET: "index"]
        }

        "/normalize" {
            controller = "import"
            action = [GET: 'removeDuplicateCounterparties']
        }

        "/robots.txt"(view: "/robots")

        group "/counterparties", {
            "/"(controller: "counterparty", action: [GET: 'index', PUT: 'save'])
            group "/$id", {
                "/"(controller: "counterparty", action: [GET: 'show', PUT: 'update'])
                group "/comments", {
                    "/"(controller: "comment", action: [GET: 'list_counterparty', PUT: 'add_counterparty'])
                    "/$cid"(controller: "comment", action: [PUT: 'update_counterparty'])
                }
            }
        }

        group "/realties", {
            "/"(controller: "realty", action: [GET: 'index', PUT: 'save'])
            group "/$id", {
                "/rooms"(controller: "room", action: [GET: 'index', PUT: 'save'])
                group "/comments", {
                    "/"(controller: "comment", action: [GET: 'list_realties', PUT: 'add_realties'])
                    "/$cid"(controller: "comment", action: [PUT: 'update_realties'])
                }
                "/"(controller: "realty", action: [GET: 'show', PUT: 'update'])

            }
            "/images"(controller: "realty", action: [PUT: 'uploadImage', POST: 'uploadImage'])
        }

        group "/rooms", {
            "/"(controller: "room", action: [GET: 'index', PUT: 'save'])
            group "/$id", {
                "/"(controller: "room", action: [GET: 'show', PUT: 'update', DELETE: 'delete'])
                group "/comments", {
                    "/"(controller: "comment", action: [GET: 'list_rooms', PUT: 'add_rooms'])
                    "/$cid"(controller: "comment", action: [PUT: 'update_rooms'])
                }
            }
            "/images"(controller: "room", action: [PUT: 'uploadImage', POST: 'uploadImage'])
        }
        group "/users", {
            "/"(controller: "user", action: [GET: 'index', PUT: 'save'])
            "/-1"(view: "/notFound")
            group "/$id", {
                "/"(controller: "user", action: [GET: 'show', PUT: 'update'])
                "/activate"(controller: "user", action: [POST: 'activate'])
                "/deactivate"(controller: "user", action: [POST: 'deactivate'])
            }
        }

        "/user/$id/activate"(controller: "user", action: [POST: 'activate'])
        "/user/$id/deactivate"(controller: "user", action: [POST: 'deactivate'])

        group "/orders", {
            "/"(controller: "order", action: [GET: 'index', PUT: 'save'])
            group "/$id", {
                "/"(controller: "order", action: [GET: 'show', PUT: 'update'])
                "/complete"(controller: "order", action: [POST: 'complete'])
                "/discard"(controller: "order", action: [POST: 'discard'])
                "/reopen"(controller: "order", action: [POST: 'reopen'])
                group "/realties", {
                    "/"(controller: "order", action: [GET: "suggest"])
                    "/update"(controller: "order", action: [GET: 'updateSuggest'])
                    group "/$pid", {
                        "/"(controller: "order", action: [POST: 'updateRealty'])
                        "/comment"(controller: "comment", action: [PUT: 'comment_order_proposal'])
                    }
                }
                group "/comments", {
                    "/"(controller: "comment", action: [GET: 'list_orders', PUT: 'add_orders'])
                    "/$cid"(controller: "comment", action: [PUT: 'update_orders'])
                }
            }
        }

        "/images/$id"(controller: "image", action: [GET: 'show'])

        "/"(view: "/index")
        "500"(view: '/error')
        "404"(view: '/notFound')
        "401"(controller: 'error', action: 'error401')
    }
}
