package fonr.crm

import grails.transaction.Transactional
import org.grails.web.json.JSONArray

import static fonr.crm.Counterparty.CounterpartyType
import static org.springframework.http.HttpStatus.NOT_FOUND

class CounterpartyController implements ControllerUtils {

    def dataSource

    static allowedMethods = [index: 'GET', show: 'GET', save: 'PUT', update: 'PUT']

    CounterpartyController() {
    }

    def index() {
        def hql = new StringBuilder('from Counterparty as c where 1 = 1 ')
        def opts = [:]
        if (params.name) {
            hql <<= "and c.name like concat('%' , :name, '%') "
            opts.put('name', params.name)
        }
        if (params.phone) {
            if (String.valueOf(params.phone).matches("^8.*")) {
                String filterPhone = String.valueOf(params.phone).replaceAll("^8", "+7")
                hql <<= "and c.id in (select ph.owner from CounterpartyPhone ph where ph.phone like concat('%', :phone, '%') " +
                        "or ph.phone like concat('%', :filterPhone, '%')) "
                opts.put('phone', params.phone)
                opts.put('filterPhone', filterPhone)
            } else {
                hql <<= "and c.id in (select ph.owner from CounterpartyPhone ph where ph.phone like concat('%', :phone, '%')) "
                opts.put('phone', params.phone)
            }
        } else if (params."phone[]") {
            def i = 0
            def phonesLikes = params.list("phone[]").collect {
                i++
                if (it.matches("^8.*")) {
                    def paramName = "phone${i}".toString()
                    def filterName = "filterPhone${i}".toString()
                    def filterPhone = String.valueOf(it).replaceAll("^8", "+7")
                    opts.put(paramName, it)
                    opts.put(filterName, filterPhone)
                    "(ph.phone like concat('%', :${paramName}, '%') OR ph.phone like concat('%', :${filterName}, '%'))"
                } else {
                    def paramName = "phone${i}".toString()
                    opts.put(paramName, it)
                    "ph.phone like concat('%', :${paramName}, '%') ".toString()
                }
            }
            hql <<= " and c.id in (select ph.owner from CounterpartyPhone ph where 1 = 1 and ${phonesLikes.join(' and ')} ) "
        }
        if (params.email) {
            hql <<= "and c.email like concat('%', :email, '%') "
            opts.put('email', params.email)
        }
        if (params.type) {
            def ct = CounterpartyType.valueOf(params.type)
            if (ct == CounterpartyType.customer) {
                hql <<= "and c.id in (select DISTINCT o.counterparty from Order o) "
            } else if (ct == CounterpartyType.owner) {
                hql <<= "and c.id in (select DISTINCT r.owner from Realty r) "
            }
        }
        hql <<= " order by c.${params.orderBy ?: 'name'} ${params.order ?: 'desc'}"
        def hqlString = hql.toString()
        def countHql = new StringBuilder("select count(*) ")
                .append(hqlString)
        def count = Counterparty.executeQuery(countHql.toString(), opts)
        def paging = [
                offset: params.offset ?: 0,
        ]
        def amount = params.amount != null ? Long.parseLong(params.amount) : 200

        def size = count[0] ?: 0
        if (amount == 0)
            return [counterparties: [], size: size]

        if (amount > 0) {
            paging.put("max", amount)
        }

        def list = Counterparty.findAll(hqlString, opts, paging)
        return [counterparties: list, size: size]
    }


    def show() {
        Counterparty c = Counterparty.get(params.id)

        if (c == null) {
            renderError(NOT_FOUND, message('counterparty.notfound'))
            return
        }
        return [counterparty: c]
    }


    @Transactional
    def save() {

        def data = readJSON()
        if (!data)
            return

        def phones
        def jsonPhones = data.phones
        if (jsonPhones instanceof JSONArray) {
            phones = jsonPhones.toArray().collect {
                p -> return new CounterpartyPhone(phone: p)
            }
        } else if (jsonPhones instanceof String) {
            phones = [new CounterpartyPhone(phone: jsonPhones)]
        }

        def instance = new Counterparty(
                name: data.name ?: null,
                email: data.email ?: null,
                info: data.info ?: '',
                created: new Date(),
                phones: phones ?: []
        )
        instance.validate()

        if (instance.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(instance.errors.fieldErrors)
            return
        }
        instance.save flush: true
        return [counterparty: instance]
    }

    @Transactional
    def update() {
        def data = readJSON()
        if (!data)
            return

        def cp = Counterparty.get(params.id)
        if (cp == null) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('counterparty.notfound'))
            return
        }

        cp.resetPhones(data.phones ? Arrays.asList(data.phones.toArray()) : null)

        cp.setName(data.name ?: null)
        cp.setEmail(data.email ?: null)
        cp.setInfo(data.info ?: null)
        cp.validate()
        if (cp.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(cp.errors.fieldErrors)
            return
        }
        cp.save flush: true
        return [counterparty: cp]
    }
}
