package fonr.crm

import grails.rest.RestfulController
import grails.transaction.Transactional
import org.springframework.validation.FieldError

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NOT_FOUND

class UserController extends RestfulController<User> implements ControllerUtils {


    def dataSource
    def springSecurityService

    static allowedMethods = [index: 'GET', show: 'GET', save: 'PUT', update: 'PUT']

    static allowedOrderFields = ['id', 'name', 'email', 'username']

    UserController() {
        super(User)
    }

    UserController(Class<User> resource) {
        super(resource)
    }

    UserController(Class<User> resource, boolean readOnly) {
        super(resource, readOnly)
    }

    @Override
    def index() {
        def joinB = new StringBuilder(" ")
        def countJoinB = new StringBuilder(" ")
        def whereB = new StringBuilder(" where u.id > 0 ")
        def opts = [:]

        if (params.name) {
            whereB <<= "and u.name like concat('%', :name, '%') "
            opts.put("name", params.name)
        }
        if (params.phone) {
            joinB <<= " inner join u.phones phone "
            countJoinB <<= " inner join u.phones phone "
            if (String.valueOf(params.phone).matches("^8.*")) {
                String filterPhone = String.valueOf(params.phone).replaceAll("^8", "+7")
                whereB <<= " and (phone.phone like concat('%', :phone, '%') " +
                        "or phone.phone like concat('%', :filterPhone, '%')) "
                opts.put('phone', params.phone)
                opts.put('filterPhone', filterPhone)
            } else {
                whereB <<= " and phone.phone like concat('%', :phone, '%') "
                opts.put("phone", params.phone)
            }
        }
        if (params.email) {
            whereB <<= " and u.email like concat('%', :email, '%') "
            opts.put("email", params.email)
        }
        if (params.active && params.active != "-1") {
            whereB <<= " and u.active = :active "
            opts.put("active", params.active.toBoolean())
        }

        if (params.role) {
            def roleLowerCase = String.valueOf(params.role).toLowerCase()
            joinB <<= " inner join u.roles role "
            countJoinB <<= " inner join u.roles role "
            whereB <<= " and role.authority = :role "
            opts.put("role", Role.stringToRoleUser(roleLowerCase))
        }
        if (params.username) {
            whereB <<= " and u.username like concat('%', :username, '%') "
            opts.put("username", params.username)
        }

        def orderType = params.order ? params.order.toLowerCase() : 'desc'
        if (orderType != 'desc' && orderType != 'asc') {
            orderType = 'desc'
        }

        def sort = params.orderBy ? params.orderBy.toLowerCase() : 'name'
        if (!allowedOrderFields.contains(sort))
            sort = 'name'

        def query = "select distinct u from User u ${joinB.toString()} ${whereB.toString()} order by ${sort} ${orderType}".toString()
        def countQuery = "select count(distinct u) from User u ${countJoinB.toString()} ${whereB.toString()}".toString()

        def count = User.executeQuery(countQuery, opts)

        def queryParams = [:]
        def amount = params.amount != null ? Long.parseLong(params.amount) : 200
        if (amount == 0)
            return [users: [], size: count[0]]

        if (amount > 0)
            queryParams['max'] = amount

        queryParams['offset'] = params.offset ?: 0

        return [users: User.executeQuery(query, opts, queryParams), size: count[0]]
    }

    @Override
    def show() {
        def user = User.get(params.id)
        if (!user) {
            renderError(NOT_FOUND, message('user.not.found'))
            return
        }
        return [user: user]
    }

    @Override
    @Transactional
    def save() {
        if (handleReadOnly()) {
            return
        }

        def json = readJSON()
        if (!json)
            return


        def roleStr = json.role
        def roleIsEmpty = roleStr == null || roleStr.isEmpty()
        def role = Role.getRoleWithName(String.valueOf(roleStr).toLowerCase())

        def passwordConfirm = json.passwordConfirm
        def password = json.password

        def passwordConfirmIsEmpty = passwordConfirm == null || passwordConfirm.isEmpty()
        def passwordIsEmpty = password == null || password.isEmpty()

        def passwordAreSame = !passwordIsEmpty && !passwordConfirmIsEmpty && password.equals(passwordConfirm)

        def phones = []
        if (json.phones) {
            phones = json.phones.toArray().collect {
                str -> return new UserPhone(phone: str)
            }
        }

        def instance = new User(
                name: json.name,
                email: json.email,
                phones: phones,
                username: json.username,
                password: password,
                active: true,
        )

        if (role != null)
            instance.addToRoles(role)

        instance.validate()
        if (instance.hasErrors() || role == null || !passwordAreSame) {
            transactionStatus.setRollbackOnly()
            def errors = new ArrayList<>(instance.errors.fieldErrors)
            if (roleIsEmpty) {
                errors.add(new FieldError('user', 'role', message('user.role.empty')))
            } else if (role == null) {
                String[] args = new String[1]
                args[0] = roleStr ?: null
                errors.add(new FieldError('user', 'role', messageSource.getMessage('role.not.found', args, Locale.default)))
            }
            if (!passwordConfirmIsEmpty) {
                if (!passwordAreSame) {
                    errors.add(new FieldError('user', 'password', message('user.password.confirm.failed')))
                    errors.add(new FieldError('user', 'passwordConfirm', message('user.password.confirm.failed')))
                }
            } else {
                errors.add(new FieldError('user', 'passwordConfirm', message('user.password.confirm.empty')))
            }

            renderFieldErrors(errors)
            return
        }
        instance.save flush: true
        return [user: instance]
    }

    @Override
    @Transactional
    def update() {
        if (handleReadOnly()) {
            return
        }

        def json = readJSON()
        if (!json)
            return

        User u = User.get(params.id)

        if (u == null) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('user.not.found'))
            return
        }

        u.resetPhones(json.phones ? Arrays.asList(json.phones.toArray()) : null)


        u.setName(json.name ?: null)
        u.setEmail(json.email ?: null)
        u.setUsername(json.username ?: null)
        def passwordAreSame = true
        def password = json.password
        def passwordChange = password != null && !password.isEmpty()
        def passwordConfirmIsEmpty = false
        if (passwordChange) {
            def passwordConfirm = json.passwordConfirm
            passwordConfirmIsEmpty = passwordConfirm == null || passwordConfirm.isEmpty()
            passwordAreSame = !passwordConfirmIsEmpty && password.equals(passwordConfirm)
            if (passwordAreSame)
                u.setPassword(json.password)
        }
        def roleHasErrors = false
        if (u.id > 1) {
            def roleStr = json.role
            def roleIsEmpty = roleStr == null || roleStr.isEmpty()
            def role = Role.getRoleWithName(String.valueOf(roleStr).toLowerCase())
            u.roles.clear()
            u.roles.add(role)
        }

        u.validate()
        if (u.hasErrors() || roleHasErrors || !passwordAreSame) {
            transactionStatus.setRollbackOnly()
            def errors = new ArrayList<>(u.errors.fieldErrors)
            if (roleHasErrors) {
                if (roleIsEmpty) {
                    errors.add(new FieldError('user', 'role', message('user.role.empty')))
                } else if (role == null) {
                    String[] args = new String[1]
                    args[0] = json.role ?: null
                    errors.add(new FieldError('user', 'role', message('role.not.found')))
                }
            }
            if (passwordChange) {
                if (passwordConfirmIsEmpty) {
                    errors.add(new FieldError('user', 'passwordConfirm', message('user.password.confirm.empty')))
                } else if (!passwordAreSame) {
                    errors.add(new FieldError('user', 'password', message('user.password.confirm.failed')))
                    errors.add(new FieldError('user', 'passwordConfirm', message('user.password.confirm.failed')))
                }
            }
            renderFieldErrors(errors)
            return
        }
        u.save flush: true
        return [user: u]
    }

    @Transactional
    def activate() {
        if (handleReadOnly()) {
            return
        }

        def cp = User.get(params.id)
        if (cp == null) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('user.not.found'))
            return
        }
        cp.setActive(true)
        cp.validate()
        if (cp.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(cp.errors.fieldErrors)
            return
        }

        cp.save flush: true
    }

//    @Override
    @Transactional
    def deactivate() {
        if (handleReadOnly()) {
            return
        }

        def u = User.get(params.id)
        if (u.id == 1) {
            renderError(BAD_REQUEST, message("user.admin.deativate"))
            return
        }
        if (u == null) {
            transactionStatus.setRollbackOnly()
            renderError(NOT_FOUND, message('user.not.found'))
            return
        }
        u.setActive(false)
        u.validate()
        if (u.hasErrors()) {
            transactionStatus.setRollbackOnly()
            renderFieldErrors(u.errors.fieldErrors)
            return
        }
        u.save flush: true
    }
}
