package fonr.crm

import grails.converters.JSON
import org.springframework.http.HttpStatus

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED
import static org.springframework.http.HttpStatus.NOT_FOUND

class ImageController implements ControllerUtils {

    static allowedMethods = [index: "GET", show: "GET"]

    def index() {
        renderError(METHOD_NOT_ALLOWED)
    }

    def show(Long id) {
        Image.async.get(id)
                .onComplete { promise ->
            Image image = promise.value
            if (image == null) {
                renderError(NOT_FOUND)
                return
            }
            render file: image.data, contentType: image.contentType
        }
        .onError {
            renderError(INTERNAL_SERVER_ERROR)
        }
    }

// Upload file samples POST only
//    def upload() {
//        if (!(request instanceof StandardMultipartHttpServletRequest)) {
//            renderError(HttpStatus.BAD_REQUEST, null)
//            return
//        }
//        def file = request.getFile('file')
//        Image image = new Image(name: file.originalFilename, contentType: file.contentType, data: file.bytes)
//        image.validate()
//        if (image.hasErrors()) {
//            renderError(HttpStatus.BAD_REQUEST, null)
//            return
//        }
//        image.save flush: true
//        render file: image.data, contentType: image.contentType
//    }
}
