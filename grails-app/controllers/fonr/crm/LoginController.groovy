package fonr.crm

import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices

import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED
import static org.springframework.http.HttpStatus.UNAUTHORIZED

class LoginController implements ControllerUtils {

    static allowedMethods = [login: 'POST', check: 'GET']

    SpringSecurityService springSecurityService
    AuthenticationManager authenticationManager
    AbstractRememberMeServices rememberMeServices

    def index() {
        renderError(METHOD_NOT_ALLOWED)
    }

    def login() {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(params.login, params.password);
        try {
            Authentication auth = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(auth);
//            rememberMeServices.loginSuccess(request, response, auth)
            return [user: springSecurityService.currentUser]
        } catch (BadCredentialsException e) {
            renderError(UNAUTHORIZED, message('user.login.failed.invalid'))
        } catch (DisabledException e) {
            renderError(UNAUTHORIZED, message('user.login.failed.disabled'))
        }
    }

    def check() {
        if (!springSecurityService.isLoggedIn()) {
            renderError(UNAUTHORIZED)
            return
        }

        def user = springSecurityService.currentUser
        if (user) {
            return [user: user]
        }
        renderError(UNAUTHORIZED)
    }

    def logout() {

        if (!springSecurityService.isLoggedIn()) {
            renderError(UNAUTHORIZED)
            return
        }

        Authentication auth = springSecurityService.authentication
        new SecurityContextLogoutHandler().logout(request, response, auth);
//        rememberMeServices.logout(request, response, auth)
        return
    }
}
