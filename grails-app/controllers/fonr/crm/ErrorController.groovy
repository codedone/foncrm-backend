package fonr.crm

import org.springframework.http.HttpStatus

import static org.springframework.http.HttpStatus.UNAUTHORIZED

class ErrorController implements ControllerUtils{
    def error401() {
        renderError(UNAUTHORIZED)
    }
}
