package fonr.crm

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import groovy.sql.Sql
import org.apache.tomcat.jdbc.pool.DataSource
import org.hibernate.SessionFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.type.ClassMetadata

import javax.websocket.Session

import static java.util.UUID.randomUUID

class ImportController {

    static final Logger log = LoggerFactory.getLogger(ImportController)
    static final Integer MAX_NUMBER_OF_REQUESTS = 1000000000

    static private final String NO_NUMBER = "Нет номера"
    static private final String PARSE_ERROR = "Не удалось получить номер"

    static final String cityFileName = "number_and_cities"
    static final String csvFileName = "normal.csv"
    static allowedMethods = [index: 'GET']
//    static ArrayList<String> phonesString

    def yandexReverseService
    def dataSource
    def sessionFactory

    @Secured(value = ["hasRole('ROLE_ADMIN')"])
    @Transactional
    def index() {
        try {
            def realties = parseCSV()
            saveParsedInfo(realties)
            render true
        } catch (Exception e) {
            log.error("Failed to import", e)
            response.status = 500
            render false
        }
    }

    private ArrayList<Realty> parseCSV() {
        HashMap<Integer, String> cities = readCities(cityFileName)
        ArrayList<Realty> realties = new ArrayList<Realty>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(csvFileName))
            String city = "";
            String line;
            Integer numberOfLine = 1;
            ArrayList<String> phonesString = new ArrayList<String>()
            while ((line = br.readLine()) != null && numberOfLine < MAX_NUMBER_OF_REQUESTS) {
//                sleep(100)
                if (cities.containsKey(numberOfLine)) {
                    city = cities.get(numberOfLine)
                    numberOfLine++
                    continue
                }
                numberOfLine++
                Integer posOfFirstDot = line.indexOf(',', 0);
                Integer posOfFirstQuote = line.indexOf('"', 0);
                Integer posToCut;
                if (posOfFirstQuote == -1 || posOfFirstDot < posOfFirstQuote) {
                    posToCut = posOfFirstDot;
                } else {
                    posToCut = line.indexOf('"', posOfFirstQuote + 1);
                    posToCut = line.indexOf(',', posToCut + 1)
                }
                if (posToCut == -1) {
                    log.error("Line {} doesn't have comma", numberOfLine)
                }
                String phoneString = line.substring(0, posToCut);
                posOfFirstDot = line.indexOf(',', posToCut + 1);
                posOfFirstQuote = line.indexOf('"', posToCut + 1);
                Integer posOfSecondCut;
                if (posOfFirstQuote == -1 || posOfFirstDot < posOfFirstQuote) {
                    posOfSecondCut = posOfFirstDot;
                } else {
                    posOfSecondCut = line.indexOf('"', posOfFirstQuote + 1);
                    posOfSecondCut = line.indexOf(',', posOfSecondCut + 1)
                }
                if (posOfSecondCut == -1) {
                    log.error("Line {} doesn't have two commas", numberOfLine)
                }
                String name = line.substring(posToCut + 1, posOfSecondCut);
                String addressString = line.substring(posOfSecondCut + 1);

                log.debug("name: {}, phone: {}, address: {}, city: {}", name, phoneString, addressString, city)

                Address address = yandexReverseService.tryGeocode("Россия", "Московская Область", city, addressString)
                address.building = address.building ?: 'Неизвестно'
                address.street = address.street ?: 'Неизвестно'
                address.city = address.city ?: city
                address.region = address.region ?: 'Московская Область'
                address.country = address.country ?: 'Россия'
//                address.zip = address.zip ?: 'Неизвестно'
                address.location = address.location ?: new GeometryFactory().createPoint(new Coordinate(-1, -1))
//                address.save flush: true
                if (log.isDebugEnabled()) log.debug("address: {}", address)

                Counterparty counterparty = createCounterparty(name, phoneString)
//                counterparty.save flush: true
                if (log.isDebugEnabled()) log.debug("counterparty: {}", counterparty)

                // создать реалти
                Realty realty = createRealty(addressString, phoneString);
                // todo: add address
                realty.address = address
//                realty.purposes = [RealtyPurposeType.findByName("Cклад")]
                realty.setOwner(counterparty)
                realty.purposes = [RealtyPurposeType.findById(2)]
                realty.offerTypes = [OfferType.findById(1)]

//                realty.save flush: true
                realties.add(realty);
                if (log.isDebugEnabled()) log.debug("realty: {}", realty)
            }
        } catch (Exception e) {
            log.error("Unexpected exception", e)
        }
        yandexReverseService.writeIndexToFile()
        return realties;
    }

    private static HashMap<Integer, String> readCities(final String filename) {
        HashMap<Integer, String> cities = new HashMap<Integer, String>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename))
            String line;
            while ((line = br.readLine()) != null) {
                String[] numberAndCity = line.split(":");
                Integer pos = Integer.parseInt(numberAndCity[0]);
                String city = numberAndCity[1];
                cities.put(pos, city);
            }
        } catch (Exception e) {
            log.error("Failed to read cities", e)
        }
        return cities;
    }

    @Transactional
    private void saveParsedInfo(ArrayList<Realty> realties) {
        log.info("Saving {} realties", realties.size())

        for (Realty realty : realties) {
            log.debug("Saving realty: {}", realty)
            if (realty.address) {
                log.trace("Saving address: {}", realty.address)
                realty.address.save(failOnError: true)
            }
            if (realty.owner) {
                log.trace("Saving owner: {}", realty.owner)
                realty.owner.save(failOnError: true)
            }
//                for (CounterpartyPhone realtyPhone : realty.owner.phones) {
//                    if (realtyPhone) {
//                        log.trace("Saving counterparty phone: {}", realtyPhone)
//                        realtyPhone.save(failOnError: true)
//                    }
//                }
//                for (RealtyPhone realtyPhone : realty.phones) {
//                    if (realtyPhone) {
//                        log.trace("Saving realtyphone: {}", realtyPhone)
//                        realtyPhone.save(failOnError: true)
//                    }
//                }
            realty.save(failOnError: true)
        }
    }

    private static Counterparty createCounterparty(String name, String phone) {
        name = name.trim().replaceAll("^\"", "").replaceAll("\"\$", "")
        phone = phone.trim()

        Counterparty counterparty = new Counterparty(
                email: generateRandomEmail(name, phone),
                name: name ?: 'Не указано', // todo: read default name from the message source
                created: new Date(),
        )

        parsePhones(phone)
                .collect { new CounterpartyPhone(phone: it) }
                .each { counterparty.addToPhones(it) }
        return counterparty
    }

    private static Realty createRealty(String address, String phone) {
        address = address.trim().replaceAll("^\"", "").replaceAll("\"\$", "")
        phone = phone.trim()

        Date now = new Date()
        Realty realty = new Realty(
                name: address,
                created: now,
                updated: now
        )
        parsePhones(phone)
                .collect { new RealtyPhone(phone: it) }
                .each { realty.addToPhones(it) }
//        realty.save flush: true
        return realty
    }

    private static Collection<String> parsePhones(String phonesStr) {

        ArrayList<String> phones = new ArrayList<>()
        String cleanStr = phonesStr.replaceAll("[^0-9]", "")
        Integer strLen = cleanStr.length()
        switch (strLen) {
            case 0:
            case 1:
            case 2:
                phones.add(NO_NUMBER)
                break
            case 7:
                phones.add(cleanStr)
                break
            case 8:
                phones.add(cleanStr + " - Ошибка в номере")
                break
            case 10:
                String firstChar = cleanStr.substring(0, 1)
                if (firstChar == '4' || firstChar == '9') {
                    phones.add('8' + cleanStr)
                } else {
                    phones.add(cleanStr + " - Ошибка в номере")
                }
                break
            case 11:
            case 22:
            case 33:
            case 44:
            case 55:
                for (int i = 0; i * 11 < strLen; i++) {
                    phones.add(cleanStr.substring(i * 11, (i + 1) * 11))
                }
                break
            case 20:
            case 30:
                for (int i = 0; i * 10 < strLen; i++) {
                    phones.add('8' + cleanStr.substring(i * 10, (i + 1) * 10))
                }
                break
            case 21:
            case 31:
            case 32:
            case 52:
                String firstChar
                Integer currentPos = 0
                while (currentPos < strLen) {
                    firstChar = cleanStr.substring(currentPos, currentPos + 1)
                    String temp
                    if (firstChar == '4' || firstChar == '9') {
                        temp = '8' + cleanStr.substring(currentPos, Math.min(currentPos + 10, strLen))
                        currentPos += 10
                    } else {
                        temp = cleanStr.substring(currentPos, Math.min(currentPos + 11, strLen))
                        currentPos += 11
                    }
                    if (temp.length() != 11) {
                        temp += " - Ошибка в номере"
                    }
                    phones.add(temp)
                }
                break
            case 12:
                phones.add(cleanStr + " - Ошибка в номере")
                break
            case 15:
                phones.add(cleanStr.substring(0, 11) + ' доб. ' + cleanStr.substring(11, 15))
                break
            case 17:
//                17 цифр - "(495) 933-75-75, 935-76-31 sklad@sivma.ru"
                if (cleanStr == "49593375759357631") {
                    phones.add("84959337575")
                    phones.add("9357631")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 18:
//                18 цифр - "89299276306 , 5570306@mail.ru"
                if (cleanStr == "892992763065570306") {
                    phones.add("89299276306")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 24:
//                24 цифры - "(495)3027321, 3026170, 7208795"
                if (cleanStr == "495302732130261707208795") {
                    phones.add("84953027321")
                    phones.add("3026170")
                    phones.add("7208795")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 25:
//        25 цифр - "8495)723-73-16,  доб. 115  7(926)2141241"
                if (cleanStr == "8495723731611579262141241") {
                    phones.add("84957237316 доб. 115")
                    phones.add("79262141241")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 26:
//        26 цифр - 84951250000доб. 1105 89169490926
//        8(495)2321005 84592210001 доб 7145
//        "495) 933-75-75, (495) 935-76-31 (доб. 475, 467) "
                if (cleanStr == "84952321005845922100017145") {
                    phones.add("84952321005")
                    phones.add("84592210001 доб 7145")
                } else if (cleanStr == "49593375754959357631475467") {
                    phones.add("84959337575")
                    phones.add("84959357631 доб. 475, 467")
                } else if (cleanStr == "84951250000110589169490926") {
                    phones.add("84951250000 доб. 1105")
                    phones.add("89169490926")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 27:
//        27 цифр - "8(495)6442440 доб. 15913 моб. телефон: 8(917)5702025"
//        " 8(495)6442440 доб. 15911 моб. телефон: 8(919)9911363 "
                if (cleanStr == "849564424401591389175702025") {
                    phones.add("84956442440 доб. 15913")
                    phones.add("89175702025")
                } else if (cleanStr == "849564424401591189199911363") {
                    phones.add("84956442440 доб. 15911")
                    phones.add("89199911363")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 28:
//        28 цифр - "(499)9003620, 9003160 8(495)9700852"
                if (cleanStr == "4999003620900316084959700852") {
                    phones.add("84999003620")
                    phones.add("9003160")
                    phones.add("84959700852")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 29:
//        29 цифр - 89150702254  7 (495) 646-85-66 6468539
                if (cleanStr == "89150702254749564685666468539") {
                    phones.add("89150702254")
                    phones.add("74956468566")
                    phones.add("6468539")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 34:
//        34 цифры - "Тел. (495) 737-35-00, 737-35-01, Факс (495) 737-35-03, 737-35-05."
                if (cleanStr == "4957373500737350149573735037373505") {
                    phones.add("84957373500")
                    phones.add("84957373501")
                    phones.add("84957373503 - Факс")
                    phones.add("84957373505 - Факс")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 36:
//        36 цифр - 89161689139 84957772585 доб 197 84952216601
                if (cleanStr == "891616891398495777258519784952216601") {
                    phones.add("89161689139")
                    phones.add("84957772585 доб. 197")
                    phones.add("84952216601")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 40:
//        40 цифр - 84957979800 доб 366  84956379310 добав 1782 89104915300
                if (cleanStr == "8495797980036684956379310178289104915300") {
                    phones.add("84957979800 доб. 366")
                    phones.add("84956379310 доб. 1782")
                    phones.add("89104915300")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            case 41:
//        41 цифра - "8(499)5570387 (доб.: 1110, 1117) 8(968)5979721 89164616353"
                if (cleanStr == "84995570387111011178968597972189164616353") {
                    phones.add("84995570387 доб. 1110, 1117")
                    phones.add("89685979721")
                    phones.add("89164616353")
                } else {
                    phones.add(PARSE_ERROR)
                }
                break
            default:
                phones.add(PARSE_ERROR)
                break
        }
        log.debug("phoneStr : {}", phonesStr)
        log.debug("cleanStr : {}", cleanStr)
        log.debug("phones : {}", phones.toString())
        return phones
//        return [(randomUUID() as String).substring(0, 31)] // todo: parsePhones
    }

    private static String generateRandomEmail(String name, String phone) {
        def uuid = (randomUUID() as String).substring(0, 20)
        return "$uuid@example.com"
    }
}

