import fonr.crm.*
import fonr.crm.controllers.cmd.order.RoomFilter
import org.grails.web.json.JSONObject

import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

model {
    OrderProposalWithLastComment op
    RoomFilter roomFilter

}

json {
    id op.orderProposal.id
    created TimeUnit.MILLISECONDS.toSeconds(op.orderProposal.created.time)
    updated TimeUnit.MILLISECONDS.toSeconds(op.orderProposal.updated.time)
    state op.orderProposal.state.getName()
    excellent op.orderProposal.excellent
    shareable op.orderProposal.shareable
    byTotalSquare op.orderProposal.byTotalSquare

    def _realty = op.orderProposal.realty
    realty {
        id _realty.id
        info _realty.info
        address Address.joinAddressString(_realty.address)
        created TimeUnit.MILLISECONDS.toSeconds(_realty.created.time)
        updated TimeUnit.MILLISECONDS.toSeconds(_realty.updated.time)
        location {
            lon _realty.address.location.x
            lat _realty.address.location.y
        }
        purposes _realty.purposes.collect {
            RealtyPurposeType rp -> rp.id
        }
        offerTypes _realty.offerTypes.collect {
            OfferType ot -> ot.id
        }
        counterparty {
            id _realty.owner.id
            name _realty.owner.name
            phones _realty.owner.phones.collect {
                return it.phone
            }
        }
        images _realty.images.collect {
            tmpl.'/realty/realtyImage'(image: it)
        }
        rooms _realty.rooms.findAll { room ->
            isShareableOk(room) && isMinimalPartOk(room) && isLeasedOutOk(room) &&
                    isRoomTypeOk(room) && isRoomConditionOk(room) && isRoomPlanTypeOk(room) && isWindowTypeOk(room) &&
                    isHeatTypeOk(room) && isRampantOk(room) && isFurnitureOk(room) &&
                    isFloorOk(room) && isCeilingOk(room) && isElectricPowerOk(room) && isSquareOk(room) && isPricesOk(room) &&
                    isInfoOk(room)
        }.collect {
            tmpl.'/room/room'(room: it)
        }
    }
    phones _realty.phones.collect {
        return it.phone
    }
    comment op.comment ? tmpl.'/comment/comment'(comment: op.comment) : new JSONObject()
    stateComment op.orderProposal.comment ? tmpl.'/comment/comment'(comment: op.orderProposal.comment) : null
}

private boolean isPricesOk(Room room) {
    (roomFilter.pricePerSquareMeter == null || roomFilter.pricePerSquareMeter.max == null && roomFilter.pricePerSquareMeter.min == null) &&
            (roomFilter.totalPrice == null || roomFilter.totalPrice.max == null && roomFilter.totalPrice.min == null) ||
            (
                    roomFilter.offerType.id == 2L && isPricePerMeterForSaleOk(room) && isTotalPriceForSaleOk(room) ||
                            roomFilter.offerType.id == 1L && isPricePerMeterForRentOk(room) && isTotalPriceForRentOk(room)
            )
}

private boolean isTotalPriceForRentOk(Room room) {
    if (roomFilter.totalPrice == null || roomFilter.totalPrice.max == null && roomFilter.totalPrice.min == null) {
        return true
    }
    if (roomFilter.totalPrice.priceType == null) {
        return isTotalPriceForRentRawOk(room)
    } else {
        return (room.priceType != null &&
                (roomFilter.totalPrice.max == null ||
                        room.totalPrice * (room.priceType == Room.PriceType.PERMONTH ? 12 : 1)
                        <= roomFilter.totalPrice.max * (roomFilter.totalPrice.priceType == Room.PriceType.PERMONTH.nameForPrint() ? 12 : 1)
                ) &&
                (roomFilter.totalPrice.min == null ||
                        room.totalPrice * (room.priceType == Room.PriceType.PERMONTH ? 12 : 1)
                        >= roomFilter.totalPrice.min * (roomFilter.totalPrice.priceType == Room.PriceType.PERMONTH.nameForPrint() ? 12 : 1))
        )
    }
}

private boolean isPricePerMeterForRentOk(Room room) {
    if (roomFilter.pricePerSquareMeter == null || roomFilter.pricePerSquareMeter.min == null && roomFilter.pricePerSquareMeter.max) {
        return true
    }
    if (roomFilter.pricePerSquareMeter.priceType == null) {
        return isPricePerMeterForRentRawOk(room)
    } else {
        return (room.priceType != null &&
                (roomFilter.pricePerSquareMeter.max == null ||
                        room.pricePerSquareMeter * (room.priceType == Room.PriceType.PERMONTH ? 12 : 1)
                        <= roomFilter.pricePerSquareMeter.max * (roomFilter.pricePerSquareMeter.priceType == Room.PriceType.PERMONTH.nameForPrint() ? 12 : 1)
                ) &&
                (roomFilter.pricePerSquareMeter.min == null ||
                        room.pricePerSquareMeter * (room.priceType == Room.PriceType.PERMONTH ? 12 : 1)
                        >= roomFilter.pricePerSquareMeter.min * (roomFilter.pricePerSquareMeter.priceType == Room.PriceType.PERMONTH.nameForPrint() ? 12 : 1))
        )
    }
}

private boolean isPricePerMeterForRentRawOk(Room room) {
    roomFilter.pricePerSquareMeter == null || roomFilter.pricePerSquareMeter.max == null && roomFilter.pricePerSquareMeter.min == null ||
            room.pricePerSquareMeter != null &&
            (roomFilter.pricePerSquareMeter.max == null || room.pricePerSquareMeter <= roomFilter.pricePerSquareMeter.max) &&
            (roomFilter.pricePerSquareMeter.min == null || room.pricePerSquareMeter >= roomFilter.pricePerSquareMeter.min)
}

private boolean isTotalPriceForRentRawOk(Room room) {
    roomFilter.totalPrice == null || roomFilter.totalPrice.max == null && roomFilter.totalPrice.min == null ||
            room.totalPrice != null &&
            (roomFilter.totalPrice.max == null || room.totalPrice <= roomFilter.totalPrice.max) &&
            (roomFilter.totalPrice.min == null || room.totalPrice >= roomFilter.totalPrice.min)
}

private boolean isPricePerMeterForSaleOk(Room room) {
    roomFilter.pricePerSquareMeter == null || roomFilter.pricePerSquareMeter.max == null && roomFilter.pricePerSquareMeter.min == null ||
            room.pricePerSquareMeter != null &&
            (roomFilter.pricePerSquareMeter.max == null || room.pricePerSquareMeter <= roomFilter.pricePerSquareMeter.max) &&
            (roomFilter.pricePerSquareMeter.min == null || room.pricePerSquareMeter >= roomFilter.pricePerSquareMeter.min)
}

private boolean isTotalPriceForSaleOk(Room room) {
    roomFilter.totalPrice == null || roomFilter.totalPrice.max == null && roomFilter.totalPrice.min == null ||
            room.totalPrice != null &&
            (roomFilter.totalPrice.max == null || room.totalPrice <= roomFilter.totalPrice.max) &&
            (roomFilter.totalPrice.min == null || room.totalPrice >= roomFilter.totalPrice.min)
}

private boolean isSquareOk(Room room) {
    roomFilter.square == null || (
            (roomFilter.square.max == null || room.square != null && roomFilter.square.max >= room.square) &&
                    (roomFilter.square.min == null || room.square != null && roomFilter.square.min <= room.square)
    )
}

private boolean isFloorOk(Room room) {
    roomFilter.floorNumber == null || (
            (roomFilter.floorNumber.max == null || room.floorNumber != null && roomFilter.floorNumber.max >= room.floorNumber) &&
                    (roomFilter.floorNumber.min == null || room.floorNumber != null && roomFilter.floorNumber.min <= room.floorNumber)
    )
}

private boolean isElectricPowerOk(Room room) {
    roomFilter.dedicatedElectricPower == null || (
            (roomFilter.dedicatedElectricPower.max == null || room.dedicatedElectricPower != null && roomFilter.dedicatedElectricPower.max >= room.dedicatedElectricPower) &&
                    (roomFilter.dedicatedElectricPower.min == null || room.dedicatedElectricPower != null && roomFilter.dedicatedElectricPower.min <= room.dedicatedElectricPower)
    )
}

private boolean isCeilingOk(Room room) {
    roomFilter.ceilingHeight == null || (
            (roomFilter.ceilingHeight.max == null || room.ceilingHeight != null && roomFilter.ceilingHeight.max >= room.ceilingHeight) &&
                    (roomFilter.ceilingHeight.min == null || room.ceilingHeight != null && roomFilter.ceilingHeight.min <= room.ceilingHeight)
    )
}

private boolean isFurnitureOk(Room room) {
    roomFilter.withFurniture == null || room.withFurniture != null && roomFilter.withFurniture == room.withFurniture
}

private boolean isRampantOk(Room room) {
    roomFilter.rampantAvailable == null || room.rampantAvailable != null && roomFilter.rampantAvailable == room.rampantAvailable
}

private boolean isHeatTypeOk(Room room) {
    roomFilter.heatType == null || room.heatType != null && roomFilter.heatType == room.heatType
}

private boolean isWindowTypeOk(Room room) {
    roomFilter.windowType == null || room.windowType != null && roomFilter.windowType == room.windowType
}

private boolean isRoomPlanTypeOk(Room room) {
    roomFilter.roomPlanType == null || room.roomPlanType != null && roomFilter.roomPlanType == room.roomPlanType
}

private boolean isRoomConditionOk(Room room) {
    roomFilter.roomCondition == null || room.roomCondition != null && roomFilter.roomCondition == room.roomCondition
}

private boolean isRoomTypeOk(Room room) {
    roomFilter.roomType == null || room.roomType != null && roomFilter.roomType == room.roomType
}

private boolean isLeasedOutOk(Room room) {
    roomFilter.leased == null || roomFilter.leased == room.isLeasedOut
}

private boolean isShareableOk(Room room) {
    roomFilter.shareable == null || roomFilter.shareable == room.shareable
}

private boolean isMinimalPartOk(Room room) {
    roomFilter.minimalPart == null || (
            (roomFilter.minimalPart.max == null || room.minimalPart != null && roomFilter.minimalPart.max >= room.minimalPart) &&
                    (roomFilter.minimalPart.min == null || room.minimalPart != null && roomFilter.minimalPart.min <= room.ceilingHeight)
    )
}

private boolean isInfoOk(Room room) {
    roomFilter.info == null || roomFilter.info == "" || (room.info != null && room.info.matches(".*" + Pattern.quote(roomFilter.info) + ".*"))
}