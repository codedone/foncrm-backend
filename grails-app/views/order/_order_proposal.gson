import fonr.crm.Address
import fonr.crm.OfferType
import fonr.crm.OrderProposalWithLastComment
import fonr.crm.RealtyPurposeType
import org.grails.web.json.JSONObject

import java.util.concurrent.TimeUnit

model {
    OrderProposalWithLastComment op
}

json {
    id op.orderProposal.id
    created TimeUnit.MILLISECONDS.toSeconds(op.orderProposal.created.time)
    updated TimeUnit.MILLISECONDS.toSeconds(op.orderProposal.updated.time)
    state op.orderProposal.state.getName()
    excellent op.orderProposal.excellent
    shareable op.orderProposal.shareable
    byTotalSquare op.orderProposal.byTotalSquare

    def _realty = op.orderProposal.realty
    realty {
        id _realty.id
        info _realty.info
        address Address.joinAddressString(_realty.address)
        created TimeUnit.MILLISECONDS.toSeconds(_realty.created.time)
        updated TimeUnit.MILLISECONDS.toSeconds(_realty.updated.time)
        location {
            lon _realty.address.location.x
            lat _realty.address.location.y
        }
        purposes _realty.purposes.collect {
            RealtyPurposeType rp -> rp.id
        }
        offerTypes _realty.offerTypes.collect {
            OfferType ot -> ot.id
        }
        counterparty {
            id _realty.owner.id
            name _realty.owner.name
            phones _realty.owner.phones.collect {
                return it.phone
            }
        }
        images _realty.images.collect {
            tmpl.'/realty/realtyImage'(image: it)
        }
        rooms _realty.rooms.collect {
            tmpl.'/room/room'(room: it)
        }
    }
    phones _realty.phones.collect {
        return it.phone
    }
    comment op.comment ? tmpl.'/comment/comment'(comment: op.comment) : new JSONObject()
    stateComment op.orderProposal.comment ? tmpl.'/comment/comment'(comment: op.orderProposal.comment) : null
}