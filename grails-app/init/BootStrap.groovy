import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import fonr.crm.*
import grails.util.Environment
import groovy.sql.Sql

class BootStrap {
    def dataSource

    def init = { servletContext ->
        User admin = User.findByUsername("admin")
        if (admin == null) {
            admin = new User(name: 'admin', password: 'admin', email: 'admin@fonrcrm.com', username: 'admin').save flush: true, failOnError: true
            Role role = Role.findByAuthority("ROLE_ADMIN")
            UserRole ur = new UserRole(u: admin, r: role).save flush: true
        }

        User user = User.findByUsername("user")
        if (user == null) {
            user = new User(name: 'user', password: 'user', email: 'user@fonrcrm.com', username: 'user').save flush: true, failOnError: true
            Role role = Role.findByAuthority("ROLE_USER")
            UserRole ur = new UserRole(u: user, r: role).save flush: true
        }

        Room room = Room.findByInfo("Тестовый склад (не пускать в продакшн)");
        if (room == null) {
            room = new Room(
                    info: "Тестовый склад (не пускать в продакшн)",
                    realty: Realty.findByIdIsNotNull(),
                    created: new Date(),
                    updated: new Date(),
                    square: new BigDecimal(100.0),
                    ceilingHeight: new BigDecimal(5.0),
                    floorNumber: 1,
                    pricePerSquareMeter: new BigDecimal(1203.0),
                    priceType: Room.PriceType.PERMONTH,
                    roomType: Room.RoomType.STOCK,
                    isHeated: true,
                    rampantAvailable: true,
                    dedicatedElectricPower: new BigDecimal(10000.0)
            ).save flush: true, failOnError: true
        }
        Room room2 = Room.findByInfo("Тестовый офис (не пускать в продакшн)");
        if (room2 == null) {
            room2 = new Room(
                    info: "Тестовый офис (не пускать в продакшн)",
                    realty: Realty.findByIdIsNotNull(),
                    created: new Date(),
                    updated: new Date(),
                    square: new BigDecimal(20.0),
                    ceilingHeight: new BigDecimal(3.25),
                    floorNumber: 1,
                    pricePerSquareMeter: new BigDecimal(1230.0),
                    priceType: Room.PriceType.PERMONTH,
                    roomType: Room.RoomType.OFFICE,
                    roomPlanType: Room.RoomPlanType.OPEN,
                    roomCondition: Room.RoomCondition.CAPITAL,
                    windowType: Room.WindowType.PANORAM,
                    withFurniture: true
            ).save flush: true, failOnError: true
        }


    }
    def destroy = {
    }
}
