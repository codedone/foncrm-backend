package fonr.crm

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import groovy.json.JsonSlurper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class YandexReverseService {
    static final Logger log = LoggerFactory.getLogger(YandexReverseService)
    static private HashMap<String, String> addresses
    static final String savedAddressesFile = "./savedAddresses"

    synchronized static void init() {
        if (addresses != null) return;
        File f = new File(savedAddressesFile);
        if (f.exists() && !f.isDirectory()) {
            log.info("Reading saved addresses")
            readIndexFromFile(savedAddressesFile);
        } else {
            addresses = new HashMap<String, String>()
        }
        if (precision == null) {
            precision = new HashMap<>()
            precision.put("exact", 100)
            precision.put("number", 90)
            precision.put("near", 80)
            precision.put("range", 70)
            precision.put("street", 60)
            precision.put("other", 10)
            precision.put("none", 0)
        }

    }

    static public writeIndexToFile() {
        try {
            ObjectOutputStream objStream1 = new ObjectOutputStream(new FileOutputStream(savedAddressesFile));
            objStream1.writeObject(addresses);
        } catch (IOException e) {
            log.error("Something wrong with reading: {}", e)
        }
    }

    static private readIndexFromFile(String testIndex) {
        try {
            ObjectInputStream objStream1 = new ObjectInputStream(new FileInputStream(savedAddressesFile));
            addresses = objStream1.readObject();

        } catch (IOException e) {
            log.error("Something wrong with deserialization: {}", e);
        }
    }


    static public Address tryGeocode(String country, String region, String city, String req) {
        ArrayList<ResultAndPrecision> results = new ArrayList<>()
        for (String str : [
                country + ", " + region + ", " + city + ", " + req,
                country + ", " + region + ", " + req,
                country + ", " + city + ", " + req,
                country + ", " + req,
                region + ", " + city + ", " + req,
                region + ", " + req,
                city + ", " + req,
                req
        ]) {
            ResultAndPrecision resultAndPrecision = doReverseGeocoding(str)
            if (resultAndPrecision.precision >= 60) {
                return resultAndPrecision.address
            }
            results.add(resultAndPrecision)
        }
        return chooseBestResult(results)
    }


    static public ResultAndPrecision doReverseGeocoding(String query) {
        query = query.replaceAll('"', "")
        String valueString
        try {
            if (addresses == null) init();
            Double latitude
            Double longitude
            if (addresses.containsKey(query)) {
                log.info("Get address from saved: {}", query)
                valueString = addresses.get(query)
            } else {
                def url = "https://geocode-maps.yandex.ru/1.x/?geocode=${URLEncoder.encode(query)}&format=json&results=1"
                valueString = new URL(url).getText('utf-8')
                addresses.put(query, valueString)
            }
            def yandexJson = new JsonSlurper().parseText(valueString)
//            log.debug("Url: {}, response: {}", url, valueString)

            assert yandexJson instanceof Map

            def yandexMetaData = yandexJson.response.GeoObjectCollection.metaDataProperty.GeocoderResponseMetaData
            def yandexFeatureMember = yandexJson.response.GeoObjectCollection.featureMember
            //noinspection GroovyAssignabilityCheck
            def foundCount = Integer.parseInt(yandexMetaData.found)
//            println(valueFromYandex)
            ResultAndPrecision resultAndPrecision = new ResultAndPrecision()

            if (foundCount > 0) {
                Address addressFull = new Address();
                //noinspection GroovyAssignabilityCheck
                def usedMember = yandexFeatureMember[0];
                String pos = usedMember.GeoObject.Point.pos;
                longitude = Double.parseDouble((pos.split(' '))[0]);
                latitude = Double.parseDouble((pos.split(' '))[1]);
                //check coordinated to be really near
                resultAndPrecision.precision = precision.get(String.valueOf(usedMember.GeoObject.metaDataProperty.GeocoderMetaData.precision))
                def country = usedMember.GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country
                addressFull.country = country.CountryName
                if (country.containsKey("AdministrativeArea")) {
                    addressFull.region = country.AdministrativeArea.AdministrativeAreaName;
                    if (country.AdministrativeArea.containsKey("SubAdministrativeArea")) {
                        if (country.AdministrativeArea.SubAdministrativeArea.containsKey("Locality")) {
                            addressFull.city = country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                            if (country.AdministrativeArea.SubAdministrativeArea.Locality.containsKey("Thoroughfare")) {
                                addressFull.street = country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
                                if (country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.containsKey("Premise")) {
                                    addressFull.building = country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.Premise.PremiseNumber;
                                }
                            } else if (country.AdministrativeArea.SubAdministrativeArea.Locality.containsKey("DependentLocality")) {
                                if (country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.containsKey("Thoroughfare")) {
                                    addressFull.street = country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare.ThoroughfareName;
                                    if (country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare.containsKey("Premise")) {
                                        addressFull.building = country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare.Premise.PremiseNumber;
                                    }
                                }
                            }
                        }
                    } else {
                        if (country.AdministrativeArea.containsKey("Locality")) {
                            addressFull.city = country.AdministrativeArea.Locality.LocalityName;
                            if (country.AdministrativeArea.Locality.containsKey("Thoroughfare")) {
                                addressFull.street = country.AdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
                                if (country.AdministrativeArea.Locality.Thoroughfare.containsKey("Premise")) {
                                    addressFull.building = country.AdministrativeArea.Locality.Thoroughfare.Premise.PremiseNumber;
                                }
                            } else if (country.AdministrativeArea.Locality.containsKey("DependentLocality")) {
                                if (country.AdministrativeArea.Locality.DependentLocality.containsKey("Thoroughfare")) {
                                    addressFull.street = country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.ThoroughfareName;
                                    if (country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.containsKey("Premise")) {
                                        addressFull.building = country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.Premise.PremiseNumber;
                                    }
                                }
                            }
                        }
                    }
                }
//                addressFull.zip = NominatimReverse.getZipCodeForCoordinates(latitude, longitude)
                addressFull.location = new GeometryFactory().createPoint(new Coordinate(latitude, longitude))
                resultAndPrecision.address = addressFull
                return resultAndPrecision;
            } else {
                return new ResultAndPrecision(address: new Address(), precision: 0);
            }
        } catch (Exception e) {
            log.error("Unexpected exception in getInformation", e)
            return new ResultAndPrecision(address: new Address(), precision: 0);
        }
    }

    private static class NominatimReverse {
        static public String getZipCodeForCoordinates(Double lat, Double lon) {
            try {
                def url = "http://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lon}&format=json&addressdetails=1&accept-language=russian"
                String valueFromOsm = new URL(url).getText('utf-8')
                def osmJsonElem = new JsonSlurper().parseText(valueFromOsm)
                return osmJsonElem.address.postcode
            } catch (Exception e) {
                log.error("Failed to get zip code for lat={}, lon={}", lat, lon, e)
            }
            return null
        }
    }


    private static Address chooseBestResult(ArrayList<ResultAndPrecision> results) {
        if (results.isEmpty()) {
            return new Address()
        }
        return results.sort { a, b -> b.precision <=> a.precision }.first().address ?: new Address()
    }


    private static HashMap<String, Integer> precision

//    private static Address createAddressFromSavedInfo(SavedAddress savedInfo) {
//        Address address = new Address()
////        address.setZip(savedInfo.zip ?: "Неизвестно")
//        address.setCountry(savedInfo.country)
//        address.setRegion(savedInfo.region)
//        address.setCity(savedInfo.city)
//        address.setStreet(savedInfo.street)
//        address.setBuilding(savedInfo.building)
//        address.setLocation(savedInfo.location)
//        address.setRoom(savedInfo.room)
//        return address
//    }
//
//    private static SavedAddress createInfoToSaveFromAddress(Address address) {
//        SavedAddress savedAddress = new SavedAddress()
////        savedAddress.setZip(address.zip ?: "Неизвестно")
//        savedAddress.setCountry(address.country)
//        savedAddress.setRegion(address.region)
//        savedAddress.setCity(address.city)
//        savedAddress.setStreet(address.street)
//        savedAddress.setBuilding(address.building)
//        savedAddress.setLocation(address.location)
//        savedAddress.setRoom(address.room)
//        return savedAddress
//    }
}

//class SavedAddress implements Serializable {
////    String zip
//    String country
//    String region
//    String city
//    String street
//    String building
//    String room
//    Point location
//}

class ResultAndPrecision {
    Address address = new Address()
    Integer precision = 0
}
