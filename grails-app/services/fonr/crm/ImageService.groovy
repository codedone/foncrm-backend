package fonr.crm

import fonr.crm.exceptions.service.image.EmptyDataException
import fonr.crm.exceptions.service.image.IllegalContentTypeException
import fonr.crm.exceptions.service.image.UnknownException
import grails.async.Promise
import grails.async.Promises
import grails.transaction.Transactional
import grails.util.Holders
import net.coobird.thumbnailator.Thumbnails
import org.slf4j.LoggerFactory
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Path
import java.nio.file.Paths

@Transactional
class ImageService {

    static scope = 'singleton'
    private File fileDirectory = null
    private static def log = LoggerFactory.getLogger(getClass())

    ImageService() {
        prepareDirectory()
        prepareThumbnailsDirectory()
    }

    public Promise<UploadedImage> uploadImage(MultipartFile file) {
        Promises.task {
            if (file.empty)
                throw new EmptyDataException()

            if (!(file.contentType.toLowerCase() ==~ /^image\\/(jpeg|png|tiff|bmp|gif)/))
                throw new IllegalContentTypeException()

            doUploadImage(file)
        }
    }

    public Promise<List<UploadedImage>> uploadImages(List<MultipartFile> files) {
        Promises.task {
            // fixme: что-то тут не так
            files.each {
                if (it.empty)
                    throw new EmptyDataException()

                if (!(it.contentType.toLowerCase() ==~ /^image\\/(jpeg|png|tiff|bmp|gif)/))
                    throw new IllegalContentTypeException()
            }

            files.collect { doUploadImage(it) }
        }
    }

    public Promise<Boolean> deleteImageFiles(Image image) {
        Promises.task { doDelete(image) }
    }

    public Promise<Void> deleteList(List<Image> images) {
        Promises.task {
            images.each { doDelete(it) }
            return null
        }
    }

    private boolean doDelete(Image image) {
        File originFile = new File(fileDirectory.getAbsolutePath(), image.filePath)
        if (originFile.delete())
            log.debug('File {} was successfully deleted', originFile.getPath())
        deleteThumbnails(originFile)
        return true
    }

    private UploadedImage doUploadImage(MultipartFile file) {
        Path path = Paths.get(file.getOriginalFilename())
        def originalNAme = path.getFileName().toString()
        File targetFile = new File(fileDirectory.getAbsolutePath(), "${UUID.randomUUID()}_${originalNAme}".toString())
        try {
            log.debug('start transfer')
            file.transferTo(targetFile)
            createThumbnails(targetFile)
            log.debug('transfer success {} into {}', file.originalFilename, targetFile.absolutePath)
            return UploadedImage.createUploadedImage(originalNAme, fileDirectory, targetFile, file.contentType)
        } catch (Exception e) {
            log.error('transfer failed {} into {}', file.originalFilename, targetFile.absolutePath, e)
            throw new UnknownException(e)
        }
    }

    private File createThumbnail(File originalFile, ThumbnailSize size) {
        File thumbnailFile = new File(Paths.get(fileDirectory.absolutePath, size.subFolderPath, originalFile.name).toString())
        Thumbnails.Builder<File> builder = Thumbnails.of(originalFile)
        if (size.width > 0)
            builder.width(size.width)

        if (size.height > 0)
            builder.height(size.height)

        builder.toFile(thumbnailFile)
        thumbnailFile
    }

    private List<File> createThumbnails(File originalFile) {
        ThumbnailSize.values().collect {
            createThumbnail(originalFile, it)
        }
    }

    private void deleteThumbnail(File originalFile, ThumbnailSize size) {
        File thumbnailFile = new File(Paths.get(fileDirectory.absolutePath, size.subFolderPath, originalFile.name).toString())
        if (thumbnailFile.exists())
            thumbnailFile.delete()
    }

    private void deleteThumbnails(File originalFile) {
        ThumbnailSize.values().each {
            deleteThumbnail(originalFile, it)
        }
    }

    private void prepareDirectory() {
        String directoryPath = Holders.getConfig().getProperty("server.imageDirectory");
        if (directoryPath != null) {
            if (directoryPath.startsWith("~")) {
                String withoutHomePrefix = directoryPath.replaceFirst('^\\~\\/?', '');
                fileDirectory = new File(System.getProperty("user.home"), withoutHomePrefix);
            } else {
                fileDirectory = new File(directoryPath);
            }
        } else {
            fileDirectory = new File("imageDirectory");
        }
        boolean exists = fileDirectory.exists();
        if ((exists && !fileDirectory.canWrite()) || (!exists && !fileDirectory.mkdirs()))
            throw new RuntimeException("Can't create directory \"${fileDirectory.getAbsolutePath()}\" or haven't permissions to write".toString());
    }

    private void prepareThumbnailsDirectory() {
        ThumbnailSize.values().each {
            File thumbnailDirectory = new File(fileDirectory, it.subFolderPath);
            if (!thumbnailDirectory.exists() && !thumbnailDirectory.mkdirs())
                throw new RuntimeException("Can't create directory ${thumbnailDirectory.absolutePath}".toString())
        }
    }


    public final static class UploadedImage {
        final String originalFileName
        final String originalFilePath
        final String contentType
        final File original

        UploadedImage(String contentType, File original, String originalFilePath, String originalFileName) {
            this.originalFilePath = originalFilePath
            this.contentType = contentType
            this.original = original
            this.originalFileName = originalFileName
        }

        private
        static UploadedImage createUploadedImage(String originalFileName, File directory, File original, String contentType) {
            Path directoryAbsolutePath = Paths.get(directory.getAbsolutePath())
            Path originalAbsolutePath = Paths.get(original.getAbsolutePath())
            return new UploadedImage(
                    contentType,
                    original,
                    directoryAbsolutePath.relativize(originalAbsolutePath).toString(),
                    originalFileName
            )
        }
    }

    public enum ThumbnailSize {
        H70(0, 70), H150(0, 150), W150(150, 0);

        final int width;
        final int height;
        final String subFolderName;
        final String subFolderPath;
        final String pathFragment;

        ThumbnailSize(int width, int height) {
            this.width = width;
            this.height = height;
            String widthSuffix = width > 0 ? "w${width}".toString() : '';
            String heightSuffix = height > 0 ? "h${height}" : '';
            subFolderName = "${widthSuffix}${heightSuffix}".toString()
            subFolderPath = Paths.get('thumbnails', subFolderName).toString();
            pathFragment = "thumbnails/${subFolderName}".toString();
        }
    }
}
