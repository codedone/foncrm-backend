// Added by the Spring Security Core plugin:

//  hashing
//grails.plugin.springsecurity.password.algorithm = 'bcrypt'
//grails.plugin.springsecurity.password.bcrypt.logrounds = 20
//grails.plugin.springsecurity.dao.reflectionSaltSourceProperty = 'username'

grails.plugin.springsecurity.rest.login.failureStatusCode = "403"
//grails.plugin.springsecurity.rest.login.useJsonCredentials=true
//grails.plugin.springsecurity.rest.login.usernamePropertyName="login"
//grails.plugin.springsecurity.rest.login.passwordPropertyName="password"

grails.plugin.springsecurity.rest.login.useRequestParamsCredentials = true
grails.plugin.springsecurity.rest.login.usernamePropertyName = "username"
grails.plugin.springsecurity.rest.login.passwordPropertyName = "password"

grails.plugin.springsecurity.rememberMe.cookieName = "fonr-crm"

grails.plugin.springsecurity.rememberMe.tokenValiditySeconds = 86400
grails.plugin.springsecurity.rememberMe.key = "very secret key for encoding cookies content"
grails.plugin.springsecurity.rememberMe.useSecureCookie = false
grails.plugin.springsecurity.rememberMe.persistentToken.seriesLength = 256
grails.plugin.springsecurity.rememberMe.alwaysRemember = false

grails.plugin.springsecurity.rejectIfNoRule = true
grails.plugin.springsecurity.fii.rejectPublicInvocations = false
grails.plugin.springsecurity.logout.postOnly = false

grails.plugin.springsecurity.userLookup.userDomainClassName = 'fonr.crm.User'
grails.plugin.springsecurity.userLookup.usernamePropertyName = 'username'
grails.plugin.springsecurity.userLookup.enabledPropertyName = 'active'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'fonr.crm.UserRole'
grails.plugin.springsecurity.authority.className = 'fonr.crm.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        // администрирование
        [pattern: '/secure', access: ['ROLE_ADMIN']],
        [pattern: '/secure/*', access: ['ROLE_ADMIN']],
        [pattern: '/login/impersonate', access: ['ROLE_ADMIN']],
        [pattern: '/logout/impersonate', access: ['permitAll']],
        [pattern: '/shutdown', access: ['ROLE_ADMIN']],

        [pattern: '/login/**', access: ['permitAll']],
        [pattern: '/logout/**', access: ['permitAll']],
        [pattern: '/error', access: ['permitAll']],

        [pattern: '/', access: ['isAuthenticated()']],
        [pattern: '/index', access: ['isAuthenticated()']],
        [pattern: '/index.gson', access: ['isAuthenticated()']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],

        [pattern: '/user/*', access: ['ROLE_ADMIN']],
        [pattern: '/comment/*', access: ['isAuthenticated()']],
        [pattern: '/counterparty/*', access: ['ROLE_ADMIN']],
        [pattern: '/order/*', access: ['isAuthenticated()']],
        [pattern: '/order/save', access: ['ROLE_ADMIN']],
        [pattern: '/order/update', access: ['isAuthenticated()']],
        [pattern: '/order/updateSuggest', access: ['isAuthenticated()']],
        [pattern: '/order/complete', access: ['ROLE_ADMIN']],
        [pattern: '/order/discard', access: ['ROLE_ADMIN']],
        [pattern: '/realty/*', access: ['isAuthenticated()']],
        [pattern: '/realty/update', access: ['isAuthenticated()']],
        [pattern: '/realty/save', access: ['ROLE_ADMIN']],
        [pattern: '/realtyphones/*', access: ['isAuthenticated()']],
        [pattern: '/image/*', access: ['permitAll']],
        [pattern: '/room/*', access: ['isAuthenticated()']],
//        [pattern: '/room/update', access: ['ROLE_ADMIN']],
//        [pattern: '/room/save', access: ['ROLE_ADMIN']],

]

grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],
        [pattern: '/**', filters: 'JOINED_FILTERS']
]

grails.plugin.springsecurity.errors.login.locked = "None shall pass."
grails.plugin.springsecurity.errors.login.disabled = "Sorry, your account is disabled."
grails.plugin.springsecurity.errors.login.expired = "Sorry, your account has expired."
grails.plugin.springsecurity.errors.login.passwordExpired = "Sorry, your password has expired."
grails.plugin.springsecurity.errors.login.locked = "Sorry, your account is locked."
grails.plugin.springsecurity.errors.login.fail = "Sorry, we were not able to find a user with that username and password."


