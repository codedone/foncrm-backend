ALTER TABLE rooms ADD shareable BIT(1) DEFAULT NULL  NULL;
ALTER TABLE rooms ADD minimal_part DECIMAL(19,2) DEFAULT NULL  NULL;

UPDATE rooms SET shareable = TRUE where realty in (SELECT id from realties where realties.shareable = TRUE);
UPDATE rooms SET shareable = FALSE where realty in (SELECT id from realties where realties.shareable = FALSE);
UPDATE rooms,realties SET rooms.minimal_part = realties.minimal_part where rooms.realty = realties.id;

ALTER TABLE realties DROP shareable;
ALTER TABLE realties DROP minimal_part;