ALTER TABLE realty_offer_types
  ADD PRIMARY KEY (realty_id, offer_type);

ALTER TABLE realty_purposes
  ADD PRIMARY KEY (realty_id, realty_purpose_type_id);

ALTER TABLE realty_purpose_types
  DROP COLUMN version;

INSERT INTO realty_purpose_types VALUES
  (3, 'Торговое помещение'),
  (4, 'ПСН'),
  (5, 'Земельные участки/Открытые площадки'),
  (6, 'Отдельно стоящее здание');

UPDATE realty_purpose_types
SET name = 'Склад/Производство'
WHERE id = 2;

INSERT IGNORE INTO realty_purposes SELECT
                                     r.id,
                                     2
                                   FROM realties r LEFT JOIN realty_purposes rp ON rp.realty_id = r.id
                                   WHERE rp.realty_purpose_type_id IS NULL;