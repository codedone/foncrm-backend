CREATE TABLE addresses (
  id       BIGINT      NOT NULL AUTO_INCREMENT,
  version  BIGINT      NOT NULL,
  building LONGTEXT    NOT NULL,
  city     LONGTEXT    NOT NULL,
  country  VARCHAR(32) NOT NULL,
  location GEOMETRY,
  region   VARCHAR(32) NOT NULL,
  room     LONGTEXT,
  street   LONGTEXT    NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE comments (
  id      BIGINT   NOT NULL AUTO_INCREMENT,
  version BIGINT   NOT NULL,
  author  BIGINT   NOT NULL,
  comment LONGTEXT,
  created DATETIME NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE comments_cp (
  customer   BIGINT NOT NULL,
  comment_id BIGINT
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE comments_orders (
  order_id   BIGINT NOT NULL,
  comment_id BIGINT
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE comments_realties (
  realty_id  BIGINT NOT NULL,
  comment_id BIGINT
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE counterparties (
  id      BIGINT       NOT NULL AUTO_INCREMENT,
  version BIGINT       NOT NULL,
  created DATETIME     NOT NULL,
  email   VARCHAR(32)  NOT NULL,
  info    LONGTEXT,
  name    VARCHAR(128) NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE counterparty_phones (
  phone VARCHAR(32) NOT NULL,
  owner BIGINT      NOT NULL,
  PRIMARY KEY (phone, owner)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE order_proposals (
  id      BIGINT       NOT NULL AUTO_INCREMENT,
  version BIGINT       NOT NULL,
  created DATETIME     NOT NULL,
  oid     BIGINT       NOT NULL,
  rid     BIGINT       NOT NULL,
  state   VARCHAR(255) NOT NULL,
  updated DATETIME     NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE orders (
  id                 BIGINT         NOT NULL AUTO_INCREMENT,
  version            BIGINT         NOT NULL,
  assignee           BIGINT         NOT NULL,
  created            DATETIME       NOT NULL,
  customer           BIGINT         NOT NULL,
  info               LONGTEXT       NOT NULL,
  max_price          DECIMAL(19, 2) NOT NULL,
  min_price          DECIMAL(19, 2) NOT NULL,
  name               LONGTEXT       NOT NULL,
  realty_suggestions LONGTEXT,
  region             GEOMETRY       NOT NULL,
  region_suggestions LONGTEXT,
  state              VARCHAR(255)   NOT NULL,
  updated            DATETIME       NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE realties (
  id         BIGINT   NOT NULL AUTO_INCREMENT,
  version    BIGINT   NOT NULL,
  address_id BIGINT   NOT NULL,
  created    DATETIME NOT NULL,
  info       LONGTEXT,
  name       LONGTEXT NOT NULL,
  owner      BIGINT   NOT NULL,
  updated    DATETIME NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE realty_phone (
  realty_id       BIGINT NOT NULL,
  realty_phone_id BIGINT
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE realty_phones (
  id      BIGINT      NOT NULL AUTO_INCREMENT,
  version BIGINT      NOT NULL,
  phone   VARCHAR(32) NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE realty_purpose_types (
  id      BIGINT       NOT NULL AUTO_INCREMENT,
  version BIGINT       NOT NULL,
  name    VARCHAR(128) NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE realty_purposes (
  realty_id              BIGINT NOT NULL,
  realty_purpose_type_id BIGINT
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE roles (
  id        BIGINT      NOT NULL AUTO_INCREMENT,
  version   BIGINT      NOT NULL,
  authority VARCHAR(20) NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE user_phones (
  phone   VARCHAR(32) NOT NULL,
  user_id BIGINT      NOT NULL,
  PRIMARY KEY (phone, user_id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE user_roles_ (
  user BIGINT NOT NULL,
  role BIGINT NOT NULL,
  PRIMARY KEY (user, role)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

CREATE TABLE users (
  id       BIGINT       NOT NULL AUTO_INCREMENT,
  version  BIGINT       NOT NULL,
  active   BIT          NOT NULL,
  email    VARCHAR(32)  NOT NULL,
  name     VARCHAR(128) NOT NULL,
  password VARCHAR(64)  NOT NULL,
  username VARCHAR(64)  NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET 'utf8'
  COLLATE 'utf8_unicode_ci';

ALTER TABLE counterparties
  ADD CONSTRAINT uk_counterparty_email UNIQUE (email);

ALTER TABLE roles
  ADD CONSTRAINT uk_role_authority UNIQUE (authority);

ALTER TABLE users
  ADD CONSTRAINT uk_user_email UNIQUE (email);

ALTER TABLE users
  ADD CONSTRAINT uk_user_username UNIQUE (username);

CREATE INDEX login_pass ON users (password, username);

ALTER TABLE comments
  ADD CONSTRAINT author__users_fkey FOREIGN KEY (author) REFERENCES users (id);

ALTER TABLE comments_cp
  ADD CONSTRAINT cp_comment_id__comments_fkey FOREIGN KEY (comment_id) REFERENCES comments (id);

ALTER TABLE comments_cp
  ADD CONSTRAINT cp_customer__counterparties_fkey FOREIGN KEY (customer) REFERENCES counterparties (id);

ALTER TABLE comments_orders
  ADD CONSTRAINT co_comment_id__comments_fkey FOREIGN KEY (comment_id) REFERENCES comments (id);

ALTER TABLE comments_orders
  ADD CONSTRAINT order_id__orders_fkey FOREIGN KEY (order_id) REFERENCES orders (id);

ALTER TABLE comments_realties
  ADD CONSTRAINT cr_comment_id__comments_fkey FOREIGN KEY (comment_id) REFERENCES comments (id);

ALTER TABLE comments_realties
  ADD CONSTRAINT realty_id__realties_fkey FOREIGN KEY (realty_id) REFERENCES realties (id);

ALTER TABLE counterparty_phones
  ADD CONSTRAINT cp_owner__counterparties_fkey FOREIGN KEY (owner) REFERENCES counterparties (id);

ALTER TABLE order_proposals
  ADD CONSTRAINT oid__orders_fkey FOREIGN KEY (oid) REFERENCES orders (id);

ALTER TABLE order_proposals
  ADD CONSTRAINT rid__realties_fkey FOREIGN KEY (rid) REFERENCES realties (id);

ALTER TABLE orders
  ADD CONSTRAINT assignee__users_fkey FOREIGN KEY (assignee) REFERENCES users (id);

ALTER TABLE orders
  ADD CONSTRAINT o_customer__counterparties_fkey FOREIGN KEY (customer) REFERENCES counterparties (id);

ALTER TABLE realties
  ADD CONSTRAINT address_id__addresses_fkey FOREIGN KEY (address_id) REFERENCES addresses (id);

ALTER TABLE realties
  ADD CONSTRAINT r_owner__counterparties_fkey FOREIGN KEY (owner) REFERENCES counterparties (id);

ALTER TABLE realty_phone
  ADD CONSTRAINT realty_phone_id__realty_hones_fkey FOREIGN KEY (realty_phone_id) REFERENCES realty_phones (id);

ALTER TABLE realty_phone
  ADD CONSTRAINT rp_realty_id__realties_fkey FOREIGN KEY (realty_id) REFERENCES realties (id);

ALTER TABLE realty_purposes
  ADD CONSTRAINT realty_purpose_type_id__realty_purpose_types_fkey FOREIGN KEY (realty_purpose_type_id) REFERENCES realty_purpose_types (id);

ALTER TABLE realty_purposes
  ADD CONSTRAINT rpp_realty_id__realties_fkey FOREIGN KEY (realty_id) REFERENCES realties (id);

ALTER TABLE user_phones
  ADD CONSTRAINT user_id__users_fkey FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE user_roles_
  ADD CONSTRAINT user__users_fkey FOREIGN KEY (user) REFERENCES users (id);

ALTER TABLE user_roles_
  ADD CONSTRAINT role__roles_fkey FOREIGN KEY (role) REFERENCES roles (id);

# insert roles
INSERT INTO roles (id, version, authority) VALUES (1, 1, 'ROLE_ADMIN'), (2, 1, 'ROLE_USER');

#insert realty purpose types
INSERT INTO realty_purpose_types (id, version, name)
VALUES (1, 1, 'Офис'), (2, 1, 'Склад');