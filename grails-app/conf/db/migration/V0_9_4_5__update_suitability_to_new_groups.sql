UPDATE order_proposals, orders,realties,rooms SET order_proposals.excellent = true WHERE
order_proposals.oid = orders.id and order_proposals.rid = realties.id AND realties.id = rooms.realty AND
(orders.min_square IS NULL OR rooms.square IS NOT NULL and orders.min_square <= rooms.square) AND
(orders.max_square IS NULL OR rooms.square IS NOT NULL and orders.max_square >= rooms.square);

UPDATE order_proposals, orders,realties,rooms SET order_proposals.shareable = true WHERE
  order_proposals.oid = orders.id and order_proposals.rid = realties.id AND realties.id = rooms.realty AND
  rooms.shareable and rooms.minimal_part IS NOT NULL and (orders.max_square IS NULL OR rooms.minimal_part < orders.max_square);

update order_proposals,(SELECT order_proposals.id as oid, orders.min_square as ms, sum(square) as s from order_proposals,orders,realties,rooms WHERE
order_proposals.oid = orders.id and order_proposals.rid = realties.id AND realties.id = rooms.realty
GROUP BY order_proposals.id) x SET order_proposals.bysquare = TRUE
  where order_proposals.id = x.oid and x.ms <= s
;