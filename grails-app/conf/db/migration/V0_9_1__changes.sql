CREATE TABLE offer_types (
  id    BIGINT      NOT NULL AUTO_INCREMENT,
  label VARCHAR(24) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO offer_types VALUES (1, 'Аренда'), (2, 'Продажа');

CREATE TABLE realty_offer_types (
  realty_id  BIGINT NOT NULL,
  offer_type BIGINT
);

ALTER TABLE realty_offer_types
  ADD CONSTRAINT offer_type_id__offer_types_fkey FOREIGN KEY (offer_type) REFERENCES offer_types (id),
  ADD CONSTRAINT ot_realty_id__realties_fkey FOREIGN KEY (realty_id) REFERENCES realties (id);

INSERT INTO realty_offer_types SELECT
                                 realty_id,
                                 1
                               FROM realty_purposes
                               WHERE realty_purpose_type_id = 3;

INSERT INTO realty_offer_types SELECT
                                 realty_id,
                                 2
                               FROM realty_purposes
                               WHERE realty_purpose_type_id = 4;

INSERT IGNORE INTO realty_offer_types SELECT
                                        r.id,
                                        1
                                      FROM realties r LEFT JOIN realty_offer_types rot ON rot.realty_id = r.id
                                      WHERE rot.offer_type IS NULL;

DELETE FROM realty_purposes
WHERE realty_purpose_type_id = 3;
DELETE FROM realty_purposes
WHERE realty_purpose_type_id = 4;

DELETE FROM realty_purpose_types
WHERE id IN (3, 4);