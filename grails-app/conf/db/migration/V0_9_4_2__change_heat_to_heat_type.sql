ALTER TABLE rooms ADD heat_type VARCHAR(255) DEFAULT NULL  NULL;
UPDATE rooms SET heat_type="HEATED" where heated = TRUE;
UPDATE rooms SET heat_type="COLD" where heated = false;
ALTER TABLE rooms DROP heated;