CREATE TABLE rooms
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    version BIGINT(20) NOT NULL,
    height DECIMAL(19,2),
    created DATETIME NOT NULL,
    electric_power DECIMAL(19,2),
    floor VARCHAR(32) NOT NULL,
    info VARCHAR(128),
    heated BIT(1),
    price_per_meter DECIMAL(19,2),
    price_type VARCHAR(255) NOT NULL,
    rampant BIT(1),
    realty BIGINT(20) NOT NULL,
    remont VARCHAR(255),
    plan_type VARCHAR(255),
    room_type VARCHAR(255) NOT NULL,
    square DECIMAL(19,2) NOT NULL,
    total_price DECIMAL(19,2) NOT NULL,
    updated DATETIME NOT NULL,
    window_type VARCHAR(255),
    furniture BIT(1),
    CONSTRAINT FK_rooms_realty__realties_id FOREIGN KEY (realty) REFERENCES realties (id)
);
CREATE INDEX FK_rooms_realty ON rooms (realty);

CREATE TABLE comments_rooms
(
    room_id BIGINT(20) NOT NULL,
    comment_id BIGINT(20),
    CONSTRAINT FK_comments_rooms_room_id__rooms_id FOREIGN KEY (room_id) REFERENCES rooms (id),
    CONSTRAINT FK_comments_rooms_comment_id__comments_id FOREIGN KEY (comment_id) REFERENCES comments (id)
);
CREATE INDEX FK_comments_rooms_comment_id ON comments_rooms (comment_id);
CREATE INDEX FK_comments_rooms_room_id ON comments_rooms (room_id);


CREATE TABLE room_images
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    is_main BIT(1) NOT NULL,
    room_id BIGINT(20),
    CONSTRAINT FK_room_images_id__images_id FOREIGN KEY (id) REFERENCES images (id),
    CONSTRAINT FK_room_images_room_id__rooms_id FOREIGN KEY (room_id) REFERENCES rooms (id)
);
CREATE INDEX FK_room_images_room_id ON room_images (room_id);


