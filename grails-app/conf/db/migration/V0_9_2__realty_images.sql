CREATE TABLE images (
  id           BIGINT       NOT NULL AUTO_INCREMENT,
  content_type VARCHAR(255) NOT NULL,
  name         VARCHAR(255) NOT NULL,
  created      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  path         VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE realty_images (
  id        BIGINT NOT NULL,
  is_main   BIT    NOT NULL,
  realty_id BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT im_id__images_fkey FOREIGN KEY (id) REFERENCES images (id),
  CONSTRAINT ri_realty_id__realties FOREIGN KEY (realty_id) REFERENCES realties (id)
);