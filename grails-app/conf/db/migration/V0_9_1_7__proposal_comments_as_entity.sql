DROP PROCEDURE IF EXISTS TakeOutComments;
-- MySQL procedure
DELIMITER //
CREATE DEFINER = CURRENT_USER PROCEDURE TakeOutComments()
  BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE comment_count INTEGER;
    DECLARE taked_out INTEGER DEFAULT 0;
    DECLARE proposal_id BIGINT;
    DECLARE comment_text LONGTEXT;
    DECLARE comment_cursor CURSOR FOR SELECT
                                        id,
                                        comment
                                      FROM order_proposals
                                      WHERE comment IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      ROLLBACK;
    END;
    START TRANSACTION;
    SELECT count(id)
    INTO comment_count
    FROM order_proposals
    WHERE comment IS NOT NULL;

    OPEN comment_cursor;
    FETCH comment_cursor
    INTO proposal_id, comment_text;
    WHILE done = FALSE DO
      INSERT INTO comments (version, author, comment, created) VALUE (0, 1, comment_text, NOW());
      UPDATE order_proposals
      SET comment_id = LAST_INSERT_ID();
      SET taked_out = taked_out + 1;
      FETCH comment_cursor
      INTO proposal_id, comment_text;
    END WHILE;

    IF taked_out = comment_count
    THEN
      BEGIN
        COMMIT;
        ALTER TABLE order_proposals
          DROP COLUMN comment;
      END;
    ELSE
      ROLLBACK;
    END IF;
  END //
DELIMITER ;

ALTER TABLE order_proposals
  ADD COLUMN comment_id BIGINT NULL  DEFAULT NULL,
  ADD CONSTRAINT op__comment_id__comments_fkey FOREIGN KEY (comment_id) REFERENCES comments (id);

CALL TakeOutComments();
DROP PROCEDURE IF EXISTS TakeOutComments;