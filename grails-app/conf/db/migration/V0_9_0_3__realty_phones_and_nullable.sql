ALTER TABLE realty_phone
  DROP
  FOREIGN KEY realty_phone_id__realty_hones_fkey,
  DROP
  FOREIGN KEY rp_realty_id__realties_fkey;

RENAME TABLE
    realty_phones TO tmp_realty_phones;
RENAME TABLE
    realty_phone TO jt_realty_phones;

CREATE TABLE `realty_phones` (
  `realty` BIGINT(20)              NOT NULL,
  `phone`  VARCHAR(32)
           COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`realty`, `phone`),
  CONSTRAINT `realty__realties_fkey`
  FOREIGN KEY (`realty`) REFERENCES `realties` (`id`)
)
  ENGINE = InnoDB;

INSERT
IGNORE INTO realty_phones (realty, phone)
  SELECT
    jtrp.realty_id,
    trp.phone
  FROM tmp_realty_phones trp
    INNER JOIN jt_realty_phones jtrp ON trp.id = jtrp.realty_phone_id;

DROP TABLE tmp_realty_phones;
DROP TABLE jt_realty_phones;

ALTER TABLE addresses
  MODIFY COLUMN street VARCHAR(256) NULL DEFAULT NULL,
  MODIFY COLUMN building VARCHAR(256) NULL DEFAULT NULL,
  MODIFY COLUMN room VARCHAR(256) NULL DEFAULT NULL,
  DROP COLUMN version;

UPDATE addresses
SET location = POINT(0, 0)
WHERE location IS NULL;

ALTER TABLE addresses
  MODIFY COLUMN location GEOMETRY NOT NULL;