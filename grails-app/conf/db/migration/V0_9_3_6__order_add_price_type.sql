ALTER TABLE orders ADD price_type VARCHAR(255) DEFAULT NULL NULL;
UPDATE orders SET price_type = 'PERMONTH' WHERE (min_price IS not null or max_price is not null) and id in (select order_id from order_offer_types where offer_type = 1);
ALTER table orders modify column min_price DECIMAL(19,2) DEFAULT NULL NULL;
ALTER table orders modify column max_price DECIMAL(19,2) DEFAULT NULL NULL;
ALTER table orders add min_price_per_meter DECIMAL(19,2) DEFAULT NULL NULL;
ALTER table orders add max_price_per_meter DECIMAL(19,2) DEFAULT NULL NULL;