ALTER TABLE order_proposals ADD excellent BIT DEFAULT 0 NOT NULL;
ALTER TABLE order_proposals ADD shareable BIT DEFAULT 0 NOT NULL;
ALTER TABLE order_proposals ADD bysquare BIT DEFAULT 0 NOT NULL;
ALTER TABLE order_proposals DROP suitability;