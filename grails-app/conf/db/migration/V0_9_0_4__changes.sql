INSERT INTO users (id, version, active, email, name, password, username)
VALUES (-1, 1, FALSE, 'noone@spam.su', 'Не указано', 'super dooper password', 'noone');

ALTER TABLE addresses
  ADD SPATIAL INDEX (location);
ALTER TABLE orders
  ADD SPATIAL INDEX (region);

ALTER TABLE orders
  DROP FOREIGN KEY o_customer__counterparties_fkey;

ALTER TABLE orders
  CHANGE COLUMN customer counterparty BIGINT NOT NULL,
  ADD CONSTRAINT o_counterparty__counterparties_fkey FOREIGN KEY (counterparty) REFERENCES counterparties (id);