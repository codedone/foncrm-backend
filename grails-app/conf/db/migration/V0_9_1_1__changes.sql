ALTER TABLE realty_offer_types
  MODIFY COLUMN offer_type BIGINT NOT NULL;
ALTER TABLE realty_purposes
  MODIFY COLUMN realty_purpose_type_id BIGINT NOT NULL;