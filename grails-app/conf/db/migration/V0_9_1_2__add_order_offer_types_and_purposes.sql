CREATE TABLE order_offer_types (
  order_id   BIGINT NOT NULL,
  offer_type BIGINT
);

ALTER TABLE order_offer_types
  ADD CONSTRAINT order_offer_type_id__offer_types_fkey FOREIGN KEY (offer_type) REFERENCES offer_types (id),
  ADD CONSTRAINT ot_order_id__orders_fkey FOREIGN KEY (order_id) REFERENCES orders (id);

INSERT INTO order_offer_types (order_id, offer_type)
  SELECT
    id,
    1
  FROM orders;

CREATE TABLE order_purposes (
  order_id               BIGINT     NOT NULL,
  realty_purpose_type_id BIGINT(20) NOT NULL
);
ALTER TABLE order_purposes
  ADD CONSTRAINT order_realty_purpose_type_id__realty_purpose_types_fkey FOREIGN KEY (realty_purpose_type_id) REFERENCES realty_purpose_types (id),
  ADD CONSTRAINT rpp_order_id__orders_fkey FOREIGN KEY (order_id) REFERENCES orders (id);

INSERT INTO order_purposes (order_id, realty_purpose_type_id)
  SELECT
    id,
    2
  FROM orders;