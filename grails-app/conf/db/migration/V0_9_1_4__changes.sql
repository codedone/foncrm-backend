ALTER TABLE order_offer_types
  MODIFY COLUMN offer_type BIGINT NOT NULL,
  ADD PRIMARY KEY (order_id, offer_type);

ALTER TABLE order_purposes
  ADD PRIMARY KEY (order_id, realty_purpose_type_id);

