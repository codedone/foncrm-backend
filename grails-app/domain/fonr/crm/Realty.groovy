package fonr.crm

import org.hibernate.SQLQuery
import org.hibernate.SessionFactory

class Realty {

    transient SessionFactory sessionFactory
    transient imageService

    static transients = ['sessionFactory', 'imageService']

    Long id
    Date created
    Date updated
    String info
    Counterparty owner
    Address address


    static mapping = {
        table 'realties'
        id name: 'id'
        owner column: 'owner', fetch: 'join'
        comments joinTable: [name: 'comments_realties', key: 'realty_id']
        purposes joinTable: [name: 'realty_purposes', key: 'realty_id']
        rooms joinTable: [name: 'rooms', key: 'realty'], fetch: 'join'
        offerTypes joinTable: [name: 'realty_offer_types', key: 'realty_id', column: 'offer_type']
        phones cascade: 'all-delete-orphan', fetch: 'select'
        address fetch: 'join'
        images sort: 'isMain', order: 'desc', fetch: 'join'

    }

    static constraints = {
        info maxSize: 4096, nullable: true
        address nullable: false, blank: false, validator: { Address add, Realty r ->
            if (add != null) {
                if (!add.validate()) {
                    return ['invalid']
                } else if (r.owner != null) {
                    def query = """select count(distinct r) from Realty r
 inner join r.address address where r.owner.id = :owner_id and address.country = :country
 and address.region = :region and address.city = :city
 and address.street ${add.street == null ? 'is null' : '= :street'}
 and address.building ${add.building == null ? 'is null' : '= :building'}
 and address.room ${add.room == null ? 'is null' : '= :room'}
 ${r.id != null ? 'and r.id <> :id' : ''}"""

                    def params = [
                            owner_id: r.owner.id,
                            country : add.country,
                            region  : add.region,
                            city    : add.city
                    ]
                    if (add.street != null)
                        params.street = add.street

                    if (add.building != null)
                        params.building = add.building

                    if (add.room != null)
                        params.room = add.room

                    if (r.id != null)
                        params.id = r.id

                    def count = Realty.executeQuery(query, params)
                    if (count[0] > 0) {
                        return ['unique']
                    }
                }
            }
            return true
        }

        purposes minSize: 1, validator: {
            Set<RealtyPurposeType> rpts, Realty r ->
                if (rpts != null && rpts.contains(null)) {
                    return ['nullInList']
                }
                return true
        }
        offerTypes minSize: 1, maxSize: 1, validator: {
            Set<OfferType> ots, Realty r ->
                if (ots != null && ots.contains(null)) {
                    return ['nullInList']
                }
                return true
        }
        images validator: {
            Set<RealtyImage> set, Realty r ->
                if (set != null && !set.empty) {
                    if (set.contains(null)) {
                        return ['notFound']
                    } else {
                        def mainImages = set.findAll { image -> image?.isMain }
                        if (mainImages.size() > 1) {
                            return ['mainImage.multiple']
                        } else if (mainImages.size() < 1) {
                            return ['noMainImage']
                        }
                    }
                }
                return true
        }
    }

    static hasMany = [comments: Comment, purposes: RealtyPurposeType, phones: RealtyPhone, offerTypes: OfferType, images: RealtyImage, rooms: Room]

    def beforeUpdate() {
        updated = new Date()
    }

    def afterUpdate() {
        def toDelete = []
        def deleteQuery
        def selectQuery
        def params
        if (images.isEmpty()) {
            selectQuery = 'from RealtyImage ri where ri.realty.id = :realty_id'
            deleteQuery = 'delete from RealtyImage ri where ri.realty.id = :realty_id'
            params = [realty_id: this.id]
        } else {
            selectQuery = 'from RealtyImage ri where ri.realty.id = :realty_id and ri.id not in (:ids)'
            deleteQuery = 'delete from RealtyImage ri where ri.realty.id = :realty_id and ri.id not in (:ids)'
            params = [realty_id: this.id, ids: images.collect { it.id }]
        }

        toDelete.addAll(RealtyImage.executeQuery(selectQuery, params))
        RealtyImage.executeUpdate(deleteQuery, params)
        if (!toDelete.empty)
            imageService.deleteList(toDelete)
    }

    public List<Comment> getLatestComments() {
        def session = sessionFactory.currentSession

        final SQLQuery sqlQuery = session.createSQLQuery('select c.* from comments as c inner join comments_realties cr on cr.comment_id = c.id where cr.realty_id = :realty_id order by c.created desc limit 1')
        return sqlQuery.with {
            addEntity(Comment)
            setLong('realty_id', this.id)
            list()
        }
    }

    public void resetPhones(List<String> phoneStrs) {
        def oldPhones = new ArrayList<RealtyPhone>(phones)
        phones.clear()
        if (phoneStrs != null) {
            phones.addAll(phoneStrs.collect {
                str -> oldPhones.find { p -> p.phone == str } ?: new RealtyPhone(phone: str, realty: this)
            })
        }
    }

    public void resetPurposes(List<Long> purposesIds) {
        def oldPuposes = new ArrayList<RealtyPurposeType>(purposes)
        purposes.clear()
        if (purposesIds != null) {
            purposes.addAll(purposesIds.collect {
                pid -> oldPuposes.find { p -> p.id == pid } ?: RealtyPurposeType.get(pid)
            })
        }
    }

    public void resetOfferTypes(List<Long> newOfferTypes) {
        def oldTypes = new ArrayList(offerTypes)
        offerTypes.clear()
        if (newOfferTypes != null) {
            offerTypes.addAll(
                    newOfferTypes.collect {
                        id -> oldTypes.find { ot -> ot.id == id } ?: OfferType.get(id)
                    } as Collection<? extends OfferType>
            )
        }
    }

    public void resetImages(List<Long> ids) {
        def oldImages = new ArrayList(images)
        images.clear()
        if (ids != null) {
            images.addAll(ids.collect {
                id ->
                    def image = oldImages.find { im -> im.id == id } ?: RealtyImage.get(id)
                    if (image?.realty == null) image.realty = this
                    return image

            } as Collection<RealtyImage>)
        }
    }

    public void resetMainImage(Long id) {
        def oldMainImage = images.find { image -> image?.isMain }
        if (oldMainImage == null || oldMainImage.id != id) {
            def newMainImage = images.find { it?.id == id }
            if (newMainImage != null) {
                if (oldMainImage != null) {
                    oldMainImage.isMain = false
                }
                newMainImage.isMain = true
            }
        }
    }
}
