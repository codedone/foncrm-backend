package fonr.crm

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.geom.Point
import groovy.transform.ToString
import org.hibernate.spatial.GeometryType

@ToString(includes = ['id', 'country', 'region', 'city', 'street', 'building', 'room'], includeNames = true, includePackage = false)
class Address implements Serializable {
    Long id
//    String zip
    String country
    String region
    String city
    String street
    String building
    String room
    Point location

    static mapping = {
        table 'addresses'
        version false
        id name: 'id'
        location type: GeometryType
    }

    static constraints = {
//        zip maxSize: 128, blank: true, nullable: true
        country maxSize: 32
        region maxSize: 32
        city maxSize: 256
        street maxSize: 256, nullable: true
        building maxSize: 256, nullable: true
        room maxSize: 256, nullable: true
        location nullable: false
    }

    public void setLocationByCoordinates(Double lat, Double lon) {
        if (lat && lon) {
            def gf = new GeometryFactory()
            location = gf.createPoint(new Coordinate(lat, lon))
        }
    }

    public static String joinAddressString(Address address) {
        def addressParts = []
        if (address.country)
            addressParts.add(address.country)
        if (address.region)
            addressParts.add(address.region)
        if (address.city)
            addressParts.add(address.city)
        if (address.street)
            addressParts.add(address.street)
        if (address.building)
            addressParts.add(address.building)
        if (address.room)
            addressParts.add(address.room)
        return String.join(', ', addressParts)
    }
}
