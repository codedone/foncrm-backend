package fonr.crm

class RealtyPurposeType {
    Long id
    String name


    static constraints = {
        name maxSize: 128
    }

    static mapping = {
        table 'realty_purpose_types'
        id name: 'id'
        version false
    }
}
