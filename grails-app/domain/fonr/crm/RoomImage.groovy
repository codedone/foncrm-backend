package fonr.crm

class RoomImage extends Image {

    transient imageService
    static transients = ['imageService']

    Boolean isMain = false

    static mapping = {
        table 'room_images'
        version false
        room column: 'room_id'
    }

    static constraints = {
        room nullable: true
    }

    static belongsTo = [room: Room]

    def afterDelete() {
        imageService.deleteImageFiles(this)
    }
}
