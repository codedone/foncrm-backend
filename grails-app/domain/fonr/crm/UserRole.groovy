package fonr.crm

import grails.gorm.DetachedCriteria
import org.apache.commons.lang.builder.HashCodeBuilder

class UserRole implements Serializable {
    private static final long serialVersionUID = 1


    User u
    Role r

//    UserRole(User u, String auth) {
//        uid = u
//        authority = auth
//    }

    @Override
    boolean equals(other) {
        System.out.println("Other - ${other}")
        System.out.println("This - ${this}")
        if (!(other instanceof UserRole)) {
            print("Result - false")
            return false
        }

        def result = other.u?.id == u?.id && r?.id == other.r?.id
        print("Result - ${result}")
        return result
    }

    @Override
    int hashCode() {
        def builder = new HashCodeBuilder()
        if (u) builder.append(u.id)
        if (r) builder.append(r.id)
        builder.toHashCode()
    }

    static UserRole get(long userId, long roleId) {
        criteriaFor(userId, roleId).get()
    }

    static boolean exists(long userId, long roleId) {
        criteriaFor(userId, roleId).count()
    }

    private static DetachedCriteria criteriaFor(long userId, long roleId) {
        UserRole.where {
            u == User.load(userId) &&
                    Role.load(roleId) == r
        }
    }

    static UserRole create(User user, Role role, boolean flush = false) {
        def instance = new UserRole(u: user, r: role)
        instance.save(flush: flush, insert: true)
        instance
    }

    static boolean remove(User user, Role role, boolean flush = false) {
        if (user == null || role == null) return false

        int rowCount = UserRole.where { u == user && r == role }.deleteAll()

        if (flush) { UserRole.withSession { it.flush() } }

        rowCount
    }

    static void removeAll(User user, boolean flush = false) {
        if (user == null) return

        UserRole.where { u == user }.deleteAll()

        if (flush) { UserRole.withSession { it.flush() } }
    }

    static void removeAll(Role role, boolean flush = false) {
        if (role == null) return

        UserRole.where { r == role }.deleteAll()

        if (flush) { UserRole.withSession { it.flush() } }
    }

    static constraints = {
        r validator: { Role r, UserRole ur ->
            if ((ur.u != null && ur.u.id == null) || ur.r.id == null) return
            boolean existing = false
            UserRole.withNewSession {
                if(ur.u != null){
                    existing = exists(ur.u.id, ur.r.id)
                }
            }
            if (existing) {
                return 'userRole.exists'
            }
        }
    }

    static belongsTo = [u: User, r: Role]

    static mapping = {
        table 'user_roles_'
        u column: 'user'
        r column: 'role'
        id composite: ['u', 'r']
        version false
    }
}
