package fonr.crm

import org.hibernate.SQLQuery
import org.hibernate.SessionFactory

import java.math.MathContext
import java.math.RoundingMode

class Room {

    transient SessionFactory sessionFactory
    transient imageService

    static transients = ['sessionFactory', 'imageService']

    Long id
    String info
    Realty realty
    Date created
    Date updated
    BigDecimal square // in square meters
    BigDecimal ceilingHeight // in meters
    Integer floorNumber
    BigDecimal pricePerSquareMeter // in rubles
    BigDecimal totalPrice // in rubles
    PriceType priceType
    RoomType roomType
    Boolean isLeasedOut = false

    Boolean shareable = false
    BigDecimal minimalPart = null

    // only for stocks
    HeatType heatType
    Boolean rampantAvailable
    BigDecimal dedicatedElectricPower // in Watts

    //only for offices
    RoomPlanType roomPlanType
    RoomCondition roomCondition
    WindowType windowType
    Boolean withFurniture

    static mapping = {
        table 'rooms'
        id name: 'id'
        realty column: 'realty', fetch: 'join'
        isLeasedOut column: 'leased'

        shareable column: 'shareable'
        minimalPart column: 'minimal_part'

        ceilingHeight column: "height"
        floorNumber column: "floor"
        square column: "square"//, index: 'cost_idx'
        pricePerSquareMeter column: "price_per_meter"//, index: 'cost_idx'
        totalPrice column: "total_price"//, index: 'cost_idx'
        withFurniture column: "furniture"

        rampantAvailable column: 'rampant'
        dedicatedElectricPower column: 'electric_power'

        priceType enumType: "string", column: "price_type"//, index: 'cost_idx'
        roomType enumType: "string", column: "room_type"
        roomPlanType enumType: "string", column: "plan_type"
        roomCondition enumType: "string", column: "remont"
        windowType enumType: "string", column: "window_type"
        heatType enumType: "string", column: 'heat_type'

        comments joinTable: [name: 'comments_rooms', key: 'room_id']
        images sort: 'isMain', order: 'desc', fetch: 'join'
    }

    static belongsTo = [realty: Realty]

    static constraints = {
        info maxSize: 4096, blank: true, nullable: true
        realty nullable: false
        square nullable: false, min: new BigDecimal("0.0")
        //        , validator: {
//            BigDecimal value, Room r ->
//                if (r.realty.shareable && r.realty.minimalPart > value) {
//                    return ['lessThanMinimalPart']
//                }
//                return true
//        }
        isLeasedOut nullable: false

        shareable nullable: true, validator: {
            Boolean value, Room r ->
                if (value != null && value && r.realty != null) {
                    if (r.realty.offerTypes.id.contains(2L)) {
                        return ["onSaleCanNotBeShareable"]
                    }
                }
                return true
        }
        minimalPart nullable: true, min: new BigDecimal("0.0"), validator: {
            BigDecimal value, Room r ->
                if (r.realty != null && r.realty.offerTypes.id.contains(2L)) {
                    if (value != null) {
                        return ["isNotNullForSale"]
                    }
                } else {
                    if (r.shareable != null && r.shareable && value == null) {
                        return ["isNull"]
                    }
                    if ((r.shareable == null || !r.shareable) && value != null) {
                        return ["isNotNull"]
                    }
                    if (r.shareable != null && r.shareable && value != null
                            && r.square != null && value > r.square) {
                        return ["squareIsLessThanMinimalPart"]
                    }
                }
                return true
        }

        roomType nullable: false
        floorNumber nullable: false
        pricePerSquareMeter nullable: true, min: new BigDecimal("0.0")
        totalPrice nullable: false, min: new BigDecimal("0.0")
        priceType nullable: true, validator: {
            PriceType value, Room r ->
                if (r.realty != null && r.realty.offerTypes.id.contains(2L)) {
                    // объект на продажу
                    if (value != null) {
                        return ['isNotNull']
                    }
                } else {
                    // объект для аренды
                    if (value == null) {
                        return ['isNull']
                    }
                }
                return true
        }

        // проверки на поля, проверка которых разичается в зависимости от типа помещения
        ceilingHeight nullable: true, min: new BigDecimal("0.0"), validator: {
            BigDecimal h, Room r ->
                if(r.roomType == RoomType.STOCK && h == null){
                    return ["isNullForStock"]
                }
                return true
        }
        heatType nullable: true, validator: {
            HeatType value, Room r ->
                if(r.roomType == RoomType.STOCK && value == null){
                    return ["isNullForStock"]
                }
                if(r.roomType == RoomType.OFFICE && value != null){
                    return ["notNullForOffice"]
                }
                return true
        }
        rampantAvailable nullable: true,  validator: {
            Boolean value, Room r ->
                if(r.roomType == RoomType.STOCK && value == null){
                    return ["isNullForStock"]
                }
                if(r.roomType == RoomType.OFFICE && value != null){
                    return ["notNullForOffice"]
                }
                return true
        }
        dedicatedElectricPower nullable: true, min: new BigDecimal("0.0"), validator: {
            BigDecimal value, Room r ->
                if(r.roomType == RoomType.OFFICE && value != null){
                    return ["notNullForOffice"]
                }
                return true
        }
        roomPlanType nullable: true,  validator: {
            RoomPlanType value, Room r ->
                if(r.roomType == RoomType.STOCK && value != null){
                    return ["notNullForStock"]
                }
                return true
        }
        roomCondition nullable: true, validator: {
            RoomCondition value, Room r ->
                if(r.roomType == RoomType.STOCK && value != null){
                    return ["notNullForStock"]
                }
                return true
        }
        windowType nullable: true, validator: {
            WindowType value, Room r ->
                if(r.roomType == RoomType.STOCK && value != null){
                    return ["notNullForStock"]
                }
                return true
        }
        withFurniture nullable: true, validator: {
            Boolean value, Room r ->
                if(r.roomType == RoomType.STOCK && value != null){
                    return ["notNullForStock"]
                }
                if(r.roomType == RoomType.OFFICE && value == null){
                    return ["isNullForOffice"]
                }
                return true
        }

        images validator: {
            Set<RoomImage> set, Room r ->
                if (set != null && !set.empty) {
                    if (set.contains(null)) {
                        return ['notFound']
                    } else {
                        def mainImages = set.findAll { image -> image?.isMain }
                        if (mainImages.size() > 1) {
                            return ['mainImage.multiple']
                        } else if (mainImages.size() < 1) {
                            return ['noMainImage']
                        }
                    }
                }
                return true
        }

    }

    def beforeValidate() {
        if(square != null){
          if(pricePerSquareMeter != null){
              totalPrice = square*pricePerSquareMeter
          }else if (totalPrice != null && square != new BigDecimal("0.0")){
              pricePerSquareMeter = totalPrice.divide(square,new MathContext(2, RoundingMode.HALF_UP))
          }
        }
    }

    def beforeUpdate() {
        if(square != null){
            if(pricePerSquareMeter != null){
                totalPrice = square*pricePerSquareMeter
            }else if (totalPrice != null && square != new BigDecimal("0.0")){
                pricePerSquareMeter = totalPrice.divide(square,new MathContext(2, RoundingMode.HALF_UP))
            }
        }
        updated = new Date()
    }

    def afterUpdate(){
        def toDelete = []
        def deleteQuery
        def selectQuery
        def params
        if (images.isEmpty()) {
            selectQuery = 'from RoomImage ri where ri.room.id = :room_id'
            deleteQuery = 'delete from RoomImage ri where ri.room.id = :room_id'
            params = [room_id: this.id]
        } else {
            selectQuery = 'from RoomImage ri where ri.room.id = :room_id and ri.id not in (:ids)'
            deleteQuery = 'delete from RoomImage ri where ri.room.id = :room_id and ri.id not in (:ids)'
            params = [room_id: this.id, ids: images.collect { it.id }]
        }

        toDelete.addAll(RoomImage.executeQuery(selectQuery, params))
        RoomImage.executeUpdate(deleteQuery, params)
        if (!toDelete.empty)
            imageService.deleteList(toDelete)
    }

    def beforeInsert(){
        if(totalPrice == null && square != null && pricePerSquareMeter != null){
            totalPrice = square*pricePerSquareMeter
        }else if(pricePerSquareMeter == null && totalPrice != null && square != null){
            pricePerSquareMeter = totalPrice / square
        }
        updated = new Date()
    }

    public List<Comment> getLatestComments() {
        def session = sessionFactory.currentSession

        final SQLQuery sqlQuery = session.createSQLQuery('select c.* from comments as c inner join comments_rooms cr on cr.comment_id = c.id where cr.room_id = :room_id order by c.created desc limit 1')
        return sqlQuery.with {
            addEntity(Comment)
            setLong('room_id', this.id)
            list()
        }
    }

    static hasMany = [comments: Comment, images: RoomImage]

    public void resetImages(List<Long> ids) {
        def oldImages = new ArrayList(images)
        images.clear()
        if (ids != null) {
            images.addAll(ids.collect {
                id ->
                    def image = oldImages.find { im -> im.id == id } ?: RoomImage.get(id)
                    if (image?.room == null) image.room = this
                    return image

            } as Collection<RoomImage>)
        }
    }

    public void resetMainImage(Long id) {
        def oldMainImage = images.find { image -> image?.isMain }
        if (oldMainImage == null || oldMainImage.id != id) {
            def newMainImage = images.find { it?.id == id }
            if (newMainImage != null) {
                if (oldMainImage != null) {
                    oldMainImage.isMain = false
                }
                newMainImage.isMain = true
            }
        }
    }

    enum RoomType {
        OFFICE("office"),
        STOCK("stock")

        String name

        RoomType(String name) {
            this.name = name
        }

        public static RoomType of(String name) {
            def type = values().find {
                it.name == name
            }
            if (type == null)
                throw new IllegalArgumentException("No type of room with name ${name}".toString())

            return type
        }
    }

    enum PriceType {
        PERMONTH("permonth"),
        PERYEAR("peryear")

        String name

        PriceType(String name) {
            this.name = name
        }

        public static PriceType of(String name) {
            def type = values().find {
                it.name == "per" + name
            }
            if (type == null)
                throw new IllegalArgumentException("No type of price with name ${name}".toString())

            return type
        }

        public String nameForPrint() {
            return name.substring(3)
        }
    }

    enum RoomPlanType {
        CORIDOR("coridor"),
        OPEN("open"),
        MIXED("mixed")

        String name

        RoomPlanType(String name) {
            this.name = name
        }

        public static RoomPlanType of(String name) {
            def type = values().find {
                it.name == name
            }
            if (type == null)
                throw new IllegalArgumentException("No type of room plan with name ${name}".toString())

            return type
        }
    }

    enum RoomCondition {
        OFFICE("office"),
        CLEAN("clean"),
        CAPITAL("capital"),
        COSMETIC("cosmetic")

        String name

        RoomCondition(String name) {
            this.name = name
        }

        public static RoomCondition of(String name) {
            def type = values().find {
                it.name == name
            }
            if (type == null)
                throw new IllegalArgumentException("No type of room plan with name ${name}".toString())

            return type
        }
    }

    enum  WindowType {
        TYPICAL("typical"),
        PANORAM("panoram"),
        PIT("pit"),
        WITHOUT("without")

        String name

        WindowType(String name) {
            this.name = name
        }

        public static WindowType of(String name) {
            def type = values().find {
                it.name == name
            }
            if (type == null)
                throw new IllegalArgumentException("No type of windows with name ${name}".toString())

            return type
        }
    }

    enum HeatType {
        HEATED("heated"),
        WARMED("warmed"),
        COLD("cold"),
        OPEN("open"),
        REFRIGERATOR("refrigerator")

        String name

        HeatType(String name) {
            this.name = name
        }

        public static HeatType of(String name) {
            def type = values().find {
                it.name == name
            }
            if (type == null)
                throw new IllegalArgumentException("No type of heated with name ${name}".toString())

            return type
        }
    }
}
