package fonr.crm

import org.apache.commons.lang.builder.HashCodeBuilder

class UserPhone implements Serializable {

    String phone
    User user

    static constraints = {
        phone nullable: false, blank: false, maxSize: 32
        user nullable: false
    }

    static mapping = {
        table 'user_phones'
        version false
        user column: 'user_id'
        id composite: ['phone', 'user']
    }

    static belongsTo = [user: User]

    @Override
    boolean equals(other) {
        if (other == null) return false
        if (!(other instanceof UserPhone)) return false
        return other.phone?.equals(phone) && other.user?.id == user?.id
    }

    @Override
    int hashCode() {
        def builder = new HashCodeBuilder()
        if (phone) builder.append(phone)
        if (user) builder.append(user.id)
        return builder.toHashCode()
    }
}
