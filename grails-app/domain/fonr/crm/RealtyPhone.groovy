package fonr.crm

import grails.rest.Resource
import org.apache.commons.lang.builder.HashCodeBuilder

@Resource
class RealtyPhone implements Serializable {
    String phone
    Realty realty

    static constraints = {
        phone maxSize: 32
    }

    static mapping = {
        table 'realty_phones'
        version false
        realty column: 'realty', fetch: 'join'
        id composite: ['realty', 'phone']
    }

    static belongsTo = [realty: Realty]

    @Override
    boolean equals(Object obj) {
        if (!(obj instanceof RealtyPhone) || obj == null)
            return false

        return obj.phone == phone && obj.realty?.id == realty?.id
    }

    @Override
    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append phone
        if (realty)
            builder.append realty.id
        builder.toHashCode()
    }
}
