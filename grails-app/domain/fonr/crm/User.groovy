package fonr.crm
//@Resource(superClass = UserController, formats = "json")
class User implements Serializable {
    Long id
    String name
    String email
    String username
    Boolean active = true
    String password

    transient springSecurityService

    static mapping = {
        table 'users'
        sort name: 'asc'
        id name: 'id'
        username index: 'login_pass'
        password index: 'login_pass'
        roles joinTable: [name: 'user_roles_', key: 'user', column: 'role']
        phones cascade: 'all-delete-orphan'
    }

    static constraints = {
        name maxSize: 128
        email unique: true, maxSize: 32, email: true
        username unique: true, maxSize: 64, blank: false
        password maxSize: 64, blank: false, password: true
        phones minSize: 1
        roles minSize: 1
    }

    static transients = ['springSecurityService']

    static hasMany = [comments: Comment, orders: Order, phones: UserPhone, roles: Role]

    Set<Role> getAuthorities() {
        def result = UserRole.findAllByU(this)*.r
        return result
    }

    public boolean isAdmin() {
        return roles.any { it.authority == Role.NAME_OF_ROLE_ADMIN }
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

//    def afterUpdate() {
//        print("After Update")
//        UserRole.removeAll(this)
//        for(UserRole role : roles){
//            UserRole.create(role.u,role.r).save(true)
//        }
//    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ?
                springSecurityService.encodePassword(password) :
                password
    }

    boolean getAccountExpired() {
        return false;
    }

    boolean getAccountLocked() {
        return false;
    }

    boolean getPasswordExpired() {
        return false;
    }

    public void resetPhones(List<String> phoneStrs) {
        def oldPhones = new ArrayList<UserPhone>(phones)
        phones.clear()
        if (phoneStrs != null) {
            phones.addAll(phoneStrs.collect {
                str -> oldPhones.find { p -> p.phone == str } ?: new UserPhone(phone: str, user: this)
            })
        }
    }
}
