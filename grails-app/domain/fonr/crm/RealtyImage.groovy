package fonr.crm

class RealtyImage extends Image {

    transient imageService
    static transients = ['imageService']

    Boolean isMain = false

    static mapping = {
        table 'realty_images'
        version false
        realty column: 'realty_id'
    }

    static constraints = {
        realty nullable: true
    }

    static belongsTo = [realty: Realty]

    def afterDelete() {
        imageService.deleteImageFiles(this)
    }
}
