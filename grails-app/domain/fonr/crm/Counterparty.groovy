package fonr.crm

import net.sf.ehcache.search.aggregator.Count
import org.hibernate.SessionFactory

class Counterparty {

    transient SessionFactory sessionFactory

    static transients = ['sessionFactory']

    Long id
    Date created
    String name
    String email
    String info

    static mapping = {
        table 'counterparties'
        id name: 'id'
        comments joinTable: [name: 'comments_cp', key: 'customer']
        sort name: 'acs'
        phones cascade: 'all-delete-orphan', fetch: 'select'
//      phones cascade: 'all-delete-orphan', fetch: 'join' // this work incorrectly with limits in criteria
    }

    static constraints = {
        name maxSize: 128, blank: false, nullable: false
        email maxSize: 32, nullable: true, blank: true, unique: true, email: true
        info maxSize: 4096, nullable: true, blank: true
        phones minSize: 1, validator: {
            Set<CounterpartyPhone> phoneSet, Counterparty cp ->
                if (phoneSet != null && !phoneSet.empty) {
                    def phones = phoneSet.collect { it.phone }
                    def duplicates = []
                    def hasDuplicates
                    if (cp.id != null) {
                        hasDuplicates = CounterpartyPhone.countByPhoneInListAndOwnerNotEqual(phones, cp) > 0
                        if (hasDuplicates)
                            duplicates = CounterpartyPhone.findAllByPhoneInListAndOwnerNotEqual(phones, cp).phone
                    } else {
                        hasDuplicates = CounterpartyPhone.countByPhoneInList(phones) > 0
                        if (hasDuplicates)
                            duplicates = CounterpartyPhone.findAllByPhoneInList(phones).phone
                    }
                    if (hasDuplicates) {
                        return ['duplicate', duplicates]
                    }
                }
                return true
        }
    }

    static hasMany = [comments: Comment, phones: CounterpartyPhone]

    public boolean hasRealties() {
        return Realty.countByOwner(this) > 0
    }

    public boolean hasOrders() {
        return Order.countByCounterparty(this) > 0
    }

    public List<Comment> getLatestComments() {
        final session = sessionFactory.currentSession
        final query = session.createSQLQuery("select c.* from comments AS c inner join comments_cp cp ON cp.comment_id = c.id where cp.customer = ? order by c.created desc limit 3")
        final results = query.with {
            addEntity(Comment)
            setLong(0, id)
            list()
        }
        return results
    }

    public List<Realty> getRealties() {
        return Realty.findAllByOwner(this)
    }

    public List<Order> getOrders() {
        return Order.findAllByCounterparty(this)
    }

    public void resetPhones(List<String> phoneStrs) {
        def oldPhones = new ArrayList<CounterpartyPhone>(phones)
        phones.clear()
        if (phoneStrs != null) {
            phones.addAll(phoneStrs.collect {
                str -> oldPhones.find { p -> p.phone == str } ?: new CounterpartyPhone(phone: str, owner: this)
            })
        }
    }

    public static enum CounterpartyType {
        owner("owner"),
        customer("customer")

        final String name

        CounterpartyType(String name) {
            this.name = name
        }
    }
}
