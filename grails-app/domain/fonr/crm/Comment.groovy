package fonr.crm

class Comment {
    Long id
    Date created = new Date()
    String comment
    User author

    static constraints = {
        comment maxSize: 4096, nullable: true
    }

    static mapping = {
        table 'comments'
        id name: 'id'
        author column: 'author'
    }
}
