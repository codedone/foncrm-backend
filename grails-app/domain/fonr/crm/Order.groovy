package fonr.crm

import com.vividsolutions.jts.geom.GeometryCollection
import grails.rest.Resource
import org.hibernate.spatial.GeometryType

@Resource
class Order {
    Long id
    Date created
    Date updated
    String info
    String regionSuggestions
    String realtySuggestions
    GeometryCollection region
    BigDecimal minPrice
    BigDecimal maxPrice
    BigDecimal minPricePerMeter
    BigDecimal maxPricePerMeter
    BigDecimal minSquare
    BigDecimal maxSquare
    State state
    User assignee
    Counterparty counterparty
    Room.PriceType priceType

    static mapping = {
        table 'orders'
        id name: 'id'
        state enumType: "string"
        assignee column: 'assignee', fetch: 'join'
        counterparty column: 'counterparty', fetch: 'join'
        comments joinTable: [name: 'comments_orders', key: 'order_id']
        region type: GeometryType
        regionSuggestions column: 'region_suggestions'
        realtySuggestions column: 'realty_suggestions'
        purposes joinTable: [name: 'order_purposes', key: 'order_id']
        offerTypes joinTable: [name: 'order_offer_types', key: 'order_id', column: 'offer_type']
        minSquare column: 'min_square'
        maxSquare column: 'max_square'
        minPrice column: 'min_price'
        maxPrice column: 'max_price'
        minPricePerMeter column: 'min_price_per_meter'
        maxPricePerMeter column: 'max_price_per_meter'
        priceType column: 'price_type'
    }

    static belongsTo = [assignee: User]

    static constraints = {
        info maxSize: 4096
        minSquare nullable: true, min: BigDecimal.ZERO
        maxSquare nullable: true, min: BigDecimal.ZERO
        minPrice nullable: true, min: BigDecimal.ZERO
        maxPrice nullable: true, min: BigDecimal.ZERO
        minPricePerMeter nullable: true, min: BigDecimal.ZERO
        maxPricePerMeter nullable: true, min: BigDecimal.ZERO
        priceType nullable: true, validator: {
            Room.PriceType value, Order o ->
                if (o.offerTypes != null && o.offerTypes.id.contains(1L) &&
                        (o.minPrice != null || o.maxPrice != null || o.minPricePerMeter != null || o.maxPricePerMeter != null) && value == null
                ) {
                    //в аренде указан фильтры по цене, но не указан тип цены
                    o.errors.rejectValue("priceType", "order.priceType.nullForRent")
                }
                if (o.offerTypes != null && o.offerTypes.id.contains(2L) && value != null) {
                    o.errors.rejectValue("priceType", "order.priceType.notNullForSale")
                }
                return true
        }
        regionSuggestions maxSize: 4096, nullable: true
        realtySuggestions maxSize: 4096, nullable: true
        region nullable: false
        offerTypes minSize: 1, maxSize: 1, nullable: false, validator: {
            Set<OfferType> ots, Order o ->
                if (ots != null && ots.contains(null)) {
                    o.errors.rejectValue("offerTypes", "order.offerTypes.nullInList")
                }
                return true
        }
        purposes minSize: 1, nullable: false, validator: {
            Set<RealtyPurposeType> rpts, Order o ->
                if (rpts != null && rpts.contains(null)) {
                    o.errors.rejectValue("purposes", "order.purposes.nullInList")
                }
                return true
        }
    }

    def beforeUpdate() {
        updated = new Date()
    }

    static hasMany = [comments: Comment, purposes: RealtyPurposeType, offerTypes: OfferType]

    enum State {
        OPEN("open"),
        CLOSED("closed"),
        REJECTED("rejected")

        String name

        State(String name) {
            this.name = name
        }

        public static State of(String name) {
            def state = values().find {
                it.name == name
            }
            if (state == null)
                throw new IllegalArgumentException("No state with name ${name}".toString())

            return state
        }
    }

    public void resetPurposes(List<Long> purposesIds) {
        def oldPuposes = new ArrayList<RealtyPurposeType>(purposes)
        purposes.clear()
        if (purposesIds != null) {
            purposes.addAll(purposesIds.collect {
                pid -> oldPuposes.find { p -> p.id == pid } ?: RealtyPurposeType.get(pid)
            })
        }
    }

    public void resetOfferTypes(List<Long> newOfferTypes) {
        def oldTypes = new ArrayList(offerTypes)
        offerTypes.clear()
        if (newOfferTypes != null) {
            offerTypes.addAll(
                    newOfferTypes.collect {
                        id -> oldTypes.find { ot -> ot.id == id } ?: OfferType.get(id)
                    } as Collection<? extends OfferType>
            )
        }

    }
}
