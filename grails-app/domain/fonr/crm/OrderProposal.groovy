package fonr.crm

import org.springframework.validation.FieldError

class OrderProposal {
    State state
    Order order
    Realty realty
    Date created
    Date updated
    Comment comment
    Boolean excellent
    Boolean shareable
    Boolean byTotalSquare

    static mapping = {
        table 'order_proposals'
        order column: 'oid'
        realty column: 'rid', fetch: 'join'
        comment column: 'comment_id', fetch: 'join'
        excellent column: 'excellent'
        shareable column: 'shareable'
        byTotalSquare column: 'bysquare'
    }

    static constraints = {
//        suitability nullable: false
        excellent nullable: false
        shareable nullable: false
        byTotalSquare nullable: false
        comment nullable: true, validator: {
            Comment c, OrderProposal op ->
                if (c != null) {
                    c.validate()
                    if (c.hasErrors()) {
                        c.errors.fieldErrors.each {
                            FieldError fe ->
                                op.errors.rejectValue("comment", fe.code, fe.arguments, fe.defaultMessage)

                        }
                    }
                }
                return true
        }
    }

    def beforeUpdate() {
        updated = new Date()
    }

    enum State {
        UNPROCESSED("unprocessed"),
        COMPATIBLE("compatible"),
        INCOMPATIBLE("incompatible"),
        PENDING("pending"),
        PROCESSING("processing")

        String name

        State(String name) {
            this.name = name
        }
    }
    enum Suitability {
        EXCELLENT("excellent"),
        SHAREABLE("shareable"),
        BYTOTALSQUARE("bytotalsquare"),
        BYPLACE("byplace")

        String name
        Suitability(String name){
            this.name = name
        }

        public static Suitability of(String name) {
            def type = values().find {
                it.name == name
            }
            if (type == null)
                throw new IllegalArgumentException("No type of suitability with name ${name}".toString())

            return type
        }
    }
}
