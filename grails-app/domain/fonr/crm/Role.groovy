package fonr.crm

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='authority')
@ToString(includes='authority', includeNames=true, includePackage=false)
class Role implements Serializable {

    static final NAME_OF_ROLE_ADMIN = "ROLE_ADMIN"
    static final NAME_OF_ROLE_OPERATOR = "ROLE_USER"

    private static final long serialVersionUID = 1

    String authority

    Role() {
    }

    Role(String auth) {
        this()
        this.authority = auth
    }

    static constraints = {
        authority blank: false, unique: true, maxSize: 20
    }

    static mapping = {
        authority column: 'authority'
        table 'roles'
        cache true
    }

    public static String roleUserToPrint(String roleString){
        if (roleString == NAME_OF_ROLE_ADMIN){
            return "admin"
        }else if (roleString == NAME_OF_ROLE_OPERATOR){
            return  "user"
        }else{
            return "undefined"
        }
    }

    public static Role getRoleWithName(String str){
        return findByAuthority(stringToRoleUser(str))
    }

    public static String stringToRoleUser(String str){
        if (str == "user"){
            return NAME_OF_ROLE_OPERATOR
        }else if (str == "admin"){
            return NAME_OF_ROLE_ADMIN
        }else{
            return null
        }
    }
}
