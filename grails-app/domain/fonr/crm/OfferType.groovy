package fonr.crm

class OfferType {

    Long id
    String label

    static mapping = {
        table 'offer_types'
        version false
    }
    static constraints = {
        id name: 'id'
        label blank: false, nullable: false, maxSize: 24
    }
}
