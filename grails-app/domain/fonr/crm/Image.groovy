package fonr.crm

class Image {

    transient imageService
    static transients = ['imageService']

    Long id
    String contentType
    String name
    String filePath
    Date dateCreated

    static mapping = {
        version false
        tablePerHierarchy false
        autoTimestamp true
        table 'images'
        id name: 'id'
        name column: 'name'
        filePath column: 'path'
        contentType column: 'content_type'
        dateCreated column: 'created'
    }

    static constraints = {
        name nullable: false, blank: false
        contentType nullable: false, blank: false, validator: {
            String contentType, Image image ->
                if (!contentType.toLowerCase().startsWith("image/")) {
                    image.errors.rejectValue("contentType", "image.contentType.invalid")
                }
                return true
        }
        filePath maxSize: 256, nullable: false, blank: false
    }

    def afterDelete() {
        imageService.deleteImageFiles(this)
    }
}
