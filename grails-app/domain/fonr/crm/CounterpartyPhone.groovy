package fonr.crm

import org.apache.commons.lang.builder.HashCodeBuilder

class CounterpartyPhone implements Serializable {

    String phone
    Counterparty owner

    static mapping = {
        table 'counterparty_phones'
        owner column: 'owner', fetch: 'join'
        id composite: ['phone', 'owner']
        version false
    }

    static constraints = {
        phone maxSize: 32
    }
    static belongsTo = [owner: Counterparty]

    @Override
    boolean equals(Object obj) {

        if (!(obj instanceof CounterpartyPhone) || obj == null)
            return false

        return obj.phone == phone && ((obj.owner & owner) && obj.owner.id == owner.id)
    }

    @Override
    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append phone
        if (owner)
            builder.append owner.id
        builder.toHashCode()
    }
}
