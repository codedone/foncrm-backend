FROM openjdk:latest
WORKDIR /opt
COPY ./build/libs/ .
EXPOSE 8080
CMD [ "sh", "-c", "java $OPTS -Dgrails.env=production -jar fonr-backend-0.9.4.6.war" ]