УСТАНОВКА СИСТЕМЫ

0. Restore database from fonr_production.sql
1. Create user for database
2. Backend:
    2.1. Install oracle java 8
    2.2. Install grails 3.1.14
    2.3. Edit /opt/backend/grails-app/conf/application.yml - настроить параметры подключения к БД в секциях dataSource и environments:production
    2.4. Go to folder /opt/backend и выполнить grails package
    2.5. Run
		export LC_ALL=en_US.UTF-8
		java -Xms1024m -Xmx1024m -XX:MaxPermSize=256m -XX:+UseParallelGC -XX:PermSize=256m -Dgrails.env=prod -jar fonr-backend.war &> /dev/null &
3. frontend:
    3.1. Install nginx или другой
    3.2. Unpack fonr-frontend.tar.gz в какую-либо папку, например в /var/www/fonr
    3.3. Install git, npm 2.15.11, bower 1.7.9, grunt 1.2.0
    3.4. Execute:
	3.4.1. npm install
	3.4.2. bower install
	3.4.3. grunt vulcanize
    3.5. Unpack imageDirectory.tar.gz into folder /var/www/fonr-images (или /var/www/fonr/images)
    3.6. Configure веб-сервер таким образом, чтобы:
	3.6.1. Redirect к /images to /var/www/fonr-images (если imageDirectory.tar.gz распаковывался в /var/www/fonr/images, то этот шаг можно пропустить)
	3.6.2. Redirect к /api to port 8099
	3.6.3. Other requests to к /var/www/fonr