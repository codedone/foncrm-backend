package db.migration

import com.vividsolutions.jts.geom.Point
import com.vividsolutions.jts.io.WKTReader
import fonr.crm.Address
import fonr.crm.Comment
import fonr.crm.Image
import fonr.crm.RealtyImage
import groovy.sql.Sql

import javax.sql.DataSource
import java.sql.Connection
import java.util.Date
import java.sql.PreparedStatement
import java.sql.ResultSet

/**
 * Created by rs on 1/25/17.
 */
public class RealtySplitter {
    public static boolean splitRealties(DataSource dataSource) {
        try {
            Sql sql = new Sql(dataSource);
            if (sql.equals(null)) {
                throw new Exception("Couldn't get sql connection");
            }
            Connection connection = dataSource.connection
            HashMap<Long, RealtyCorrespondingData> correspondingData = getCorrespondingData(sql)
            System.out.println("Got corresponding data")
            createNewRealties(connection, correspondingData)
            System.out.println("Created new realties")
            addOfferTypesForSplitted(connection, correspondingData)
            System.out.println("Added offer types for splitted")
            addPurposesForSplitted(connection, correspondingData)
            System.out.println("Added purposes for splitted")
            addCommentsForSplitted(connection, correspondingData)
            System.out.println("Added comments for splitted")
            addImagesForSplitted(connection, correspondingData)
            System.out.println("Added images for splitted")
            addPhonesForSplitted(connection, correspondingData)
            System.out.println("Added phones for splitted")
            updateProposalsToSplitted(connection, correspondingData)
            System.out.println("Updated proposals for splitted")
            updateRoomsForSplitted(connection, correspondingData)
            System.out.println("Updated rooms for splitted")
            removeOutdatedEntries(sql, correspondingData)
            System.out.println("Removed outdated entries")
        } catch (Exception e) {
            e.printStackTrace()
            return false
        }
        return true
    }

    private static HashMap<Long, RealtyCorrespondingData> getCorrespondingData(Sql sql) {
        HashMap<Long, RealtyCorrespondingData> correspondingData = new HashMap<>()
        sql.eachRow(
                "SELECT x.* from (" +
                        "SELECT realties.id as id,realties.created as created,realties.info as info," +
                        "realties.owner as owner, realties.updated as updated, realties.shareable as shareable," +
                        "realties.minimal_part as minimalPart,realties.version as version," +
                        "count(*) as c from realties INNER JOIN realty_offer_types ON realties.id = realty_offer_types.realty_id " +
                        "GROUP BY realties.id) x WHERE x.c>1;"
        ) { row ->
            RealtyCorrespondingData realtyCorrespondingData = new RealtyCorrespondingData(
                    version: row.version,
                    created: row.created,
                    info: row.info,
                    owner: row.owner,
                    updated: row.updated,
                    shareable: row.shareable,
                    minimalPart: row.minimalPart
            )
            correspondingData.put(row.id, realtyCorrespondingData)

        }
        if (correspondingData.isEmpty()) {
            return correspondingData
        }

        //копируем адреса
        WKTReader wktReader = new WKTReader()
        def joinStringIds = String.join(',', correspondingData.keySet().collect { it.toString() })
        sql.eachRow(
                "SELECT realties.id as rid,country,region,city,street,building,room,ST_AsText(location) as loc from realties INNER JOIN addresses ON realties.address_id = addresses.id where realties.id in (" +
                        "${joinStringIds});"
        ) { row ->
            Long id = row.rid
            RealtyCorrespondingData realtyCorrespondingData = correspondingData.get(id)
            realtyCorrespondingData.address = new RealtyAddress(
                    country: row.country,
                    region: row.region,
                    city: row.city,
                    street: row.street,
                    building: row.building,
                    room: row.room,
                    location: row.loc
            )
        }

        //копируем изображения
        sql.eachRow(
                "SELECT realties.id as rid,content_type,name,images.created,path,is_main " +
                        "from realties " +
                        "INNER JOIN realty_images ON realties.id = realty_images.realty_id " +
                        "INNER JOIN images ON realty_images.id = images.id " +
                        "where realties.id in (" +
                        "${joinStringIds});"
        ) { row ->
            Long id = row.rid
            RealtyCorrespondingData realtyCorrespondingData = correspondingData.get(id)
            realtyCorrespondingData.images.add(
                    new RealtyImage(
                            contentType: row.content_type,
                            name: row.name,
                            dateCreated: row.created,
                            filePath: row.path,
                            isMain: row.is_main
                    )
            )
        }

        //копируем комментарии
        sql.eachRow(
                "SELECT realty_id as rid,version,author,comment,created from comments_realties " +
                        "INNER JOIN comments ON comments_realties.comment_id = comments.id " +
                        "WHERE realty_id IN (${joinStringIds});"
        ) { row ->
            Long id = row.rid
            RealtyCorrespondingData realtyCorrespondingData = correspondingData.get(id)
            realtyCorrespondingData.comments.add(
                    new RealtyComment(
                            version: row.version,
                            author: row.author,
                            comment: row.comment,
                            created: row.created
                    )
            )
        }

        //копируем purposes
        sql.eachRow(
                "SELECT realty_id as rid, realty_purpose_type_id as pid " +
                        "from realty_purposes where realty_id in (${joinStringIds});"
        ) { row ->
            Long id = row.rid
            RealtyCorrespondingData realtyCorrespondingData = correspondingData.get(id)
            realtyCorrespondingData.purposes.add(row.pid)
        }

        //копируем phones
        sql.eachRow(
                "SELECT realty,phone from realty_phones where realty in (${joinStringIds});"
        ) { row ->
            Long id = row.realty
            RealtyCorrespondingData realtyCorrespondingData = correspondingData.get(id)
            realtyCorrespondingData.phones.add(row.phone)
        }

        //копируем orderProposals
        sql.eachRow(
                "SELECT order_proposals.id as opid,rid,order_offer_types.offer_type otid from order_proposals " +
                        "  INNER JOIN orders ON order_proposals.oid = orders.id " +
                        "  INNER JOIN order_offer_types ON orders.id = order_offer_types.order_id " +
                        "where rid in (${joinStringIds});"
        ) { row ->
            Long rid = row.rid
            RealtyCorrespondingData realtyCorrespondingData = correspondingData.get(rid)
            if (row.otid == 1L) {
                realtyCorrespondingData.orderProposalsForRent.add(row.opid)
            } else {
                realtyCorrespondingData.orderProposalsForSale.add(row.opid)
            }
        }
        return correspondingData
    }

    private
    static void createNewRealties(Connection connection, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        // создаем адреса
        PreparedStatement psAddress = connection.prepareStatement(
                "INSERT INTO addresses (country,region,city,street,building,room,location) VALUES(?,?,?,?,?,?,ST_GeomFromText(?));",
                PreparedStatement.RETURN_GENERATED_KEYS
        )
        correspondingData.values().forEach {
            psAddress.setString(1, it.address.country)
            psAddress.setString(2, it.address.region)
            psAddress.setString(3, it.address.city)
            psAddress.setString(4, it.address.street)
            psAddress.setString(5, it.address.building)
            psAddress.setString(6, it.address.room)
            psAddress.setString(7, it.address.location)
            psAddress.addBatch()
            psAddress.addBatch()
        }
        psAddress.executeBatch()
        ResultSet rsAddress = psAddress.getGeneratedKeys()
        ArrayList<Long> addressIds = new ArrayList<Long>(2 * correspondingData.size())
        while (rsAddress.next()) {
            addressIds.add(rsAddress.getLong(1))
        }

        //создаем realties
        Iterator<Long> iterator = addressIds.iterator()
        PreparedStatement psRealties = connection.prepareStatement(
                "INSERT INTO realties (version, address_id, created, info, owner, updated, shareable, minimal_part) " +
                        "VALUES (?,?,?,?,?,?,?,?);", PreparedStatement.RETURN_GENERATED_KEYS
        )
        correspondingData.values().forEach { data ->
            psRealties.setLong(1, data.version)
            psRealties.setLong(2, iterator.next())
            psRealties.setDate(3, new java.sql.Date(data.created.getTime()))
            psRealties.setString(4, data.info)
            psRealties.setLong(5, data.owner)
            psRealties.setDate(6, new java.sql.Date(data.updated.getTime()))
            if (data.shareable == null) {
                psRealties.setNull(7, java.sql.Types.BOOLEAN)
            } else {
                psRealties.setBoolean(7, data.shareable)
            }
            if (data.minimalPart == null) {
                psRealties.setNull(8, java.sql.Types.DECIMAL)
            } else {
                psRealties.setBigDecimal(8, data.minimalPart)
            }
            psRealties.addBatch()
            psRealties.setLong(2, iterator.next())
            psRealties.addBatch()
        }
        psRealties.executeBatch()
        ResultSet rsRealties = psRealties.getGeneratedKeys()
        ArrayList<Long> realtiesIds = new ArrayList<Long>(2 * correspondingData.size())
        while (rsRealties.next()) {
            realtiesIds.add(rsRealties.getLong(1))
        }
        Iterator<Long> iteratorRealties = realtiesIds.iterator()
        correspondingData.values().forEach { data ->
            data.newRentId = iteratorRealties.next()
            data.newSaleId = iteratorRealties.next()
        }

    }

    private
    static void addOfferTypesForSplitted(Connection connection, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO realty_offer_types (realty_id, offer_type) VALUES (?,?);"
        )
        correspondingData.values().forEach { data ->
            ps.setLong(1, data.newRentId)
            ps.setLong(2, 1L)
            ps.addBatch()
            ps.setLong(1, data.newSaleId)
            ps.setLong(2, 2L)
            ps.addBatch()
        }
        ps.executeBatch()
    }

    private
    static void addCommentsForSplitted(Connection connection, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO comments (version, author, `comment`, `created`) VALUES (?,?,?,?);",
                PreparedStatement.RETURN_GENERATED_KEYS
        )
        correspondingData.values().forEach { data ->
            [data.newRentId, data.newSaleId].forEach { id ->
                data.comments.forEach { comment ->
                    ps.setLong(1, comment.version)
                    ps.setLong(2, comment.author)
                    ps.setString(3, comment.comment)
                    ps.setDate(4, new java.sql.Date(comment.created.getTime()))
                    ps.addBatch()
                }
            }
        }
        ps.executeBatch()
        ResultSet rs = ps.getGeneratedKeys()
        ArrayList<Long> commentIds = new ArrayList<>()
        while (rs.next()) {
            commentIds.add(rs.getLong(1))
        }
        Iterator<Long> iterator = commentIds.iterator()
        PreparedStatement psComemnts = connection.prepareStatement(
                "INSERT INTO comments_realties (realty_id, comment_id) VALUES (?,?);"
        )
        correspondingData.values().forEach { data ->
            [data.newRentId, data.newSaleId].forEach { id ->
                data.comments.forEach { comment ->
                    psComemnts.setLong(1, id)
                    psComemnts.setLong(2, iterator.next())
                }
            }
        }
    }

    private
    static void addImagesForSplitted(Connection connection, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO images (content_type, name, path, created) VALUES (?,?,?,?);",
                PreparedStatement.RETURN_GENERATED_KEYS
        )
        correspondingData.values().forEach { data ->
            [data.newRentId, data.newSaleId].forEach { id ->
                data.images.forEach { image ->
                    ps.setString(1, image.contentType)
                    ps.setString(2, image.name)
                    ps.setString(3, image.filePath)
                    ps.setDate(4, new java.sql.Date(image.dateCreated.getTime()))
                    ps.addBatch()
                }
            }
        }
        ps.executeBatch()
        ResultSet rs = ps.getGeneratedKeys()
        ArrayList<Long> imageIds = new ArrayList<>()
        while (rs.next()) {
            imageIds.add(rs.getLong(1))
        }
        Iterator<Long> iterator = imageIds.iterator()
        PreparedStatement psImages = connection.prepareStatement(
                "INSERT INTO realty_images (realty_id, is_main, id) VALUES (?,?,?);"
        )
        correspondingData.values().forEach { data ->
            [data.newRentId, data.newSaleId].forEach { id ->
                psImages.setLong(1, id)
                data.images.forEach { image ->
                    psImages.setBoolean(2, image.isMain)
                    psImages.setLong(3, iterator.next())
                    psImages.addBatch()
                }
            }
        }
        psImages.executeBatch()
    }

    private
    static void addPhonesForSplitted(Connection connection, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO realty_phones (realty, phone) VALUES (?,?);"
        )
        correspondingData.values().forEach { data ->
            [data.newRentId, data.newSaleId].forEach { id ->
                ps.setLong(1, id)
                data.phones.forEach { phone ->
                    ps.setString(2, phone)
                    ps.addBatch()
                }
            }
        }
        ps.executeBatch()
    }

    private
    static void addPurposesForSplitted(Connection connection, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO realty_purposes (realty_id, realty_purpose_type_id) VALUES (?,?);"
        )
        correspondingData.values().forEach { data ->
            data.purposes.forEach { purpose ->
                ps.setLong(1, data.newRentId)
                ps.setLong(2, purpose)
                ps.addBatch()
                ps.setLong(1, data.newSaleId)
                ps.setLong(2, purpose)
                ps.addBatch()
            }
        }
        ps.executeBatch()
    }

    private
    static void updateProposalsToSplitted(Connection connection, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        PreparedStatement ps = connection.prepareStatement(
                "UPDATE order_proposals SET rid = ? WHERE id = ? ;"
        )
        correspondingData.values().forEach { data ->
            ps.setLong(1, data.newRentId)
            data.orderProposalsForRent.forEach { op ->
                ps.setLong(2, op)
                ps.addBatch()
            }
            ps.setLong(1, data.newSaleId)
            data.orderProposalsForSale.forEach { op ->
                ps.setLong(2, op)
                ps.addBatch()
            }
        }
        ps.executeBatch()
    }

    private
    static void updateRoomsForSplitted(Connection connection, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        PreparedStatement ps = connection.prepareStatement(
                "UPDATE rooms SET realty = IF(price_type IS NULL, ?, ?) WHERE realty = ? ;"
        )
        correspondingData.entrySet().forEach { entry ->
            ps.setLong(1, entry.value.newSaleId)
            ps.setLong(2, entry.value.newRentId)
            ps.setLong(3, entry.key)
            ps.addBatch()
        }
        ps.executeBatch()
    }

    private static void removeOutdatedEntries(Sql sql, HashMap<Long, RealtyCorrespondingData> correspondingData) {
        if (correspondingData.isEmpty()) {
            return
        }
        String joinStringIds = "(" + String.join(",", correspondingData.keySet().collect { it.toString() }) + ")"
        sql.execute("DELETE from realty_phones where realty in " + joinStringIds + ";")
        sql.execute("DELETE from realty_offer_types where realty_id in " + joinStringIds + ";")
        sql.execute("DELETE from realty_purposes where realty_id in " + joinStringIds + ";")
        ArrayList<Long> addressIds = new ArrayList<>(correspondingData.size())
        sql.eachRow(
                "SELECT address_id from realties where id in " + joinStringIds + ";"
        ) { row ->
            addressIds.add(row.address_id)
        }
        ArrayList<Long> imageIds = new ArrayList<>()
        sql.eachRow(
                "SELECT id from realty_images where realty_id in " + joinStringIds + ";"
        ) { row ->
            imageIds.add(row.id)
        }
        if (!imageIds.isEmpty()) {
            String imageIdsString = String.join(",", imageIds.collect { it.toString() })
            sql.execute(
                    "DELETE from realty_images where id in (" + imageIdsString + ");"
            )
            sql.execute(
                    "DELETE from images where id in (" + imageIdsString + ");"
            )
        }
        ArrayList<Long> commentIds = new ArrayList<>()
        sql.eachRow(
                "SELECT comment_id from comments_realties where realty_id in " + joinStringIds + ";"
        ) { row ->
            commentIds.add(row.comment_id)
        }
        if (!commentIds.isEmpty()) {
            String commentIdsString = String.join(",", commentIds.collect { it.toString() })
            sql.execute(
                    "DELETE from comments_realties where comment_id in (" + commentIdsString + ");"
            )
            sql.execute(
                    "DELETE from comments where id in (" + commentIdsString + ");"
            )
        }
        sql.execute(
                "DELETE from realties where id in " + joinStringIds + ";"
        )
        sql.execute(
                "DELETE from addresses where id in (" + String.join(",", addressIds.collect { it.toString() }) + ");"
        )
    }

    private static class RealtyCorrespondingData {
        Long newRentId
        Long newSaleId
        RealtyAddress address
        Long version
        Date created
        String info
        Long owner
        Date updated
        Boolean shareable
        BigDecimal minimalPart
        ArrayList<RealtyImage> images = new ArrayList<>()
        ArrayList<RealtyComment> comments = new ArrayList<>()
        ArrayList<String> phones = new ArrayList<>()
        ArrayList<Long> purposes = new ArrayList<>()
        ArrayList<Long> orderProposalsForRent = new ArrayList<>()
        ArrayList<Long> orderProposalsForSale = new ArrayList<>()
    }

    private static class RealtyComment {
        Long id
        Long version
        Long author
        String comment
        Date created
    }

    private static class RealtyAddress {
        String country
        String region
        String city
        String street
        String building
        String room
        String location
    }
}
