package db.migration;

import groovy.sql.Sql;
import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by rs on 6/27/16.
 */
public class V0_9_2_3__remove_duplicate_realties implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        Sql sql = new Sql(jdbcTemplate.getDataSource());
        if(sql.equals(null)){
            throw new Exception("Couldn't get sql connection");
        }
        if(!Normalizer.prettifyRealties(sql)){
            throw new Exception("Couldn't remove duplicate realties");
        }
    }
}
