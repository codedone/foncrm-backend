package db.migration

import groovy.sql.Sql

/**
 * Created by rs on 6/27/16.
 */
public class Normalizer {
    public static boolean prettifyCounterparties(Sql sql) {
        System.out.println("Im in v0.9.2.2");
        processCounterpartiesToDelete(sql)
        if (!deleteIncorrectCounterparties(sql)) {
            System.out.println("Error in deleteIncorrectCounterparties")
            return false
        }
        def phoneToCounterparty = findCorrespondingOwnerForPhones(sql)
        HashMap<Long, Long> oldCpToNewCp = findOriginalCounterpartyForDuplicate(sql, phoneToCounterparty)
        HashSet<String> allCounterpartyPhones = phonesOfAllCounterparties(sql)
        changeCreatedTimeForCounterparties(sql, oldCpToNewCp)
        if (!mergeCounterpartyRealties(sql, oldCpToNewCp)) {
            return false
        }
        if (!mergeCounterpartyOrders(sql, oldCpToNewCp)) {
            return false
        }
        if (!mergeCounterpartyComments(sql, oldCpToNewCp)) {
            return false
        }
        if (!mergeCounterpartyPhones(sql, phoneToCounterparty, allCounterpartyPhones)) {
            return false
        }
        removeUselessCounterparties(sql)
        return true
    }

    private static HashSet<String> phonesOfAllCounterparties(Sql sql) {
        HashSet<String> allPhones = new HashSet<>()
        sql.eachRow(
                "SELECT phone from counterparty_phones;"
        ) { row ->
            allPhones.add(row.phone)
        }
        return allPhones
    }

    private static HashMap<Long, Long> findOriginalCounterpartyForDuplicate(Sql sql, phoneToCounterparty) {
        HashMap<Long, Long> oldCpToNewCp = new HashMap<>()
        sql.eachRow(
                "SELECT owner,phone from counterparty_phones;"
        ) { row ->
            oldCpToNewCp.put(row.owner, phoneToCounterparty.get(row.phone))
        }
        sql.eachRow(
                "SELECT owner,phone from realty_phones INNER JOIN realties ON realty_phones.realty = realties.id;"
        ) { row ->
            oldCpToNewCp.put(row.owner, phoneToCounterparty.get(row.phone))
        }
        return oldCpToNewCp
    }

    public static boolean prettifyRealties(Sql sql) {
        System.out.println("Im in v0.9.2.3");
        HashMap<Long, Long> realtyIdToOriginalRealtyId = findCorrespondingRealties(sql)
        mergeCommentsOfDuplicateRealties(sql, realtyIdToOriginalRealtyId)
        changeCreatedTimeForRealties(sql, realtyIdToOriginalRealtyId)
        changeLastUpdateTimeForRealties(sql, realtyIdToOriginalRealtyId)
        if (!changeLastUpdateTimeForMergedRealtiesIfThereIsNewerComment(sql)) {
            return false
        }
        if (!mergeOfferTypesPurposesAndPhonesOfDuplicateRealties(sql, realtyIdToOriginalRealtyId)) {
            return false
        }
        if (!mergeInfoOfDuplicateRealties(sql, realtyIdToOriginalRealtyId)) {
            return false
        }
        mergeRealtiesImages(sql, realtyIdToOriginalRealtyId)
        mergeDuplicateOrderProposals(sql, realtyIdToOriginalRealtyId)
        removeDuplicateRealtiesAndAllDependentObjects(sql, realtyIdToOriginalRealtyId)
        return true
    }

    public static boolean prettifyPhones(Sql sql) {
        changePhonesToInternationalFormat(sql)
        return true
    }

    private static boolean deleteIncorrectCounterparties(Sql sql) {
        try {
            sql.execute("DELETE from realty_phones WHERE " +
                    "LOWER(phone) REGEXP 'нет номера' OR " +
                    "LOWER(phone) REGEXP 'не удалось получить номер' OR " +
                    "LOWER(phone) REGEXP 'ошибка в номере' OR " +
                    "NOT LOWER(phone) REGEXP '[0-9]';"
            )
            sql.execute("DELETE from counterparty_phones WHERE " +
                    "LOWER(phone) REGEXP 'нет номера' OR " +
                    "LOWER(phone) REGEXP 'не удалось получить номер' OR " +
                    "LOWER(phone) REGEXP 'ошибка в номере' OR " +
                    "NOT LOWER(phone) REGEXP '[0-9]' OR " +
                    "owner in (SELECT id from counterparties where LOWER(name) REGEXP 'удалить');"
            )
            def counterparties = new ArrayList<Long>()
            sql.eachRow(
                    "SELECT id from counterparties where id not in (SELECT owner from counterparty_phones);"
            ) { row ->
                counterparties.add(row.id)
            }
            String sqlDelete

            if(!counterparties.isEmpty()){
                def counterpartiesIdList = "(" + String.join(",", counterparties.collect {
                    return it.toString()
                }) + ")"

                // удалить все объекты связыанные с counterparty которые должны быть удалены
                // удалить комментарии на cp
                sqlDelete = "DELETE from comments_cp WHERE " +
                        "customer in " + counterpartiesIdList + ";"
                sql.execute(sqlDelete)

                // удалить комментарии на realties принадлежащие cp
                sqlDelete = "DELETE from comments_realties WHERE " +
                        "realty_id in (SELECT id from realties WHERE owner in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить комментарии на orders принадлежащие cp
                sqlDelete = "DELETE from comments_orders WHERE " +
                        "order_id in (SELECT id from orders WHERE counterparty in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить orderProposals, где либо объект принадлежит cp, либо order принадлежит cp
                sqlDelete = "DELETE from order_proposals WHERE " +
                        "rid in (SELECT id from realties WHERE owner in " + counterpartiesIdList + ") OR " +
                        "oid in (SELECT id from orders WHERE counterparty in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить orderPurposes
                sqlDelete = "DELETE from order_purposes WHERE " +
                        "order_id in (SELECT id from orders WHERE counterparty in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить orderOfferTypes
                sqlDelete = "DELETE from order_offer_types WHERE " +
                        "order_id in (SELECT id from orders WHERE counterparty in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить realtyPurposes
                sqlDelete = "DELETE from realty_purposes WHERE " +
                        "realty_id in (SELECT id from realties WHERE owner in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить realtyOfferTypes
                sqlDelete = "DELETE from realty_offer_types WHERE " +
                        "realty_id in (SELECT id from realties WHERE owner in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить orders
                sqlDelete = "DELETE from orders where " +
                        "counterparty in " + counterpartiesIdList + ";"
                sql.execute(sqlDelete)

                // удалить realtyPhones
                sqlDelete = "DELETE from realty_phones where " +
                        "realty in (SELECT id from realties WHERE owner in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить realtyImages
                sqlDelete = "DELETE from realty_images where " +
                        "realty_id in (SELECT id from realties WHERE owner in " + counterpartiesIdList + ");"
                sql.execute(sqlDelete)

                // удалить realty
                sqlDelete = "DELETE from realties WHERE " +
                        "owner in " + counterpartiesIdList + ";"
                sql.execute(sqlDelete)

                // удалить cp
                sqlDelete = "DELETE from counterparties WHERE " +
                        "id in " + counterpartiesIdList + ";"
                sql.execute(sqlDelete)
            }

            // почистить неиспользуемые адреса
            sqlDelete = "DELETE from addresses WHERE " +
                    "id not in (SELECT address_id from realties);"
            sql.execute(sqlDelete)

            // в самом конце потереть неиспользуемые комментарии
            sqlDelete = "DELETE from comments WHERE " +
                    "id not in (SELECT comment_id from comments_cp) and " +
                    "id not in (SELECT comment_id from comments_orders) and " +
                    "id not in (SELECT comment_id from comments_realties) and " +
                    "id not in (SELECT comment_id from order_proposals);"
            sql.execute(sqlDelete)
        } catch (Exception e) {
            System.out.println("Error in delete counterparties")
            e.printStackTrace()
            return false
        }
        return true
    }

    private static boolean changePhonesToInternationalFormat(Sql sql) {
        HashMap<Long, HashSet<String>> phonesHashMap = new HashMap<>()
        HashSet<String> debugPhones = new HashSet<>()
        sql.eachRow(
                "SELECT realty, phone from realty_phones " +
                        "WHERE not LOWER(phone) REGEXP 'нет номера' AND " +
                        "NOT LOWER(phone) REGEXP 'не удалось получить номер' AND " +
                        "NOT LOWER(phone) REGEXP 'ошибка в номере' AND " +
                        "LOWER(phone) REGEXP '[0-9]';"
        ) { row ->
            def realtyId = row.realty
            def phone = row.phone
            if (!phonesHashMap.containsKey(realtyId)) {
                phonesHashMap.put(realtyId, new HashSet<String>())
            }
            if (phone.substring(0, 1) != "+" && phone.matches("^[0-9]{11,}.*")) {
                String newPhone = phone.replaceAll("^[789]", "+7")
//                        log.debug("Found phone: {}, {}", phone.phone, newPhone)
                phonesHashMap.get(realtyId).add(newPhone)
            } else if (phone.matches("\\+7[0-9]{10}.*")) {
                phonesHashMap.get(realtyId).add(phone)
            } else if (phone.matches("[49][0-9]{9}.*")) {
                String newPhone = "+7" + phone
                phonesHashMap.get(realtyId).add(newPhone)
            } else if (phone.matches("[0-9]{7}.*")) {
                String newPhone = "+7495" + phone //todo: maybe +7499
                phonesHashMap.get(realtyId).add(newPhone)
            } else {
                String newPhone = phone.replaceAll("[^0-9]*", "")
//                    println(newPhone)
                if (newPhone.matches("[789][0-9]{10}")) {
                    phonesHashMap.get(realtyId).add(newPhone.replaceAll("^[789]", "+7"))
                } else if (newPhone.matches("[49][0-9]{9}")) {
                    phonesHashMap.get(realtyId).add("+7" + newPhone)
                } else if (newPhone.matches("[0-9]{7}")) {
                    phonesHashMap.get(realtyId).add("+7495" + newPhone)
                } else {
                    debugPhones.add(phone)
                }
            }
        }
        int maxLength = 0;
        String maxPhone = ""
        phonesHashMap.values().forEach {
            String tempMax = it.max {
                it.length()
            }
            if (tempMax.length() > maxLength) {
                maxPhone = tempMax
                maxLength = tempMax.length()
            }
        }
        System.out.println("Max length - ${maxLength}")
        System.out.println("Phone with max length - ${maxPhone}")

        sql.execute("DELETE from realty_phones;")
        sql.withBatch("INSERT INTO realty_phones (realty,phone) VALUES (?,?);") { psInsert ->
            phonesHashMap.entrySet().forEach { entry ->
                entry.value.forEach { phone ->
                    psInsert.addBatch([entry.key, phone])
                }
            }
        }

        phonesHashMap.clear()
        sql.eachRow(
                "SELECT owner, phone from counterparty_phones " +
                        "WHERE not LOWER(phone) REGEXP 'нет номера' AND " +
                        "NOT LOWER(phone) REGEXP 'не удалось получить номер' AND " +
                        "NOT LOWER(phone) REGEXP 'ошибка в номере' AND " +
                        "LOWER(phone) REGEXP '[0-9]';"
        ) { row ->
            def counterpartyId = row.owner
            def phone = row.phone
            if (!phonesHashMap.containsKey(counterpartyId)) {
                phonesHashMap.put(counterpartyId, new HashSet<String>())
            }
            if (phone.substring(0, 1) != "+" && phone.matches("^[0-9]{11,}.*")) {
                String newPhone = phone.replaceAll("^[789]", "+7")
//                        log.debug("Found phone: {}, {}", phone.phone, newPhone)
                phonesHashMap.get(counterpartyId).add(newPhone)
            } else if (phone.matches("\\+7[0-9]{10}.*")) {
                phonesHashMap.get(counterpartyId).add(phone)
            } else if (phone.matches("[49][0-9]{9}.*")) {
                String newPhone = "+7" + phone
                phonesHashMap.get(counterpartyId).add(newPhone)
            } else if (phone.matches("[0-9]{7}.*")) {
                String newPhone = "+7495" + phone //todo: maybe +7499
                phonesHashMap.get(counterpartyId).add(newPhone)
            } else {
                String newPhone = phone.replaceAll("[^0-9]*", "")
//                    println(newPhone)
                if (newPhone.matches("[789][0-9]{10}")) {
                    phonesHashMap.get(counterpartyId).add(newPhone.replaceAll("^[789]", "+7"))
                } else if (newPhone.matches("[49][0-9]{9}")) {
                    phonesHashMap.get(counterpartyId).add("+7" + newPhone)
                } else if (newPhone.matches("[0-9]{7}")) {
                    phonesHashMap.get(counterpartyId).add("+7495" + newPhone)
                } else {
                    debugPhones.add(phone)
                }
            }
        }
        sql.execute("DELETE from counterparty_phones;")
        sql.withBatch("INSERT INTO counterparty_phones (owner,phone) VALUES (?,?);") { psInsert ->
            phonesHashMap.entrySet().forEach { entry ->
                entry.value.forEach { phone ->
                    psInsert.addBatch([entry.key, phone])
                }
            }
        }
        return true
    }

    private static HashMap<String, Long> findCorrespondingOwnerForPhones(Sql sql) {
        HashMap<String, Long> phoneToCounterparty = new HashMap<>()
        HashMap<Long, List<String>> realtyPhones = new HashMap<>()
        HashMap<Long, List<String>> counterpartyPhones = new HashMap<>()

        sql.eachRow(
                "SELECT phone,owner FROM realty_phones INNER JOIN realties ON realty_phones.realty = realties.id;"
        ){ row->
            Long owner = row.owner
            String phone = row.phone
            if(!realtyPhones.containsKey(owner)){
                realtyPhones.put(owner,new ArrayList<String>())
            }
            realtyPhones.get(owner).add(phone)
        }

        sql.eachRow(
                "SELECT phone, owner from counterparty_phones;"
        ){ row->
            Long owner = row.owner
            String phone = row.phone
            if(!counterpartyPhones.containsKey(owner)){
                counterpartyPhones.put(owner,new ArrayList<String>())
            }
            counterpartyPhones.get(owner).add(phone)
        }

        realtyPhones.entrySet().forEach {
            HashSet<Long> duplicates = new HashSet<>()
            for (String realtyPhone : it.value) {
                if (phoneToCounterparty.containsKey(realtyPhone)) {
                    duplicates.add(phoneToCounterparty.get(realtyPhone))
                }
            }
            if (duplicates.isEmpty()) {
                //create new
                for (String realtyPhone : it.value) {
                    phoneToCounterparty.put(realtyPhone, it.key)
                }
            } else {
                Long newOwnerId = it.key
                // todo:  игнорировать с именем "Удалить"
                for (Long counterPartyId : duplicates) {
                    phoneToCounterparty.entrySet().findAll { entry ->
                        entry.value == counterPartyId
                    }.forEach { entry ->
                        phoneToCounterparty.put(entry.key, newOwnerId)
                    }
                }
                for (String realtyPhone : it.value) {
                    phoneToCounterparty.put(realtyPhone, newOwnerId)
                }
            }
        }

        counterpartyPhones.entrySet().forEach {
            Long counterparty = it.key
            HashSet<Long> duplicates = new HashSet<>()
            for (String counterpartyPhone : it.value) {
                if (phoneToCounterparty.containsKey(counterpartyPhone)) {
                    duplicates.add(phoneToCounterparty.get(counterpartyPhone))
                }
            }
            if (duplicates.isEmpty()) {
                //create new
                for (String counterpartyPhone : it.value) {
                    phoneToCounterparty.put(counterpartyPhone, counterparty)
                }
            } else {
                Long newOwnerId = counterparty
                for (Long counterPartyId : duplicates) {
                    phoneToCounterparty.entrySet().findAll { entry ->
                        entry.value == counterPartyId
                    }.forEach { entry ->
                        phoneToCounterparty.put(entry.key, newOwnerId)
                    }
                }
                for (String realtyPhone : it.value) {
                    phoneToCounterparty.put(realtyPhone, newOwnerId)
                }
            }
        }
        return phoneToCounterparty
    }

    private static boolean mergeCounterpartyRealties(Sql sql, HashMap<Long, Long> oldCpToNewCp) {
        try{
            sql.withBatch (
                    "UPDATE realties SET owner = ? " +
                            "where owner = ?;"
            ){ ps->
                oldCpToNewCp.entrySet().forEach {
                    ps.addBatch([it.value,it.key])
                }
            }
        }catch (Exception e){
            e.printStackTrace()
            return false
        }
//
//        def realties = Realty.listOrderById()
//        sql.withBatch(
//                "UPDATE realties SET owner = ? WHERE id = ? ;"
//        ) { ps ->
//            realties.forEach {
//                try {
//                    def phones = it.phones.phone
//                    phones.retainAll(phoneToCounterparty.keySet())
//                    if (phones.isEmpty()) {
//                        phones = it.owner.phones.phone
//                        phones.retainAll(phoneToCounterparty.keySet())
//                    }
//                    if (!phones.isEmpty()) {
//                        def ownerId = phoneToCounterparty.get(
//                                phones.sort().first()
//                        )
//                        ps.addBatch([ownerId, it.id])
//                    } else {
//                        log.error("phones is empty")
//                        log.error("Error merge: realty id - {}", it.id)
//                        log.error("Realty phones - {}", it.phones.phone)
//                        log.error("Owner phones - {}", it.owner.phones.phone)
//                    }
//                } catch (Exception e) {
//                    log.error("Error merge: realty id - {}", it.id)
//                    log.error("Realty phones - {}", it.phones.phone)
//                    log.error("Owner phones - {}", it.owner.phones.phone)
//                    return false
//                }
//            }
//        }
        return true
    }

    private static boolean mergeCounterpartyOrders(Sql sql, HashMap<Long, Long> oldCpToNewCp) {
        try{
            sql.withBatch (
                    "UPDATE orders SET counterparty = ? " +
                            "where counterparty = ?;"
            ){ ps->
                oldCpToNewCp.entrySet().forEach {
                    ps.addBatch([it.value,it.key])
                }
            }
        }catch (Exception e){
            e.printStackTrace()
            return false
        }


//        def orders = Order.listOrderById()
//        sql.withBatch(
//                "UPDATE orders SET counterparty = ? WHERE id = ? ;"
//        ) { ps ->
//            orders.forEach {
//                try {
//                    def phones = it.counterparty.phones.phone
//                    phones.retainAll(phoneToCounterparty.keySet())
//                    if (!phones.isEmpty()) {
//                        def ownerId = phoneToCounterparty.get(
//                                phones.sort().first()
//                        )
//                        if (ownerId != null) {
//                            ps.addBatch([ownerId, it.id])
//                        }
//                    } else {
//                        log.error("Error merge counterparty orders: order id - {}", it.id)
//                        log.error("Counterparty phones - {}", it.counterparty.phones.phone.sort())
//                    }
//                } catch (Exception e) {
//                    log.error("Error merge counterparty orders: order id - {}", it.id)
//                    log.error("Counterparty before - {}", it.counterparty.id)
//                    log.error("Counterparty phones - {}", it.counterparty.phones.phone.sort())
//                    return false
//                }
//            }
//        }
        return true
    }

    private static boolean mergeCounterpartyComments(Sql sql, HashMap<Long, Long> oldCpToNewCp) {
        try{
            sql.withBatch (
                    "UPDATE comments_cp SET customer = ? " +
                            "where customer = ? ;"
            ){ ps->
                oldCpToNewCp.entrySet().forEach {
                    ps.addBatch([it.value,it.key])
                }
            }
        }catch (Exception e){
            e.printStackTrace()
            return false
        }

//        def counterparties = Counterparty.listOrderById()
//        sql.withBatch(
//                "UPDATE comments_cp SET customer = ? WHERE customer = ? ;"
//        ) { ps ->
//            counterparties.forEach {
//                try {
//                    def newCustomerId = phoneToCounterparty.get(
//                            it.phones.phone.sort().first()
//                    )
//                    if (newCustomerId != null) {
//                        ps.addBatch([newCustomerId, it.id])
//                    }
//                } catch (Exception e) {
//                    log.error("Error merge counterparty comments: counterparty id - {}", it.id)
//                    log.error("Phones - {}", it.phones.sort())
//                    return false
//                }
//            }
//        }
        return true
    }

    private
    static boolean mergeCounterpartyPhones(Sql sql, HashMap<String, Long> phoneToCounterparty, HashSet<String> allCounterpartyPhones) {
        sql.execute("DELETE from counterparty_phones;")
        sql.withBatch(
                "INSERT INTO counterparty_phones (phone, owner) VALUES (?, ?);"
        ) { ps ->
            phoneToCounterparty.entrySet().forEach {
                if (allCounterpartyPhones.contains(it.key)) {
                    try {
                        ps.addBatch([it.key, it.value])
                    } catch (Exception e) {
                        return false
                    }
                }
            }
        }
        return true
    }

    private static HashMap<Long, Long> findCorrespondingRealties(Sql sql) {
        Map<Long, List<Long>> ownerToCounterparties = new HashMap<>()
        def fakeToOriginal = new HashMap<Long, Long>()
        HashMap<Long,InnerAddress> realtyAddresses = new HashMap<>()

        sql.eachRow(
                "SELECT realties.id as rid, owner,country,region,city,street,building,room " +
                        "from realties INNER JOIN addresses ON realties.address_id = addresses.id;"
        ){ row->
            InnerAddress address = new InnerAddress()
            address.country = row.country
            address.region = row.region
            address.city = row.city
            address.street = row.street
            address.building = row.building
            address.room = row.room
            Long owner = row.owner
            Long realtyId = row.rid
//            System.out.println(address as JSON)
            if(ownerToCounterparties.containsKey(owner)){
                def original = ownerToCounterparties.get(owner).find {
                    isAddressesEqual(address,realtyAddresses.get(it))
                }
                if (original != null){
                    // Найден дубликат
                    fakeToOriginal.put(realtyId,original)
                }else{
                    fakeToOriginal.put(realtyId,realtyId)
                    ownerToCounterparties.get(owner).add(realtyId)
                    realtyAddresses.put(realtyId,address)
                }
            }else{
                ownerToCounterparties.put(owner,new ArrayList<Long>())
                ownerToCounterparties.get(owner).add(realtyId)
                fakeToOriginal.put(realtyId,realtyId)
                realtyAddresses.put(realtyId,address)
            }
        }
//        for (Counterparty counterparty : ownerToCounterparties.keySet()) {
//            def customerRealties = new HashSet<Realty>()
//            for (Realty realty : ownerToCounterparties.get(counterparty)) {
//                def original = customerRealties.find {
//                    return isAddressesEqual(it.address, realty.address)
//                }
//                if (original == null) {
//                    customerRealties.add(realty)
//                    result.put(realty.id, realty.id)
//                } else {
//                    result.put(realty.id, original.id)
//                }
//            }
//        }
        return fakeToOriginal
    }

    private static void mergeCommentsOfDuplicateRealties(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        // быстрый
        sql.withBatch(
                "UPDATE comments_realties SET realty_id = ? WHERE realty_id = ? ;"
        ) { ps ->
            fakeToOriginal.entrySet().forEach {
                ps.addBatch([it.value, it.key])
            }
        }
    }

    private static boolean changeLastUpdateTimeForMergedRealtiesIfThereIsNewerComment(Sql sql) {
        // быстрый
        try {
            sql.withBatch(
                    "UPDATE realties SET updated = ? WHERE id = ?"
            ) { batch ->
                sql.rows(
                        "SELECT realty_id, max(comments.created) AS last_update " +
                                "FROM comments_realties INNER JOIN realties ON realty_id = comments_realties.realty_id " +
                                "INNER JOIN comments ON comments_realties.comment_id = comments.id " +
                                "WHERE realties.updated < comments.created GROUP BY realty_id;"
                ).forEach {
                    batch.addBatch([it.last_update, Long.valueOf(it.realty_id)])
                }
            }
        } catch (Exception e) {
            e.printStackTrace()
            return false
        }
        return true
    }

    private
    static boolean mergeOfferTypesPurposesAndPhonesOfDuplicateRealties(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        HashMap<Long, HashSet<Long>> originalToOfferTypes = new HashMap<>()
        HashMap<Long, HashSet<Long>> originalToPurposeTypes = new HashMap<>()
        HashMap<Long, HashSet<String>> originalToPhones = new HashMap<>()

        try {
            sql.rows(
                    "SELECT realty_id, offer_type from realty_offer_types;"
            ).forEach { row ->
                def original = fakeToOriginal.get(Long.valueOf(row.realty_id))
                if (!originalToOfferTypes.containsKey(original)) {
                    originalToOfferTypes.put(original, new HashSet<Long>())
                }
                originalToOfferTypes.get(original).add(Long.valueOf(row.offer_type))
            }

            sql.rows(
                    "SELECT realty_id, realty_purpose_type_id from realty_purposes;"
            ).forEach { row ->
                def original = fakeToOriginal.get(Long.valueOf(row.realty_id))
                if (!originalToPurposeTypes.containsKey(original)) {
                    originalToPurposeTypes.put(original, new HashSet<Long>())
                }
                originalToPurposeTypes.get(original).add(Long.valueOf(row.realty_purpose_type_id))
            }

            sql.rows(
                    "SELECT realty, phone from realty_phones;"
            ).forEach { row ->
                def original = fakeToOriginal.get(Long.valueOf(row.realty))
                if (!originalToPhones.containsKey(original)) {
                    originalToPhones.put(original, new HashSet<Long>())
                }
                originalToPhones.get(original).add(row.phone)
            }

            sql.execute("DELETE FROM realty_offer_types;")
            sql.execute("DELETE FROM realty_phones;")
            sql.execute("DELETE FROM realty_purposes;")

            sql.withBatch(
                    "INSERT INTO realty_offer_types (realty_id, offer_type) VALUES (? , ?);"
            ) { ps ->
                originalToOfferTypes.entrySet().forEach { original ->
                    original.value.forEach { offerType ->
                        ps.addBatch([original.key, offerType])
                    }
                }
            }

            sql.withBatch(
                    "INSERT INTO realty_purposes (realty_id, realty_purpose_type_id) VALUES (? , ?);"
            ) { ps ->
                originalToPurposeTypes.entrySet().forEach { original ->
                    original.value.forEach { purposeType ->
                        ps.addBatch([original.key, purposeType])
                    }
                }
            }

            sql.withBatch(
                    "INSERT INTO realty_phones (realty, phone) VALUES (? , ?);"
            ) { ps ->
                originalToPhones.entrySet().forEach { original ->
                    original.value.forEach { phone ->
                        ps.addBatch([original.key, phone])
                    }
                }
            }
        } catch (Exception e) {
            return false
        }
        return true
    }

    private static boolean mergeInfoOfDuplicateRealties(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        HashMap<Long,String> realtyInfo = new HashMap<>()
        sql.eachRow(
                "SELECT id, info from realties WHERE info IS NOT NULL ORDER BY updated ASC;"
        ){ row->
            Long realtyId = fakeToOriginal.get(row.id)
            String info = row.info
            if(!info.isEmpty()){
                if(realtyInfo.containsKey(realtyId)){
                    realtyInfo.put(realtyId, info + "\n" + realtyInfo.get(realtyId))
                }else{
                    realtyInfo.put(realtyId,info)
                }
            }
        }
        sql.withBatch (
                "UPDATE realties SET info = ? WHERE id = ?;"
        ) { ps->
            realtyInfo.entrySet().forEach {
                ps.addBatch([it.value, it.key])
            }
        }

//        try {
//            sql.withBatch(
//                    "UPDATE realties SET info = ? WHERE id = ? ;"
//            ) { ps ->
//                def duplicates = Realty.listOrderById().groupBy {
//                    fakeToOriginal.get(it.id)
//                }
//                duplicates.entrySet().forEach {
//                    def realties = it.value
//                            .findAll { r ->
////                            log.debug("realty - {}", r)
////                            log.debug("realty info - {}", r.info)
//                        return r.info != null && !(r.info.isEmpty())
//                    }
//                    .sort { a, b ->
//                        return a.updated <=> b.updated
//                    }
//
//                    if (!realties.isEmpty()) {
//                        def info = String.join("\n", realties.info)
//                        ps.addBatch([info, it.key])
//                    }
//                }
//            }
//        } catch (Exception e) {
//            return false
//        }
        return true
    }

    private static void mergeDuplicateOrderProposals(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        HashMap<Long,HashMap<Long,ArrayList<Long>>> orderProposals = new HashMap<>() // order,original_id -> op_id
        HashMap<Long,InnerOrderProposal> proposals = new HashMap<>()
        sql.eachRow(
                "SELECT order_proposals.id as op_id,oid,rid,comment_id,comment,updated,state from order_proposals LEFT JOIN comments ON order_proposals.comment_id = comments.id;"
        ) { row->
            Long order = row.oid
            Long realty = fakeToOriginal.get(row.rid)
            InnerOrderProposal orderProposal = new InnerOrderProposal()
            orderProposal.id = row.op_id
            orderProposal.comment = row.comment
            orderProposal.commentId = row.comment_id
            orderProposal.updated = row.updated
            orderProposal.state  = row.state
            if(!orderProposals.containsKey(order)){
                orderProposals.put(order,new HashMap<Long, ArrayList<Long>>())
            }
            def opsForOrder = orderProposals.get(order)
            if(!opsForOrder.containsKey(realty)){
                opsForOrder.put(realty,new ArrayList<Long>())
            }
            def opsForRealty = opsForOrder.get(realty)
            opsForRealty.add(orderProposal.id)
            proposals.put(orderProposal.id,orderProposal)
        }

        ArrayList<Long> orderProposalsWhichShouldNotBeDeleted = new ArrayList<>()


        sql.withBatch(
                "UPDATE order_proposals SET rid = ?, comment_id = ?, state = ? WHERE id = ?;"
        ){ ps->
            sql.withBatch(
                    "UPDATE comments SET comment = ? WHERE id = ?;"
            ) { ps2->
                orderProposals.entrySet().forEach { opsForOrderEntry ->
                    opsForOrderEntry.value.entrySet().forEach { opsForRealtyEntry ->
                        List<InnerOrderProposal> opsForRealty = opsForRealtyEntry.value.collect {
                            proposals.get(it)
                        }
                        String comment = String.join("\n",opsForRealty.findAll{
                            it.comment != null && it.comment != ""
                        }.sort {
                            a,b -> a.updated <=> b.updated
                        }.comment)
                        Long opId
                        def opsProcessing = opsForRealty.findAll {
                            it.state != "UNPROCESSED"
                        }.max {it.updated}
                        if(opsProcessing == null) {
                            opId = opsForRealty.max {it.updated}.id
                        }else{
                            opId = opsProcessing.id
                        }
                        Long commentId
                        def commentWithMaxUpdatedDate = opsForRealty.findAll {
                            it.comment != null && it.commentId != ""
                        }.max {it.updated}
                        if(commentWithMaxUpdatedDate != null){
                            commentId = commentWithMaxUpdatedDate.commentId
                        }
                        ps.addBatch([opsForRealtyEntry.key, commentId, proposals.get(opId).state,opId])
                        orderProposalsWhichShouldNotBeDeleted.add(opId)
                        if(commentId != null){
                            ps2.addBatch([comment,commentId])
                        }
                    }
                }
            }
        }

//        HashMap<Long, HashMap<Long, List<OrderProposal>>> orderProposals = OrderProposal.listOrderById().groupBy(
//                [{ it.order.id },
//                 { fakeToOriginal.get(it.realty.id) }]
//        )
////        log.debug("orderProposals - {}", orderProposals)
//        HashSet<Long> orderProposalsWhichShouldNotBeDeleted = new HashSet<>()
//        def commentsToDelete = new ArrayList<Long>()
//        orderProposals.entrySet().forEach { oTop ->
//            oTop.value.entrySet().forEach { rToP ->
//                rToP.value.comment.forEach() {
//                    if (it != null) {
//                        commentsToDelete.add(it.id)
//                    }
//                }
//            }
//        }
//
//        sql.withBatch(
//                "UPDATE order_proposals SET comment_id = ? , rid = ?, state = ? , updated = ? WHERE id = ? ;"
//        ) { ps ->
//            sql.withBatch(
//                    "UPDATE comments SET comment = ? where id = ? ;"
//            ) { ps2 ->
//                orderProposals.entrySet().forEach { oToP ->
//
////                log.debug("entry - {}", oToP)
////                log.debug("key - {}", oToP.key)
////                log.debug("value - {}", oToP.value)
//                    oToP.value.entrySet().forEach { rToP ->
////                    rToP.value.forEach {
////                        log.debug("order - {}, original - {}, proposal - {}", oToP.key, rToP.key, it.id)
////                    }
//                        def newestOP = rToP.value.max {
//                            it.created
//                        }
//                        def processedOP = rToP.value.findAll {
//                            it.state != OrderProposal.State.UNPROCESSED
//                        }.max { it.updated }
//                        if (!processedOP)
//                            processedOP = newestOP
//                        ps.addBatch([newestOP.commentId, rToP.key, processedOP.state.toString(), newestOP.updated, newestOP.id])
//                        orderProposalsWhichShouldNotBeDeleted.add(newestOP.id)
//                        def comments = rToP.value.comment.findAll {
//                            com -> com != null && com.comment != null && !com.comment.isEmpty()
//                        }
//                        def comment = String.join("\n", comments.sort { a, b ->
//                            a.created <=> b.created
//                        }.comment
//                        )
//                        if (!comments.isEmpty()) {
//                            def commentIds = comments.max { a ->
//                                a.created
//                            }
//                            def commentId = commentIds.id
//                            commentsToDelete.removeElement(commentId)
//                            ps2.addBatch([comment, commentId])
//                            ps.addBatch([commentId, rToP.key, processedOP.state.toString(), newestOP.updated, newestOP.id])
//                        } else {
//                            if (newestOP.commentId != null) {
//                                commentsToDelete.removeElement(newestOP.commentId)
//                            }
//                        }
//                    }
//                }
//            }
//        }

        if (!orderProposalsWhichShouldNotBeDeleted.isEmpty()) {
            sql.execute(
                    "DELETE from order_proposals WHERE id not in (" +
                            String.join(",", orderProposalsWhichShouldNotBeDeleted.collect {
                                return it.toString()
                            }) + ");"
            )
        }
        sql.execute(
                "DELETE from comments " +
                        "where id not in (SELECT comment_id from comments_cp) " +
                        "and id not in (SELECT comment_id from comments_orders) " +
                        "and id not in (SELECT comment_id from comments_realties) " +
                        "and id not in (SELECT comment_id from order_proposals);"
        )
    }

    private static void removeDuplicateRealtiesAndAllDependentObjects(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        // remove realty_phones, realty_purposes, realty_offer_types, order_proposals, realties
        def realtiesList = fakeToOriginal.values()
        String sqlDelete
        if(!realtiesList.isEmpty()){
            def usedRealties = "(" + String.join(",", realtiesList.collect {
                return it.toString()
            }) + ")"
            sqlDelete = "DELETE from realty_phones where realty not in " + usedRealties + ";"
            sql.execute(sqlDelete)

            sqlDelete = "DELETE from realty_purposes where realty_id not in " + usedRealties + ";"
            sql.execute(sqlDelete)

            sqlDelete = "DELETE from realty_offer_types where realty_id not in " + usedRealties + ";"
            sql.execute(sqlDelete)

            sqlDelete = "DELETE from order_proposals where rid not in " + usedRealties + ";"
            sql.execute(sqlDelete)

            sqlDelete = "DELETE from realties where id not in " + usedRealties + ";"
            sql.execute(sqlDelete)

        }else{
            sqlDelete = "DELETE from realty_phones;"
            sql.execute(sqlDelete)

            sqlDelete = "DELETE from realty_purposes;"
            sql.execute(sqlDelete)

            sqlDelete = "DELETE from realty_offer_types;"
            sql.execute(sqlDelete)

            sqlDelete = "DELETE from order_proposals;"
            sql.execute(sqlDelete)

            sqlDelete = "DELETE from realties;"
            sql.execute(sqlDelete)
        }

        sqlDelete = "DELETE from addresses where id not in (SELECT address_id from realties);"
        sql.execute(sqlDelete)
    }

    private static boolean isAddressesEqual(InnerAddress a, InnerAddress b) {
        def shouldBeEqual = ["country", "region", "city", "street", "building", "room"]
        for (def property : shouldBeEqual) {
            def valueA = a.getProperty(property)
//            if (
//            valueA == null || valueA == "" || valueA == "Неизвестно"
//            ) {
//                return false
//            }
            def valueB = b.getProperty(property)
//            if (
//            valueB == null || valueB == "" || valueB == "Неизвестно"
//            ) {
//                return false
//            }
            if (valueA != valueB) {
                return false
            }
        }
//        if (
//        a.room != null && a.room != "" && a.room != " Неизвестно" ||
//                b.room != null && b.room != "" && b.room != " Неизвестно") {
//            if (a.room != b.room) {
//                return false
//            }
//        }
        return true
    }

    private static void removeUselessCounterparties(Sql sql) {
        sql.execute("DELETE FROM counterparties " +
                "where id not in (SELECT owner FROM counterparty_phones) " +
                "and id not in (SELECT customer FROM comments_cp) " +
                "and id not in (SELECT counterparty FROM orders) " +
                "and id not in (SELECT owner FROM realties);")
    }

    private static void mergeRealtiesImages(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        sql.withBatch(
                "UPDATE realty_images SET realty_id = ? WHERE realty_id = ? ;"
        ) { ps ->
            fakeToOriginal.entrySet().forEach {
                ps.addBatch([it.value, it.key])
            }
        }
        ArrayList<Long> shouldBeNotMain = new ArrayList<>()
        // оставляем по одному main
        sql.eachRow(
                "SELECT r1.id as rid from realty_images r1 INNER JOIN realty_images r2 ON r1.realty_id = r2.realty_id " +
                        "WHERE r1.is_main and r2.is_main and r1.id > r2.id;"
        ) { row ->
            shouldBeNotMain.add(row.rid)
        }
        if (!shouldBeNotMain.isEmpty()) {
            String ids = "(" + String.join(",", shouldBeNotMain.collect {
                return it.toString()
            }) + ")"
            sql.execute(
                    "UPDATE realty_images SET is_main = FALSE WHERE id in " + ids + ";"
            )
        }
    }

//    private static void clearCache(def sessionFactory){
//        try {
//            sessionFactory.cache.evictAllRegions()
//            Map<String, ClassMetadata> classesMetadata = sessionFactory.getAllClassMetadata();
//            for (String entityName : classesMetadata.keySet()) {
//                sessionFactory.evictEntity(entityName);
//            }
//        } catch (Exception e) {
//        }
//    }

    private static void processCounterpartiesToDelete(Sql sql) {
        HashMap<Long, Long> oldToNewCp = new HashMap<>()
        sql.eachRow(
                "SELECT DISTINCT c1.id as bad,c2.id as good from counterparties c1 " +
                        "INNER JOIN counterparty_phones cp1 ON c1.id = cp1.owner " +
                        "INNER JOIN counterparties c2 ON c2.id != c1.id " +
                        "INNER JOIN counterparty_phones cp2 ON c2.id = cp2.owner " +
                        "WHERE LOWER(c1.name) REGEXP \"удалить\" and not LOWER(c2.name) REGEXP \"удалить\" and cp1.phone = cp2.phone;"
        ) { row ->
            oldToNewCp.put(row.bad, row.good)
        }

        sql.eachRow(
                "SELECT DISTINCT c1.id as bad,c2.id as good" +
                        " FROM realty_phones rp1" +
                        " INNER JOIN realties r1 ON rp1.realty = r1.id" +
                        " INNER JOIN realty_phones rp2 ON rp1.realty != rp2.realty" +
                        " INNER JOIN realties r2 ON rp2.realty = r2.id" +
                        " INNER JOIN counterparties c1 ON r1.owner = c1.id" +
                        " INNER JOIN counterparties c2 ON r2.owner = c2.id" +
                        " WHERE LOWER(c1.name) REGEXP \"удалить\" AND NOT LOWER(c2.name) REGEXP \"удалить\" AND" +
                        " rp1.phone = rp2.phone;"
        ) { row ->
            oldToNewCp.put(row.bad, row.good)
        }
        sql.withBatch(
                "UPDATE realties SET owner = ? where owner = ? ;"
        ) { ps ->
            oldToNewCp.entrySet().forEach {
                ps.addBatch([it.value, it.key])
            }
        }
        sql.withBatch(
                "UPDATE comments_cp SET customer = ? where customer = ? ;"
        ) { ps ->
            oldToNewCp.entrySet().forEach {
                ps.addBatch([it.value, it.key])
            }
        }

        sql.withBatch(
                "UPDATE orders SET counterparty = ? where counterparty = ? ;"
        ) { ps ->
            oldToNewCp.entrySet().forEach {
                ps.addBatch([it.value, it.key])
            }
        }
    }

    private static void changeCreatedTimeForCounterparties(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        sql.withBatch(
                "UPDATE counterparties SET created = ? WHERE id = ? ;"
        ) { ps ->
            fakeToOriginal.findAll {
                it.key != it.value
            }.groupBy {
                it.value
            }.entrySet().forEach {
                ArrayList<Long> ids = new ArrayList<>()
                ids.add(it.key)
                ids.addAll(it.value.keySet())
                sql.eachRow(
                        "SELECT min(created) as minCreatedTime from counterparties WHERE id IN (" +
                                String.join(",", ids.collect { it.toString() }) + ");"
                ) { row ->
                    ps.addBatch([row.minCreatedTime, it.key])
                }
            }
        }
    }

    private static void changeCreatedTimeForRealties(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        sql.withBatch(
                "UPDATE realties SET created = ? WHERE id = ? ;"
        ) { ps ->
            fakeToOriginal.findAll {
                it.key != it.value
            }.groupBy {
                it.value
            }.entrySet().forEach {
                ArrayList<Long> ids = new ArrayList<>()
                ids.add(it.key)
                ids.addAll(it.value.keySet())
                sql.eachRow(
                        "SELECT min(created) as minCreatedTime from realties WHERE id IN (" +
                                String.join(",", ids.collect { it.toString() }) + ");"
                ) { row ->
                    ps.addBatch([row.minCreatedTime, it.key])
                }
            }
        }
    }

    private static void changeLastUpdateTimeForRealties(Sql sql, HashMap<Long, Long> fakeToOriginal) {
        sql.withBatch(
                "UPDATE realties SET updated = ? WHERE id = ? ;"
        ) { ps ->
            Date now = new Date()
            fakeToOriginal.findAll {
                it.key != it.value
            }.groupBy {
                it.value
            }.entrySet().forEach {
                ps.addBatch([now, it.key])
            }
        }
    }
}
class InnerOrderProposal {
    Long id
    String state
    Date updated
    String comment
    Long commentId
}

class InnerAddress {
    String country
    String region
    String city
    String street
    String building
    String room
}
