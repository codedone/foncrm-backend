package db.migration;

import groovy.sql.Sql;
import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by rs on 6/27/16.
 */
public class V0_9_2_1__phone_internationalizing implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        System.out.println("Im in v0.9.2.1");
        Sql sql = new Sql(jdbcTemplate.getDataSource());
        if(!Normalizer.prettifyPhones(sql)){
            throw new Exception("Couldn't change phones to the international format");
        }
    }
}
