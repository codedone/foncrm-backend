package db.migration

import groovy.sql.Sql
import org.flywaydb.core.api.migration.spring.SpringJdbcMigration
import org.springframework.jdbc.core.JdbcTemplate

import java.sql.DriverManager

/**
 * Created by rs on 1/26/17.
 */
class V0_9_3_5__split_realties implements SpringJdbcMigration {
    @Override
    void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        System.out.println("Im in v0.9.3.5");
        if (RealtySplitter.splitRealties(jdbcTemplate.dataSource)) {
            System.out.println("v0.9.3.5 - success");
        } else {
            System.out.println("v0.9.3.5 - fail");
            throw new Exception(
                    detailMessage: "Something went wrong in Split realties"
            )
        }
    }
}
