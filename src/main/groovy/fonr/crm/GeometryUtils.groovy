package fonr.crm

import com.vividsolutions.jts.geom.*
import org.grails.web.json.JSONArray

public class GeometryUtils {
    public static final DEFAULT_POLYGON = multiPolygon(new GeometryFactory(), [[1, 1], [1, 2], [2, 2], [1, 1]])


    private GeometryUtils() {
    }

    public static def pointInsideMultipolygon(Point point, def multiPolygon) {
        int numberOfOverlappingPolygons = 0
        for (int i; i < multiPolygon.numGeometries; i++) {
            if (multiPolygon.getGeometryN(i).covers(point)) {
                numberOfOverlappingPolygons++
            }
        }
        return numberOfOverlappingPolygons % 2 == 1
    }

    public static def parseMultipolygon(JSONArray json) {
        def gf = new GeometryFactory()
        if (json == null || json.isEmpty()) {
            return DEFAULT_POLYGON
        }
        return gf.createMultiPolygon(
                json.collect { polygon ->
                    gf.createPolygon(
                            polygon.collect { point ->
                                new Coordinate(x: point.lat, y: point.lon)
                            } as Coordinate[])
                } as Polygon[]
        )
    }

    public static JSONArray printMultipolygon(GeometryCollection multiPolygon) {
        println("multi: ${multiPolygon}")
        JSONArray jsonArray = new JSONArray()
        if (multiPolygon != null && !isDefaultPolygon(multiPolygon)) {
            for (int i; i < multiPolygon.numGeometries; i++) {
                jsonArray.add(multiPolygon.getGeometryN(i).coordinates.collect {
                    return [
                            lon: it.x,
                            lat: it.y
                    ]
                })
            }
        }
        return jsonArray
    }

    public static MultiPolygon multiPolygon(GeometryFactory gf, List<List<Integer>> points) {
        def coords = points.collect { new Coordinate(it[0], it[1]) }
        def polygon = gf.createPolygon(coords as Coordinate[])
        return gf.createMultiPolygon([polygon] as Polygon[])
    }

    public static boolean isDefaultPolygon(GeometryCollection multiPolygon) {
        if (multiPolygon != null && multiPolygon.numGeometries == 1) {
            return multiPolygon.getGeometryN(0).equalsExact(DEFAULT_POLYGON.getGeometryN(0))
        } else {
            return false
        }
    }
}
