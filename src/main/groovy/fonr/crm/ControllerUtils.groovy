package fonr.crm

import grails.artefact.Controller
import grails.converters.JSON
import org.grails.web.json.JSONObject
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.validation.FieldError

import static org.springframework.http.HttpStatus.BAD_REQUEST

trait ControllerUtils {
    def messageSource

    public void renderError(HttpStatus status) {
       renderError(status, null)
    }

    public String message(String messageId) {
        return messageSource.getMessage(messageId, null, Locale.default)
    }

    public void renderError(HttpStatus status, String message) {
        def result = [status: status.value(), error: status.getReasonPhrase(), message: message ?: '']

        def self = this as Controller
        self.response.status = status.value()
        self.render result as JSON
    }

    public void renderFieldErrors(List<FieldError> errors) {
        def result = errors.collect { [field: it.field, message: messageSource.getMessage(it,Locale.default)] }

        def self = this as Controller
        self.response.status = 400
        self.render result as JSON
    }

    public JSONObject readJSON() {
        def self = this as Controller

        def requestType = self.request.getMimeTypes()[0] ?: null
        if (requestType.extension != "json") {
            renderError(BAD_REQUEST, message('http.messages.badRequest'))
            return null
        }

        try {
            def json = self.request.JSON as JSONObject
            if (!json) {
                renderError(BAD_REQUEST, message('http.messages.badRequest'))
            }
            return json
        } catch (Exception ignored) {
            renderError(BAD_REQUEST, message('http.messages.badRequest'))
            return null
        }
    }

}
