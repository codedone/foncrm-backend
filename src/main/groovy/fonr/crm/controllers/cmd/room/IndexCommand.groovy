package fonr.crm.controllers.cmd.room

import fonr.crm.Room
import grails.validation.Validateable

class IndexCommand implements Validateable {

    private final static allowedOrderFields = [
            'created', 'updated',
            'square', 'totalprice','pricepersquaremeter',
            'id','info'
    ]

    TimeRange created
    TimeRange updated
    Long realty
    String info
    Integer floorNumber
    Boolean leased

    Boolean shareable
    DoubleRange minimalPart

    Boolean rampantAvailable
    Boolean withFurniture

    DoubleRange square
    DoubleRange ceilingHeight
    DoubleRange pricePerSquareMeter // in rubles
    DoubleRange totalPrice // in rubles
    String priceType
    String heatType
    DoubleRange dedicatedElectricPower

    String roomType
    String roomPlanType
    String roomCondition
    String windowType

    Long amount
    Long offset
    String order
    String orderBy

    static constraints = {
        pricePerSquareMeter nullable: true
        totalPrice nullable: true
        leased nullable: true
        shareable nullable: true
        minimalPart nullable: true
        heatType nullable: true, validator: {
            String value, IndexCommand cmd ->
                if (value != null) {
                    try {
                        !Room.HeatType.values().name.contains(value)
                    } catch (Exception e) {
                        return ['invalid']
                    }
                }
                return true
        }
        priceType nullable: true, validator: {
            String value, IndexCommand cmd ->
                if (value != null) {
                    try {
                        !Room.PriceType.values().name.contains("per" + value)
                    } catch (Exception e) {
                        return ['invalid']
                    }
                }
                return true
        }
        roomType validator: {
            String value, IndexCommand cmd ->
                if(value!=null && !Room.RoomType.values().name.contains(value)){
                    return ['invalid']
                }
                return true
        }

        roomPlanType validator: {
            String value, IndexCommand cmd ->
                if(value!=null && !Room.RoomPlanType.values().name.contains(value)){
                    return ['invalid']
                }
                return true
        }

        roomCondition validator: {
            String value, IndexCommand cmd ->
                if(value!=null && !Room.RoomCondition.values().name.contains(value)){
                    return ['invalid']
                }
                return true
        }

        windowType validator: {
            String value, IndexCommand cmd ->
                if(value!=null && !Room.WindowType.values().name.contains(value)){
                    return ['invalid']
                }
                return true
        }

        orderBy validator: {
            val, obj ->
                if (val != null && !allowedOrderFields.contains(val.toLowerCase().trim())) {
                    return ['invalid']
                }
                return true
        }

        order validator: {
            val, obj ->
                if (val != null && !(val.toLowerCase().trim() ==~ /^desc|asc$/))
                    return ['invalid']
                return true
        }

        amount validator: {
            Long value, IndexCommand cmd ->
                if (value != null && value < 0) {
                    return ['invalid']
                }
                return true
        }
        offset validator: {
            Long value, IndexCommand cmd ->
                if(value != null && value < 0){
                    return ['invalid']
                }
                return true
        }
    }

    static boolean defaultNullable() {
        true
    }

    static class TimeRange implements Validateable {
        Long min
        Long max

        static boolean defaultNullable() {
            true
        }
    }

    static class DoubleRange implements Validateable{
        Double min
        Double max

        static boolean defaultNullable() {
            true
        }
    }

    static class IntegerRange implements Validateable {
        Integer min
        Integer max

        static boolean defaultNullable() {
            true
        }
    }

    static class PriceFilter implements Validateable {
        Double min
        Double max
        String priceType

        static constraints = {
            min nullable: true, min: new Double(0.0)
            max nullable: true, min: new Double(0.0)
            priceType nullable: true, validator: {
                String value, PriceFilter priceFilter ->
                    if ((priceFilter.min != null || priceFilter.max != null) && (value == null || !Room.PriceType.values().name.contains("per" + value))) {
                        return ['priceTypeNotDefined']
                    }
                    return true
            }
        }
    }

}
