package fonr.crm.controllers.cmd.room

import fonr.crm.Realty
import fonr.crm.Room
import fonr.crm.RoomImage
import grails.validation.Validateable

class SaveCommand implements Validateable {
    Long realty
    String info
    Double square // in square meters
    Double ceilingHeight // in meters
    Boolean leased = false
    Integer floorNumber = 1
    Double pricePerSquareMeter // in rubles
//    BigDecimal totalPrice // in rubles
    String priceType
    String roomType

    Boolean shareable
    Double minimalPart

    // only for stocks
    Boolean rampantAvailable
    Double dedicatedElectricPower // in Watts

    //only for offices
    String heatType
    String roomPlanType
    String roomCondition
    String windowType
    Boolean withFurniture

    List<Image> images

    static constraints = {
//        importFrom Room
        info maxSize: 4096, blank: true, nullable: true
        square nullable: false, min: new Double(0)
        leased nullable: false
        shareable nullable: true
        minimalPart nullable: true, min: new Double(0.0), validator: {
            Double value, SaveCommand cmd ->
                if (cmd.shareable != null && cmd.shareable && value == null) {
                    return ["isNull"]
                }
                if ((cmd.shareable == null || !cmd.shareable) && value != null) {
                    return ["isNotNull"]
                }
                if (cmd.shareable != null && cmd.shareable && value != null
                        && cmd.square != null && value > cmd.square) {
                    return ["squareIsLessThanMinimalPart"]
                }
                return true
        }

        floorNumber nullable: false
        pricePerSquareMeter nullable: false, min: new Double(0)
//        totalPrice nullable: true, min: BigDecimal.ZERO

        priceType nullable: true, validator: {
            String value, SaveCommand cmd ->
                if (cmd.realty != null) {
                    Realty realty1 = Realty.findById(cmd.realty)
                    if (realty1 != null) {
                        if (realty1.offerTypes.id.contains(2L)) {
                            // объект на продажу
                            if (value != null) {
                                return ['notNullForSale']
                            }
                        } else {
                            // объект в аренду
                            if (value == null || !Room.PriceType.values().name.contains("per" + value)) {
                                return ['nullForRent']
                            }
                        }
                    }
                }
                return true
        }

        roomType nullable: false, validator: {
            String value, SaveCommand cmd ->
                if(value != null){
                    if (!Room.RoomType.values().name.contains(value)) {
                        return ['notFound']
                    }
                }
                return true
        }

        ceilingHeight nullable: true, min: new Double(0), validator: {
            Double h, SaveCommand cmd ->
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.STOCK.name && h == null) {
                    return ['isNullForStock']
                }
                return true
        }

        heatType nullable: true, validator: {
            String value, SaveCommand cmd ->
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.STOCK.name) {
                    if (value == null) {
                        return ['isNullForStock']
                    }
                    if (!Room.HeatType.values().name.contains(value)) {
                        return ['notFound']
                    }
                }
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.OFFICE.name && value != null) {
                    return ['notNullForOffice']
                }
                return true
        }

        rampantAvailable nullable: true,  validator: {
            Boolean value, SaveCommand cmd ->
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.STOCK.name && value == null) {
                    return ["isNullForStock"]
                }
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.OFFICE.name && value != null) {
                    return ["notNullForOffice"]
                }
                return true
        }

        dedicatedElectricPower nullable: true, min: new Double(0), validator: {
            Double value, SaveCommand cmd ->
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.OFFICE.name && value != null) {
                    return ["notNullForOffice"]
                }
                return true
        }

        roomPlanType nullable: true,  validator: {
            String value, SaveCommand cmd ->
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.STOCK.name && value != null) {
                    return ["notNullForStock"]
                }
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.OFFICE.name && value != null && !Room.RoomPlanType.values().name.contains(value)) {
                        return ["notFound"]
                }
                return true
        }

        roomCondition nullable: true,  validator: {
            String value, SaveCommand cmd ->
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.STOCK.name && value != null) {
                    return ["notNullForStock"]
                }
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.OFFICE.name && value != null && !Room.RoomCondition.values().name.contains(value)) {
                    return ["notFound"]
                }
                return true
        }

        windowType nullable: true,  validator: {
            String value, SaveCommand cmd ->
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.STOCK.name && value != null) {
                    return ["notNullForStock"]
                }
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.OFFICE.name && value != null && !Room.WindowType.values().name.contains(value)) {
                    return ["notFound"]
                }
                return true
        }

        withFurniture nullable: true, validator: {
            Boolean value, SaveCommand cmd ->
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.STOCK.name && value != null) {
                    return ["notNullForStock"]
                }
                if (cmd.roomType != null && cmd.roomType == Room.RoomType.OFFICE.name && value == null) {
                    return ["isNullForOffice"]
                }
                return true
        }

        realty nullable: false, validator: {
            Long id, SaveCommand cmd ->
                if (id != null && !Realty.exists(id))
                    return ['notFound']
                return true
        }
        images validator: {
            List<Image> list, SaveCommand cmd ->
                if (list != null && !list.empty) {
                    if (list.contains(null))
                        return ['list.null']

                    def mainImageCount = 0

                    def ids = list.collect {
                        if (it.main)
                            mainImageCount++
                        it.id
                    }

                    if (mainImageCount == 0)
                        return ['main.empty']

                    if (mainImageCount > 1)
                        return ['main.multiple']


                    def imageCount = RoomImage.countByIdInList(ids)
                    if (imageCount == 0 || imageCount != list.size())
                        return ['notFound']
                }
                return true
        }
    }

//    def beforeValidate(){
//        if(
////        totalPrice == null &&
//                square != null && pricePerSquareMeter != null){
//            totalPrice = square * pricePerSquareMeter
//        }
////        if(pricePerSquareMeter == null && totalPrice != null && square != null && square != new BigDecimal("new Double(0)")){
////            pricePerSquareMeter = totalPrice.divide(square,new MathContext(2, RoundingMode.HALF_UP))
////        }
//    }
}
