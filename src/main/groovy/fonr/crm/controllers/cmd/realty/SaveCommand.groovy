package fonr.crm.controllers.cmd.realty

import fonr.crm.Counterparty
import fonr.crm.OfferType
import fonr.crm.RealtyImage
import fonr.crm.RealtyPurposeType
import grails.validation.Validateable

class SaveCommand implements Validateable {
    List<String> phones
    Address address
    List<Long> purposes
    List<Long> offerTypes
    Long owner
    List<Image> images
    String info

    static constraints = {
//        importFrom Realty
        info nullable: true
        phones nullable: true
        purposes nullable: false, minSize: 1, validator: {
            List<Long> list, SaveCommand cmd ->
                if (list != null) {
                    if (list.contains(null))
                        return ['list.null']
                    if (RealtyPurposeType.countByIdInList(list) != list.size()) {
                        return ['notFound']
                    }
                }
                return true
        }
        offerTypes nullable: false, minSize: 1, maxSize: 1, validator: {
            List<Long> offerTypes, SaveCommand cmd ->
                if (offerTypes != null) {
                    if (offerTypes.contains(null))
                        return ['list.null']
                    if (OfferType.countByIdInList(offerTypes) != offerTypes.size())
                        return ['notFound']
                }
                return true
        }
        address validator: {
            Address add, SaveCommand cmd ->
                if (add != null && !add.validate()) {
                    return ['invalid']
                }
                return true
        }
        owner nullable: false, validator: {
            Long id, SaveCommand cmd ->
                if (id != null && !Counterparty.exists(id))
                    return ['notFound']
                return true
        }
        images validator: {
            List<Image> list, SaveCommand cmd ->
                if (list != null && !list.empty) {
                    if (list.contains(null))
                        return ['list.null']

                    def mainImageCount = 0

                    def ids = list.collect {
                        if (it.main)
                            mainImageCount++
                        it.id
                    }

                    if (mainImageCount == 0)
                        return ['main.empty']

                    if (mainImageCount > 1)
                        return ['main.multiple']


                    def imageCount = RealtyImage.countByIdInList(ids)
                    if (imageCount == 0 || imageCount != list.size())
                        return ['notFound']
                }
                return true
        }
    }
}
