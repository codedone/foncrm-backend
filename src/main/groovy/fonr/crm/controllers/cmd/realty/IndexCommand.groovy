package fonr.crm.controllers.cmd.realty

import grails.validation.Validateable

class IndexCommand implements Validateable {

    private final static allowedOrderFields = ['address', 'created', 'updated']

    TimeRange created
    TimeRange updated
    Long offerType
    String phone
    String address
    Long amount
    Long offset
    String order
    String orderBy

    static constraints = {
        orderBy validator: {
            val, obj ->
                if (val != null && !allowedOrderFields.contains(val.toLowerCase().trim())) {
                    return ['invalid']
                }
                return true
        }

        order validator: {
            val, obj ->
                if (val != null && !(val.toLowerCase().trim() ==~ /^desc|asc$/))
                    return ['invalid']
                return true
        }
    }

    static boolean defaultNullable() {
        true
    }

    static class TimeRange implements Validateable {
        Long min
        Long max

        static boolean defaultNullable() {
            true
        }
    }
}
