package fonr.crm.controllers.cmd.realty

import grails.validation.Validateable

class Address implements Validateable {
    String country
    String region
    String city
    String street
    String building
    String room
    Location location

    static constraints = {
        importFrom fonr.crm.Address
        location validator: {
            Location val, Address add ->
                if (val != null) {
                    return val.lat != null && val.lon != null
                }
                return true
        }
    }
}