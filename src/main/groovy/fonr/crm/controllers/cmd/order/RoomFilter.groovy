package fonr.crm.controllers.cmd.order

import fonr.crm.OfferType
import fonr.crm.Room
import fonr.crm.controllers.cmd.room.IndexCommand

/**
 * Created by rs on 1/28/17.
 */
class RoomFilter {
    boolean empty = true
    OfferType offerType

    Room.HeatType heatType
    Boolean leased
    Boolean rampantAvailable
    Boolean withFurniture
    Boolean shareable

    IndexCommand.DoubleRange minimalPart
    IndexCommand.DoubleRange square
    IndexCommand.DoubleRange ceilingHeight
    IndexCommand.PriceFilter pricePerSquareMeter // in rubles
    IndexCommand.PriceFilter totalPrice // in rubles
    IndexCommand.DoubleRange dedicatedElectricPower

    IndexCommand.IntegerRange floorNumber

    Room.RoomType roomType
    Room.RoomPlanType roomPlanType
    Room.RoomCondition roomCondition
    Room.WindowType windowType
    String info
}
