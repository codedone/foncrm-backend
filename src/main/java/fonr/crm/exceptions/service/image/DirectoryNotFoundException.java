package fonr.crm.exceptions.service.image;

public class DirectoryNotFoundException extends RuntimeException {
    public DirectoryNotFoundException(Throwable cause) {
        super(cause);
    }
}
