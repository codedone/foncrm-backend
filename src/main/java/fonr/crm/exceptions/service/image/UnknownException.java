package fonr.crm.exceptions.service.image;

public class UnknownException extends RuntimeException {
    public UnknownException(Throwable cause) {
        super(cause);
    }
}
